#Todo DPS v1 -- first launch
This is a to-do file that should get updated regularly during process of finishing DPSv1.

## Priority
* **BUG**: FB share/recommend buttons not working when routing


##General
* //responsive unified cards for article, event, initiative in Card.vue
    * sort out linking -- events need to be linked to event page
* design footer

##Laravel Vue nuxtification

* //create **article-search controller and view** by category on articles/{category/


##Article
* if DRAFT then dont show it anywhere

# Todo DPS v1.1
##General
* [vue lazyload images](https://github.com/hilongjw/vue-lazyload)
## Micelij / organisations
* markers w/ icons that correspond to category (category icon in admin)
* markers currently not shown, uncomment them in mapcontroller
* **Spaification**: create spa route controller, add meta and remove mounted()/created()
* add Micelij to menu

## Administration
* [add google analytics on admin homepage](https://murze.be/2016/06/retrieve-google-analytics-date-laravel/)

## Article
* *comments*

## Homepage
* *newsletter* just before footer

# Daljna prihodnost
* opomnik za živost posamezne iniciative
* donacije ob vpisu
* ena iniciativa lahko ima več lokacij
* partnerske iniciative