# DPS

Main dependencies:
* backend
    * Laravel 5.4
    * php-7.1
    * Mysql
    * caching system (redis suggested)
* frontend
    * Nuxt (server rendering automatically provided)
    * axios
    * lodash
    * jQuery
    * bootstrap
    
    
    
Development works best with [Laravel Homestead](https://laravel.com/docs/master/homestead).

Nginx available sites configs.
Dont forget to  `sudo ln -s /etc/nginx/sites-available/permakultura.new /etc/nginx/sites-enabled/` and `sudo service nginx restart`. 

```
# /etc/nginx/sites-available/permakultura.new

server {
    listen 80;
    server_name permakultura.new;
    access_log /var/log/nginx/permakultura.new.access.log;
    location / {
        proxy_pass    http://127.0.0.1:3000/;
    }
}

```

```
# /etc/nginx/sites-available/api.permakultura.new

server {
    listen 80;
    #listen 443 ssl http2;
    server_name api.permakultura.new;
    root "/home/vagrant/dps-laravel-nuxt/laravel/public";

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/api.permakultura.new-error.log error;

    sendfile off;

    client_max_body_size 100m;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    location ~ /\.ht {
        deny all;
    }

    #ssl_certificate     /etc/nginx/ssl/api.permakultura.new.crt;
    #ssl_certificate_key /etc/nginx/ssl/api.permakultura.new.key;
}

```
edit: permissions dont have to be set if the project resides in the user dir. In that case the only needed thing to do is:
```
cd permakultura.si
sudo chgrp www-data .
```

You can skip this step if project resides in user dir.
Also, set permissions straight with
```
find . -type d -exec chmod 0775 {} \;
find . -type f -exec chmod 0664 {} \;
```
and `sudo chown -R www-data:www-data`. Node modules need to be writeable for 
group www-data (make sure you're in it), so make that `sudo chmod -R 0775 nuxt/node_modules/`. 
Same goes for laravel storage, `sudo chmod -R 0775 laravel/storage/`. Prerequisite: make git 
`not see your permission changes with `git config core.fileMode false`.

On production server, you will be needing a node server production manager for running `npm start` as a daemon. 
Use `npm install pm2 -g` and `pm2 start npm --name "permakultura.si" -- start` to do so.

Also on production ssl needs to be implemented first in order for Nuxt server to work on the right api connection. Else errors will be returned.

## Production Server setup

* create new user permadrustvo, add to www-data and sudo group.
* create ssh keys and add to repository as read-only.
* Install nginx
* Install php, php modules and composer
```
sudo apt-get install php php-common php-mbstring php-xml php-zip php-curl composer
```
* Install mysql
    * create user permadrustvo and grant privileges to database dps.
    * create dps database
* Install Elasticsearch 7 and set memory to 512m
* Install Redis
* Setup ufw
* Add nginx server settings
* Add nginx server_name to hosts file if dns not set
* Install certbot for SSL after DNS is set.
* Setup .env for laravel, set to production edit url.
* run `composer install` in laravel dir
* run `npm install` in nuxt dir


## Nuxt init
```
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## Laravel init
First, `composer update` and then create .env file. 

## Using docker-compose for development

1. Add `127.0.0.1 permakultura.local api.permakultura.local` to hosts file
2. Run `docker-compose run --rm fpm composer install` to install Laravel dependencies
3. Run `docker-compose run --rm nuxt npm install` to install Nuxt dependencies
4. Run `docker-compose up` to start everything
5. Open (http://permakultura.local)[http://permakultura.local] in browser

## Troubleshooting

### Nuxt returnw 'status' of undefined!

Reason: Nuxt cannot connect to api.

* On dev check if server is set to correct url api.permakultura.local
* On production check if SSL is working and api.permakultura.si is reachable.

### Nuxt is unreachable, api is reachable from outside of server.

Check if api is reachable from server. Edit hosts if it isn't.

### Nginx Bad Gateway!

Check if php-fpm version in api.permakultura.si server config is the right one. Current: 7.2

[This guide on how to make a nuxt module](https://github.com/nuxt/nuxt.js/issues/1043) might come handy. 