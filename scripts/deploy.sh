#!/bin/bash

bold=`tput bold`
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

SOURCE=/var/www/source/
BAK=/var/www/bak
PROD=/var/www/permakultura.si
DATABASE=dps

sudo mkdir $SOURCE
sudo mkdir $BAK
sudo chmod -R 0775 $BAK

cd $PROD
is_unclean_work_tree () {
        git rev-parse --verify HEAD >/dev/null || exit 1
        git update-index -q --ignore-submodules --refresh
        err=0

        if ! git diff-files --quiet --ignore-submodules
        then
            echo >&2 "Cannot $1: You have unstaged changes."
            err=1
        fi

        if ! git diff-index --cached --quiet --ignore-submodules HEAD --
        then
            if [ $err = 0 ]
            then
                echo >&2 "${red}Cannot $1: Your index contains uncommitted changes.${reset}"
            else
                echo >&2 "Additionally, your index contains uncommitted changes."
            fi
            err=1
        fi

        return $err
}
is_unclean_work_tree "pull from remote repository"
ret=$?

if [ "$ret" = "1" ]; then
        echo "If you think you can discard these changes, try ${bold}cd permakultura.si && git add . && git stash${reset}."
        exit 1
else
        echo "${green}- Clean work tree, starting deployment process.${reset}"
fi

# Create destination folder

NOW=$(date +"%Y-%m-%d-%H-%M-%S")
DEST_FOLDER="$SOURCE$NOW"
echo "${green}${bold}- Folder $DEST_FOLDER created.${reset}\n"

# Mysql backup
echo "Dumping database, enter password:\n"
cd $BAK
mysqldump $DATABASE > "$NOW.sql" -u permadrustvo -p
echo "${green}${bold}- Mysqldump'd to $BAK/$NOW.sql.${reset}\n"
echo "Attempting copying directory $PROD to $DEST_FOLDER.\n"

mkdir $DEST_FOLDER
cp -rf $PROD/. $DEST_FOLDER
echo "${green}${bold}- Copied current production directory.${reset}\n"
echo "Setting permissions so that you can write in this directory, making sure git doesn't track fileMode changes.\n"
cd $DEST_FOLDER
git config core.fileMode false
sudo chmod -R 0775 $DEST_FOLDER

# Git pull
echo "Attempting ${bold}git pull origin master${reset}.\n"
git pull origin master
echo "${green}${bold}- Git pulled.${reset}\n"

# Laravel
echo "${bold}Laravel:${reset}\n"
ln -s /var/www/source/.env "$DEST_FOLDER/laravel"
cd "$DEST_FOLDER/laravel"
composer install
php artisan migrate --force --no-interaction
echo "${green}${bold}- Laravel migrated and composer installer.${reset}\n"

# Nuxt
echo "${bold}Nuxt:${reset}\n"
cd "$DEST_FOLDER/nuxt"
npm install
npm run build
echo "${green}${bold}- Nuxt npm installed and built.${reset}\n"

# Permissions
echo "Setting permissions:\n"
cd $DEST_FOLDER
sudo chown -R www-data:www-data $DEST_FOLDER
sudo find . -type d -exec chmod 0775 {} \;
sudo find . -type f -exec chmod 0664 {} \;
sudo chmod -R 0775 nuxt/node_modules/
sudo chmod -R 0775 laravel/storage/
echo "${green}${bold}- Permissions set quite accordingly as to what they should be.${reset}\n"

sudo rm -rf $PROD
ln -s $DEST_FOLDER $PROD
echo "${green}${bold}- Symlink'd.${reset}\n"
sudo service php7.1-fpm restart
echo "${green}${bold}- Restarted service php7.1-fpm${reset}\n"
pm2 reload permakultura.si
echo "${green}${bold}- pm2 reloaded. All set.${reset}\n"
