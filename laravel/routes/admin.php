<?php
/**
 * Initiatives, categories, audiences
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin\Initiative', 'middleware' => 'authorize:initiatives'], function () {
    CRUD::resource('audience', 'AudienceCrudController');
    CRUD::resource('initiative', 'InitiativeCrudController');
});

/**
 * Events
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'authorize:events'], function () {
    CRUD::resource('event', 'EventCrudController');
});

/**
 * Articles
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'authorize:articles'], function () {
    CRUD::resource('article', 'ArticleCrudController');
    CRUD::resource('reference', 'ReferenceCrudController');
});

Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'authorize:categories and tags'], function () {
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('tag', 'TagCrudController');
});

/**
 * Users
 */
Route::group(['namespace' => 'Backpack\PermissionManager\app\Http\Controllers', 'middleware' => 'authorize:users'], function () {
    CRUD::resource('permission', 'PermissionCrudController');
    CRUD::resource('role', 'RoleCrudController');
    CRUD::resource('user', 'UserCrudController');
});

/**
 * Menu
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'authorize:menu'], function () {
    CRUD::resource('menu-item', 'MenuItemCrudController');
});

/**
 * Languages
 */
Route::group(['namespace' => 'Backpack\LangFileManager\app\Http\Controllers', 'middleware' => 'authorize:translations'], function () {
    Route::get('language/texts/{lang?}/{file?}', 'LanguageCrudController@showTexts');
    Route::post('language/texts/{lang}/{file}', 'LanguageCrudController@updateTexts');
    Route::resource('language', 'LanguageCrudController');
});

/**
 * Settings
 */
Route::group(['namespace' => 'Backpack\Settings\app\Http\Controllers', 'middleware' => 'authorize:settings'], function () {
    Route::resource('setting', 'SettingCrudController');
});

/**
 * Backups
 */
Route::group(['namespace' => 'Backpack\BackupManager\app\Http\Controllers', 'middleware' => 'authorize:backups'], function () {
    Route::get('backup', 'BackupController@index');
    Route::put('backup/create', 'BackupController@create');
    Route::get('backup/download/{file_name?}', 'BackupController@download');
    Route::delete('backup/delete/{file_name?}', 'BackupController@delete');
});

/**
 * Admin ajax calls
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin\Ajax', 'prefix' => 'ajax'], function() {
    Route::get('/tags', 'TagController@index');
    Route::get('/tags/{id}', 'TagController@show');

    Route::get('/references', 'ReferenceController@index');
    Route::get('/references/{id}', 'ReferenceController@show');
});
