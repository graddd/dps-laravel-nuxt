<?php

/*
|--------------------------------------------------------------------------
| SPA Routes
|--------------------------------------------------------------------------
|
| Routes called for in fetch() method of nuxt pages.
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/article', 'ArticleController@index')->name('articles');
Route::get('/article/{slug}', 'ArticleController@find')->name('article');
Route::get('/search', 'SearchController@search')->name('search');

//Route::get('/calendar',)
Route::get('/calendar', 'EventController@calendar')->name('calendar');
Route::get('/event', 'EventController@index')->name('events');
Route::get('/event/{slug}', 'EventController@find')->name('event');