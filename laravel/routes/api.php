<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'spa', 'as' => 'spa'], function () {
   require base_path('routes/spa.php');
});

Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function() {
   Route::get('/', 'Api\UserController@current')->name('api.user.current');
   Route::get('/profile', 'Api\UserController@profile')->name('api.user.profile');

   Route::group(['prefix' => 'initiative'], function() {
       Route::get('/', 'Api\User\InitiativeController@index');
       Route::get('/{id}', 'Api\User\InitiativeController@show');
       Route::post('/', 'Api\User\InitiativeController@store')
           ->name('api.user.initiative.store');
   });

});

Route::group(['prefix' => 'auth'], function() {
    Route::get('/facebook', 'Auth\LoginController@facebook')->name('api.auth.facebook');
    Route::post('/password', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken')
        ->middleware('password-grant');
});

Route::group(['prefix' => 'initiative'], function () {
    Route::get('/', 'Api\InitiativeController@index')->name('api.initiative.index');
    Route::get('/options', 'Api\InitiativeController@options')->name('api.initiative.options');
    Route::get('/{id}', 'Api\InitiativeController@show')->name('api.initiative.show');
});

Route::group(['prefix' => 'article'], function() {
    Route::get('/search', 'Api\ArticleController@search')->name('api.article.search');
    Route::get('/options', 'Api\ArticleController@options')->name('api.article.options');
//    Route::get('/{slug}', 'Api\ArticleController@show')->name('api.article.show'); // not needed
});

Route::group(['prefix' => 'event'], function() {
    Route::get('/search', 'Api\EventController@search')->name('api.event.search');
});

Route::group(['prefix' => 'map'], function () {
    Route::get('filters-data', ['as' => 'initiative-filters-data', 'uses' => 'MapController@filtersData']); # called on init, fetches filter data

    Route::get('markers', ['as' => 'map-markers', 'uses' => 'MapController@markers']); # called on filters change
    Route::get('initiatives', ['as' => 'initiatives', 'uses' => 'MapController@initiatives']); # called on boundsChange
});

Route::group(['prefix' => 'email'], function() {
    Route::post('/subscribe', 'Api\EmailController@subscribe')->name('api.email.subscribe');
});
