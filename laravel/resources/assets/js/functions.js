export default {
    createPathFromDateObject(dateString, storeObj) {
        const path = dateString;
        const obj = storeObj;
        const explodePath = path.split('_');
        let currentRoot = obj;

        for (let i = 0; explodePath.length > i; i++) {
            if (typeof currentRoot[explodePath[i]] === 'undefined') {
                currentRoot[explodePath[i]] = {};
            }
            currentRoot = currentRoot[explodePath[i]];
        }
        return obj;
    },
    convertDateObjToStringModal(date, fixMonth) {
        const year = date.getFullYear();
        let month = date.getMonth();
        let day = date.getDate();

        month = fixMonth ? month + 1 : month;

        day = (day <= 9) ? `0${day}` : day;
        month = (month <= 9) ? `0${month}` : month;

        return `${day}.${month}.${year}`;
    },
    convertDateObjToString(date) {
        return `y${date.getFullYear()}_m${date.getMonth()}_d${date.getDate()}`;
    },
    findEventByDateString(dateString, eventsObj) {
        const events = eventsObj;
        const path = dateString;
        const explodePath = path.split('_');
        let currentRoot = events;

        for (let i = 0; explodePath.length > i; i++) {
            if (typeof currentRoot[explodePath[i]] === 'undefined') {
                return false;
            }
            currentRoot = currentRoot[explodePath[i]];
        }

        return currentRoot;
    },
    cleanCurrentEventObj(eventObj, pathArr) {
        const dateArr = pathArr;
        let counter = (dateArr.length - 1);
        console.log((dateArr));
        if (Object.keys(dateArr[counter]) &&
            dateArr.length > 0) {
            // delete dateArr[counter--];
            // this.cleanCurrentEventObj(eventObj, dateArr);
            counter--;
        } else {
            console.log('end');
        }
    },
    isEqualDates(date1, date2) {
        const date1Year = date1.getFullYear();
        const date2Year = date2.getFullYear();
        const date1Month = date1.getMonth();
        const date2Month = date2.getMonth();
        const date1Day = date1.getDate();
        const date2Day = date2.getDate();

        return date1Year === date2Year &&
            date1Month === date2Month &&
            date1Day === date2Day;
    },
    isThisMonth(date1, date2) {
        return date1.getFullYear() === date2.getFullYear() &&
            date1.getMonth() === date2.getMonth()
    },
    isBetweenDates(selected, date1, date2) {
        return selected.getFullYear() >= date1.getFullYear() &&
                selected.getFullYear() <= date2.getFullYear() &&
                selected.getMonth() >= date1.getMonth() &&
                selected.getMonth() <= date2.getMonth() &&
                selected.getDate() >= date1.getDate() &&
                selected.getDate() <= date2.getDate()
    },
    convertDateObjToStringWithLeadingZeroes(date) {
        return date.getFullYear() + '/'
            + ('0' + (date.getMonth()+1)).slice(-2) + '/'
            + ('0' + date.getDate()).slice(-2)

    },
    getDates(startDate, stopDate) {
        let dateArray = [];
        let currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push( new Date (currentDate) )
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }
};

Date.prototype.addDays = function(days) {
    let dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}