import Vue from 'vue'
import VueI18n from 'vue-i18n'

/**
 * Localization in Vue
 */
import messages from './generated/vue-i18n-locales.generated' //Localization binding Vue-Laravel.


const locale = 'sl'
const fallbackLocale = 'en'

Vue.use(VueI18n)
Vue.config.lang = locale

/**
 * Vue localization
 * @type {VueI18n}
 */
const i18n = new VueI18n({
    locale,
    fallbackLocale,
    messages,
})

export default i18n