// ALL THESE SHOULD BE SYNCHRONOUS, THUS IN MUTATIONS
// export const succeed = ({state, commit}, {progressBar}) => {
//     if(state.loading) {
//         progressBar.finish();
//     }
//     commit('updateLoading', false);
//     // failed and failedResponse stay the same! when it fails, no success (axios) can un-fail it.
// };
//
// export const fail = ({commit}, {error, progressBar}) => {
//     let html = document.createElement('html');
//     html.innerHTML = error.response.data;
//     let failedResponse = html.getElementsByTagName('body')[0].innerHTML;
//
//     progressBar.fail();
//     commit('updateLoading', false);
//     commit('updateFailed', error.response);
//     commit('updateFailedResponse', failedResponse)
// };
//
// // export const unfail = ({commit}) => {
// //     commit('updateFailed', false);
// //     commit('updateFailedResponse', null)
// // };
//
// export const load = ({commit}, progressBar) => {
//     progressBar.start();
//
//     commit('updateLoading', true);
//     // Route change can un-fail a failed state
//     commit('updateFailed', false);
//     commit('updateFailedResponse', null)
// };