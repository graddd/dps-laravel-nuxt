export const initialLoad = (state) => {
    return state.initialLoad
};
export const loading = (state) => {
    return state.loading
};
export const failed = (state) => {
    return state.failed
};
export const failedResponse = (state) => {
    return state.failedResponse
};
export const metadata = (state) => {
    return state.metadata
};
export const breadcrumb = (state) => {
    return state.metadata.breadcrumb
};