import * as actions from './actions'
import * as getters from './getters'
import mutations from "./mutations"
import Vue from 'vue'
import Vuex from 'vuex'

/**
 * vuex store custom modules
 */
import routed from './modules/routed.js'
import initiative from './modules/initiative'
import header from './modules/header'
import article from './modules/article'
import calendar from './modules/calendar'
import auth from './modules/auth'
import user from './modules/user'
import search from './modules/search'

/**
 * Debug on development only! Deep watch ahead.
 *
 * @type {boolean}
 */
const debug = window.__ENV__ !== 'production';
let cache = require('js-cache');

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        initialLoad: true,
        loading: false,
        failed: false,
        failedResponse: null,
        metadata: {}
    },
    mutations,
    getters,
    actions,
    modules: {
        routed,
        auth,
        initiative,
        header,
        article,
        calendar,
        // user,
        search
    },
    strict: debug,
});

/**
 * Replace state with supplemented initial state on initial load
 */
if(window.__INITIAL_STATE__){
    Object.keys(window.__INITIAL_STATE__).forEach(mutationName => {
        store.commit(mutationName, window.__INITIAL_STATE__[mutationName]);
    });
    cache.set(window.location.pathname + window.location.search, window.__INITIAL_STATE__);

}

export default store