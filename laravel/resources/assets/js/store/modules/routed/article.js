import Api from '../../../api'
import ApiService from '../../../services/api-service'

const state = {
    article: {}
};

const actions = {
    find({commit}, {params, next}) {
        ApiService.fetch({
            call: [Api.routed.article.find, params],
            context: {commit, next},
            autoCommit: true
        });
    }
};

const getters = {
    article: state => state.article
};

const mutations = {
    update_article(state, article) {
        state.article = article
    },
    empty_article(state) {
        state.article = { id: null, image: null, name: null, content: null }
    },
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}