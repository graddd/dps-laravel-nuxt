import Api from '../../../api'
import ApiService from '../../../services/api-service'

const actions = {
    index({commit}, {query, next}) {
        ApiService.fetch({
            call: [Api.routed.search.get, query],
            callback(response){
                console.log(response);
            },
            context: {commit, next},
            autoCommit: true
        });
    }
}

export default {
    namespaced: true,
    actions,
}