import Api from '../../../api'
import ApiService from '../../../services/api-service'

const featured_options = ['spotlight', 'top_three', 'presentation'];

const state = {
    articles_featured: {},
    articles_categorized: {},
};

const actions = {
    index({commit}, {next}) {
        ApiService.fetch({
            call: Api.routed.home.get,
            context: {commit, next},
            autoCommit: true
        });
    }
};

const getters = {
    articlesFeatured: state => state.articles_featured,
    articlesCategorized: state => state.articles_categorized,
};

const mutations = {
    update_articles_featured(state, articles_featured) {
        state.articles_featured = articles_featured;
    },
    update_articles_categorized(state, articles_categorized) {
        state.articles_categorized = articles_categorized;
    },
};


export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}