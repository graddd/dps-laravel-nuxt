import Api from '../../api'
import * as types from '../mutation-types'
import fn from '../../functions'
import _ from 'lodash'

// initial state
const state = {
    events: [],
    upcoming: [],
    selected: {
        year: new Date().getFullYear(),
        month: new Date().getMonth(),
        day: null
    },
    table: [],
    days: []
}

// getters
const getters = {
    indexEvents: state => state.events,
    indexTable: state => state.table,
    indexSelected: state => state.selected,
    indexUpcoming: state => state.upcoming,
}

// actions
const actions = {
    getEvents({ commit }) {
        Api.event.search().then(response => {
            let events = response.data.events
            commit(types.RECEIVE_EVENTS, { events })
            commit(types.UPDATE_CALENDAR_UPCOMING, { events })
            commit(types.UPDATE_CALENDAR_DAYS, { events })
            actions.generateTable({ commit, state })
        });
    },
    generateTable({commit, state}) {
        const today = new Date()
        const year = state.selected.year
        const month = state.selected.month
        const firstDay = new Date(year, month, 1)
        const selected = new Date(year, month, state.selected.day)

        let dayCounter = 0;
        let offset = 0;
        let table = [];

        /**
         * setup day counter
         * todo: no idea how this works...
         */
        if (firstDay.getDay() !== 0) {
            offset = firstDay.getDay() - 2;
            dayCounter -= offset;
        }

        /**
         * todo: No idea why i need to put month + 1 to work properly...
         * @type {number}
         */
        const monthLength = new Date(year, month + 1, 0).getDate();

        for (let row = 0; row < 42; row++, dayCounter++) {
            const fullDateOfDay = new Date(year, month, dayCounter)
            const isToday = fn.isEqualDates(fullDateOfDay, today)
            const isThisMonth = fn.isThisMonth(fullDateOfDay, firstDay)
            const formattedDate = fullDateOfDay.getFullYear()+'-'+(fullDateOfDay.getMonth()+1)+'-'+fullDateOfDay.getDate()
            const event = isThisMonth && _.includes(state.days, formattedDate)
            const picked = fn.isEqualDates(fullDateOfDay, selected) && isThisMonth
            const dayNumber = isThisMonth ? fullDateOfDay.getDate() : null
            const style = actions.addStyleToDay(dayNumber, isToday, picked, event)

            table.push({
                date: fullDateOfDay,
                dayNumber,
                today: isToday,
                haveEvent: event,
                picked,
                style,
            });

            // break so that it doesn't push in an empty row
            if(isThisMonth && fullDateOfDay.getDate() === monthLength) break
        }

        commit(types.UPDATE_CALENDAR_TABLE, { table })
    },
    nextMonth({ commit, state }) {
        let month = state.selected.month + 1
        let year = state.selected.year
        let day = null

        if(month > 11) {
           month = 0
           year++
        }

        commit(types.UPDATE_CALENDAR_SELECTED, { year, month, day })
        actions.generateTable({ commit, state })
    },
    prevMonth({ commit, state }) {
        let month = state.selected.month - 1
        let year = state.selected.year
        let day = null

        if(month < 0) {
            month = 11
            year--
        }

        commit(types.UPDATE_CALENDAR_SELECTED, { year, month, day })
        actions.generateTable({ commit, state })
    },
    setCurrentDay({commit, state}, day) {
        if(day) {
            const month = state.selected.month
            const year = state.selected.year

            commit(types.UPDATE_CALENDAR_SELECTED, { year, month, day })
            commit(types.UPDATE_CALENDAR_SELECTED_UPCOMING, { })

            actions.generateTable({ commit, state })
        }
    },
    addStyleToDay(dayNumber, today, picked, haveEvent) {
        let string = ''
        string += dayNumber ? 'has-day ' : ''
        string += today ? 'today ' : ''
        string += picked ? 'picked ' : ''
        string += haveEvent ? 'has-event ' : ''

        return string
    }
}

// mutations
const mutations = {
    [types.RECEIVE_EVENTS] (state, { events }) {
        state.events = events
    },
    [types.UPDATE_CALENDAR_TABLE] (state, { table }) {
        state.table = table
    },
    [types.UPDATE_CALENDAR_DAYS] (state, { events }) {
        let days = []
        _.each(events, function (event) {
            let start = new Date(event.start_at),
                end = new Date(event.end_at)

            _.each(fn.getDates(start,end), function (date) {
                const dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
                days.push(dateString)
            })

        })
        state.days = days
    },
    [types.UPDATE_CALENDAR_UPCOMING] (state, { events }) {
        state.upcoming = [];
        let i = 0;
        _.each(events, function (event) {
            const start_at = new Date(event.start_at)
            if(start_at >= new Date()) {
                state.upcoming.push(event);
            }
            if(i === 4) return false;
        })
    },
    [types.UPDATE_CALENDAR_SELECTED_UPCOMING] (state, { }) {
        const selected = new Date(state.selected.year, state.selected.month, state.selected.day)
        let upcoming = []

        let events = state.events

        _.each(events, function (event) {
            const starts = new Date(event.start_at)
            const ends = new Date(event.end_at)
            if(fn.isBetweenDates(selected, starts, ends)) {
                upcoming.push(event)
            }
        })
        state.upcoming = upcoming
    },
    [types.UPDATE_CALENDAR_SELECTED] (state, { year, month, day }) {
        state.selected = { year, month, day }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
