import initiative from './user/initiative'

const state = {
    page: null,
    pages: {
        'user-initiative-create': 'initiative.create',
        'user-initiative-index': 'initiative.index',
        'user-initiative-update': 'initiative.update',
        'user-profile': 'user.profile'
    },
}

const getters = {
    indexPage: state => state.page
}

const actions = {
    getPage({commit, rootState}) {
        let page = state.pages[rootState.route.name]
        commit('updatePage', page)
    },
}

const mutations = {
    updatePage(state, page) {
        state.page = page
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    modules: {
        initiative
    }
}