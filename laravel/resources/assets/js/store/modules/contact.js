import Api from '../../api'
import * as types from '../mutation-types'
import _ from 'lodash'

// initial state
const state = {
    form: {
        contact: {
            id: null,
            name: null,
            email: null,
            phone: null,
            website: null,
            facebook: null
        }
    }
}

// getters
const getters = {
    indexContactForm: state => state.form
}

// actions
const actions = {}

// mutations
const mutations = {
    updateContactForm: function(state, {field, value}) {
        Object.assign(state.form, {
            [field]: value
        });
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}