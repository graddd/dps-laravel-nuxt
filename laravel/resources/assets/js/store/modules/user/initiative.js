import form from './initiative/form'
import Api from '../../../api'
import _ from 'lodash'

const state = {
    list: [],
    current: {},
}

const getters = {
    indexList: state => state.list,
    indexCurrent: state => state.current,
    indexForm: state => state.form
}

const actions = {
    getAll({commit}) {
        Api.user.initiative.get().then(response => {
            let initiatives = response.data.initiatives
            commit('updateList', {initiatives})
        })
    },
    findCurrent({commit, rootState}) {
        let request = {
            id: rootState.route.params.id
        }

        Api.user.initiative.find(request).then(response => {
            let initiative = response.data.initiative
            commit('updateCurrent', {initiative})
        })
    },
}

const mutations = {
    updateList(state, {initiatives}) {
        state.list = initiatives
    },
    updateCurrent(state, {initiative}) {
        state.current = initiative
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    modules: {
        form
    }
}