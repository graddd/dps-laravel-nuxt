// initial state
const state = {
    showNav: false,
    links: {}
};

// getters
const getters = {
    indexHeaderNav: state => state.showNav,
    indexHeaderLinks: state => state.links,
};

// actions
const actions = {
    showNav({commit, state}) {
        let show = !state.showNav;
        commit('update_nav_show', {show})
    },
    hideNav({commit}) {
        commit('update_nav_show', false)
    },
};

// mutations
const mutations = {
    update_nav_show (state, {show}) {
        state.showNav = show
    },
    update_links (state, links) {
        state.links = links
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}