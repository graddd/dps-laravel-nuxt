import Api from '../../api'
// import * as types from '../mutation-types'

// initial state
const state = {
    articles: [],
    options: {}
}

// getters
const getters = {
    indexOptions: state => state.options
}

// actions
const actions = {
    getOptions({commit, state}) {
        Api.article.getOptions().then(response => {
            const options = response.data
            commit('updateOptions', {options})
        })
    }
};

// mutations
const mutations = {
    updateOptions(state, {options}) {
        _.each(options.categories, function (category) {
            category.label = category.name
            category.value = category.id
        })

        _.each(options.tags, function (t) {
            t.label = t.name
            t.value = t.id
        })

        state.options = options
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}