import Api from '../../api'
import router from '../../router'

// initial state
const state = {
    access_token: '',
    refresh_token: '',
    user: null,
    credentials: {
        username: '',
        password: ''
    },
    checked: false,
    errors: null,
    authenticated: false,
}

// getters
const getters = {
    indexUser: state => state.user,
    indexCredentials: state => state.credentials,
    indexErrors: state => state.errors,
    indexAuthenticated: state => state.authenticated
}

// actions
const actions = {
    logout({commit}) {
        localStorage.removeItem('access_token')

        actions.checkFacebookLogin(() => {
            FB.logout();
        })

        commit('UPDATE_AUTHENTICATED', false)
        commit('UPDATE_USER', {user: null})
        commit('UPDATE_TOKENS', {access_token: null, refresh_token: null})

        router.push('/home')
    },
    checkFacebookLogin(callback) {
        /**
         * todo! bad hack
         */
        setTimeout(() => {
            if (fbAsyncInit === 'loaded') {
                FB.getLoginStatus(response => {
                    if (response.status === 'connected') {
                        callback()
                    }
                })
            }
        })
        // fbAsyncInit().then(() => {
        //     FB.getLoginStatus(response => {
        //         if (response.status === 'connected') {
        //             callback()
        //         }
        //     })
        // })
    },
    checkLogin({commit}) {
        const access_token = localStorage.getItem('access_token')
        const refresh_token = localStorage.getItem('refresh_token')

        Api.auth.setAxiosDefaults({access_token})

        return actions.getCurrentUser({commit}).then(result => {
            if (result) {
                commit('UPDATE_TOKENS', {access_token, refresh_token})
                return true;
            }
            else return false;
        })
    },
    facebookAuth({commit, state}) {
        Api.auth.facebook().then(response => {
            const access_token = response.data.access_token

            Api.auth.setAxiosDefaults({access_token})

            actions.getCurrentUser({commit, state})

            commit('UPDATE_TOKENS', {access_token, refresh_token: null})
            commit('UPDATE_AUTHENTICATED', true)

            router.push({name: 'user-profile'})
        })
    },
    facebookLogin({commit, state}) {
        FB.login(response => {
            actions.facebookAuth({commit, state})
        }, {scope: 'email, public_profile'});
    },
    credentialsLogin({commit, state}) {
        let request = {
            grant_type: 'password',
            username: state.credentials.username,
            password: state.credentials.password,
        }

        Api.auth.login(request).then(response => {
            const access_token = response.data.access_token;
            const refresh_token = response.data.refresh_token;

            Api.auth.setAxiosDefaults({access_token})
            actions.getCurrentUser({commit})

            commit('UPDATE_TOKENS', {access_token, refresh_token})

            router.push({name: 'user-profile'})
        }).catch(error => {
            commit('UPDATE_ERRORS', {error: error.response.data})
        })
    },
    getCurrentUser({commit}) {
        return Api.auth.getUser().then(response => {
            const user = response.data.user

            commit('UPDATE_USER', {user})
            commit('UPDATE_AUTHENTICATED', true)

            return user

        }).catch(error => {
            return null
        })
    }
}

// mutations
const mutations = {
    UPDATE_TOKENS (state, {access_token, refresh_token}) {
        state.access_token = access_token
        state.refresh_token = refresh_token

        localStorage.setItem('access_token', access_token)
        localStorage.setItem('refresh_token', refresh_token)
    },
    UPDATE_USER (state, {user}) {
        state.user = user;
    },
    UPDATE_PASSWORD(state, password) {
        state.credentials.password = password
    },
    UPDATE_USERNAME(state, username) {
        state.credentials.username = username
    },
    UPDATE_ERRORS(state, {error}) {
        state.errors = error
    },
    UPDATE_AUTHENTICATED(state, auth) {
        state.authenticated = !!auth
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}