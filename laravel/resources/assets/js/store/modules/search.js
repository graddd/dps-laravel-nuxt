import Api from '../../api'
import router from '../../router'
import _ from 'lodash'

const state = {
    query: '',
    resultsPerPage: 3,
    article: {
        toggle: false,
        filters: {
            categories: [],
            tags: []
        },
        list: [],
        results: [],
        sort: '_score',
        order: 'desc'
    },
    initiative: {
        toggle: false,
        filters: {
            categories: [],
            audience: [],
            tags: []
        },
        list: [],
        results: [],
        sort: '_score',
        order: 'desc'
    },
    event: {
        toggle: false,
        filters: {},
        list: [],
        results: [],
    }
}

const getters = {
    indexQuery: state => state.query,
    indexArticleResults: state => state.article.results,
    indexArticleList: state => state.article.list,
    indexArticleToggle: state => state.article.toggle,
    indexInitiativeResults: state => state.initiative.results,
    indexInitiativeList: state => state.initiative.list,
    indexInitiativeToggle: state => state.initiative.toggle,
    indexEventResults: state => state.event.results,
    indexEventList: state => state.event.list,
}

const actions = {
    searchAll({commit, state}) {
        // actions.searchInitiatives({commit, state})
        actions.searchArticles({commit, state})
        actions.searchEvents({commit, state})

        router.push({name: 'pages-search'})
    },
    searchInitiatives({commit, state}) {
        let request = {
            search: state.query,
            ...state.initiative.filters,
            sort: state.initiative.sort,
            order: state.initiative.order
        }
        Api.initiative.get(request).then(response => {
            const initiatives = response.data.initiatives
            commit('UPDATE_INITIATIVE_LIST', initiatives)
        })
    },
    searchArticles({commit, state}) {
        let request = {
            search: state.query,
            ...state.article.filters,
            sort: state.article.sort,
            order: state.article.order
        }
        Api.article.search(request).then(response => {
            const articles = response.data.articles
            commit('UPDATE_ARTICLE_LIST', articles)
        })
    },
    searchEvents({commit, state}) {
        let request = {
            search: state.query
        }
        Api.event.search(request).then(response => {
            const events = response.data.events
            commit('UPDATE_EVENT_LIST', events)
        })
    },
    // initiative
    updateInitiativeFilter({commit, state}, {field, value}) {
        commit('UPDATE_INITIATIVE_FILTER', {field, value})
        actions.searchInitiatives({commit, state})
    },
    updateInitiativeSort({commit, state}, value) {
        commit('UPDATE_INITIATIVE_SORT', value)
        actions.searchInitiatives({commit, state})
    },
    updateInitiativeOrder({commit, state}, value) {
        commit('UPDATE_INITIATIVE_ORDER', value)
        actions.searchInitiatives({commit, state})
    },
    // article
    updateArticleFilter({commit, state}, {field, value}) {
        commit('UPDATE_ARTICLE_FILTER', {field, value})
        actions.searchArticles({commit, state})
    },
    updateArticleSort({commit, state}, value) {
        commit('UPDATE_ARTICLE_SORT', value)
        actions.searchArticles({commit, state})
    },
    updateArticleOrder({commit, state}, value) {
        commit('UPDATE_ARTICLE_ORDER', value)
        actions.searchArticles({commit, state})
    }
}

const mutations = {
    UPDATE_QUERY(state, query) {
        state.query = query
    },
    UPDATE_EVENT_LIST(state, events) {
        state.event.list = events
    },
    UPDATE_EVENT_RESULTS(state, results) {
        state.event.results = results
    },
    // article
    UPDATE_ARTICLE_LIST(state, articles) {
        state.article.list = articles
    },
    UPDATE_ARTICLE_RESULTS(state, results) {
        state.article.results = results
    },
    UPDATE_ARTICLE_TOGGLE(state) {
        state.article.toggle = !state.article.toggle
    },
    UPDATE_ARTICLE_FILTER(state, {field, value}) {
        state.article.filters[field] = value
    },
    UPDATE_ARTICLE_SORT(state, value) {
        state.article.sort = value
    },
    UPDATE_ARTICLE_ORDER(state, value) {
        state.article.order = value
    },
    // initiative
    UPDATE_INITIATIVE_TOGGLE(state) {
        state.initiative.toggle = !state.initiative.toggle
    },
    UPDATE_INITIATIVE_LIST(state, initiatives) {
        state.initiative.list = initiatives
    },
    UPDATE_INITIATIVE_RESULTS(state, results) {
        state.initiative.results = results
    },
    UPDATE_INITIATIVE_FILTER(state, {field, value}) {
        state.initiative.filters[field] = value
    },
    UPDATE_INITIATIVE_SORT(state, value) {
        state.initiative.sort = value
    },
    UPDATE_INITIATIVE_ORDER(state, value) {
        state.initiative.order = value
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}