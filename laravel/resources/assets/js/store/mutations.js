export default {
    load (state){
        state.loading = true;
        // state.failed = false;
        // state.failedResponse = null;
    },
    fail (state, error){
        let html = document.createElement('html');
        html.innerHTML = error.response.data;
        let failedResponse = html.getElementsByTagName('body')[0].innerHTML;

        state.loading = false;
        state.failed = true;
        state.failedResponse = failedResponse
    },
    success (state){
        state.loading = false;
        state.failed = false;
        state.failedResponse = null;
    },

    // initial load mutation
    updateInitialLoad (state, initialLoad){
        state.initialLoad = initialLoad;
        // state.failed = false;
        // state.failedResponse = null;
    },

    update_metadata(state, metadata) {
        state.metadata = metadata;
    }
}