export const update_filters_data = 'update_filters_data';

export const update_bounds = 'update_bounds';
export const update_center = 'update_center';
export const update_search_query = 'update_search_query';
export const update_filters = 'update_filters';
export const update_filters_categories = 'update_filters_categories';
export const update_filters_audiences = 'update_filters_audiences';

// initiatives
export const UPDATE_INITIATIVES_FILTERS = 'UPDATE_INITIATIVES_FILTERS';
export const UPDATE_SIDEBAR_CATEGORIES = 'UPDATE_SIDEBAR_CATEGORIES';
export const UPDATE_SIDEBAR_AUDIENCE = 'UPDATE_SIDEBAR_AUDIENCE';
export const UPDATE_SIDEBAR_SEARCH = 'UPDATE_SIDEBAR_SEARCH';
export const UPDATE_MARKERS = 'UPDATE_MARKERS';
export const UPDATE_SEARCH_QUERY = 'UPDATE_SEARCH_QUERY';
export const TOGGLE_SIDEBAR_FILTERS = 'TOGGLE_SIDEBAR_FILTERS';

// articles
export const RECEIVE_ARTICLES = 'RECEIVE_ARTICLES'
export const EMPTY_ARTICLE = 'EMPTY_ARTICLES'
export const UPDATE_ARTICLE = 'UPDATE_ARTICLE'

// calendar
export const RECEIVE_EVENTS = 'RECEIVE_EVENTS'
export const UPDATE_CALENDAR_TABLE = 'UPDATE_CALENDAR_TABLE'
export const UPDATE_CALENDAR_SELECTED_MONTH = 'UPDATE_CALENDAR_SELECTED_MONTH'
export const UPDATE_CALENDAR_SELECTED_YEAR = 'UPDATE_CALENDAR_SELECTED_YEAR'
export const UPDATE_CALENDAR_SELECTED = 'UPDATE_CALENDAR_SELECTED'
export const UPDATE_CALENDAR_UPCOMING = 'UPDATE_CALENDAR_UPCOMING'
export const UPDATE_CALENDAR_SELECTED_UPCOMING = 'UPDATE_CALENDAR_SELECTED_UPCOMING'
export const UPDATE_CALENDAR_DAYS = 'UPDATE_CALENDAR_DAYS'
