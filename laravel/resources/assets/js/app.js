require('./bootstrap')
/**
 * Import modules
 */

import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import VeeValidate from 'vee-validate'
import VueProgressBar from 'vue-progressbar'
import VueMeta from 'vue-meta'
import VueScrollTo from 'vue-scrollto';
/**
 * Vue JS internationalization
 */
import i18n from './i18n'

/**
 * Vue JS router
 */
import router from './router'

/**
 * Vuex vue store
 */
import store from './store'

/**
 * Vue router sync
 */
sync(store, router)

/**
 * Vue JS form validation, scroll to, progress bar and meta components
 */

Vue.use(VeeValidate);
Vue.use(VueProgressBar, {
    color: '#6aab88',
    failedColor: '#a05956',
    thickness: '8px',
    transition: {
        speed: '0.2s',
        opacity: '0.1s',
        termination: 300
    },
    autoRevert: false,
    inverse: false
});
Vue.use(VueScrollTo, {
    container: "body",
    duration: 1000,
    easing: "ease",
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false
})
Vue.use(VueMeta, {
    keyName: 'metaInfo', // the component option name that vue-meta looks for meta info on.
    attribute: 'data-vue-meta', // the attribute name vue-meta adds to the tags it observes
    tagIDKeyName: 'vmid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
});
Vue.use(require('vue-truncate-filter'));

/**
 * Create Vue instance
 *
 * @type {Vue}
 */
window.Vue = new Vue({
    el: '#app',
    store,
    router,
    i18n,
    components: {
        'spa-container': require('./containers/App.vue')
    }
});
