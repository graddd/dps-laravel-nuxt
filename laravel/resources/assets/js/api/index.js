import axios from 'axios'
import ApiService from '../services/api-service';
let cache = require('js-cache');

const cacheable = window.__ENV__ === 'production';

/**
 * ANALYZE THIS SHIT
 */

// On request, return the cached version, if any
axios.interceptors.request.use(request => {
    // Only cache GET requests
    if (request.method === 'get' && cacheable) {
        let url = ApiService.makeCacheURL(request.url, request.params)
        const _cached = cache.get(url);

        if (_cached) {
            // _cached.__fromCache = true;
            console.log(`"${url}" served from cache:`, _cached);

            request.data = _cached;

            // Set the request adapter to send the cached response and prevent the request from actually running
            request.adapter = () => {
                return Promise.resolve({
                    data: _cached,
                    status: request.status,
                    statusText: request.statusText,
                    headers: request.headers,
                    config: request,
                    request: request
                });
            };
        }
    }

    return request;

}, error => Promise.reject(error));

// On response, set or delete the cache
axios.interceptors.response.use(response => {
    // if you dont want to cache a specific url, send a param `__cache = false`
    const isCacheable = !response.config.params || (response.config.params && response.config.params.__cache !== false);
    if (cacheable && isCacheable) {
        let url = ApiService.makeCacheURL(response.config.url, response.config.params);

        if (response.config.method === 'get') {
            // console.log(url);
            // On get request, store the response in the cache
            cache.set(url, response.data);
        } else {
            // For post, put or delete, just delete the cached version of the url
            // e.g. posting to `/users` would delete the `/users` cache, so when you ask for users again you get the real version
            cache.del(response.config.url);

            // also, when making a post,put or delete request to `/users/1`, would try to delete the `/users` for the same reason
            const uri = url.replace(config.http.api.base_url, ''),
                parentUri = /(.*)\/([a-z0-9\-]+)\/?$/ig.exec(uri);

            if (parentUri)
                cache.del(`${config.http.api.base_url}${parentUri[1]}`);

            // Delete similar url that just have query string diferences
            // Specially useful for things like Laravel's `with` param
            // e.g. `/users?with=permissions` will get cached but the post to `/users` wont remove it from the cache
            // so I look all the cached url's and try to match it without the querystrings
            const urls = Object.keys(cache.debug());

            for (const _url of urls) {
                if (_url.match(/^[^?]+/)[0] === response.config.url)
                    cache.del(_url);
            }
        }
    }

    return response;
}, error => Promise.reject(error));

export default {
    routed: {
        home: {
            get() {
                return axios.get('spa/')
            }
        },
        article: {
            find(params) {
                return axios.get('spa/article/' + params.slug)
            }
        },
        search: {
            get(params) {
                return axios.get('spa/search', {params})
            }
        }
    },
    user: {
        initiative: {
            get() {
                return axios.get('user/initiative')
            },
            find(params) {
                return axios.get('user/initiative/' + params.id)
            },
            create(params) {
                return axios.post('user/initiative', params)
            },
            update(params) {
                return axios.put('user/initiative/' + params.id, {params})
            }
        },

    },
    event: {
        get(params) {
            return axios.get('event', {params})
        },
    },
    article: {
        get(params) {
            return axios.get('article', {params})
        },
        search(params) {
            return axios.get('article/search', {params})
        },
        getFeaturedAndCategorized(params) {
            return axios.get('article', {params})
        },
        // find(params) {
        //     return axios.get('article/' + params.slug)
        // },
        getOptions() {
            return axios.get('article/options')
        }
    },
    initiative: {
        get(params) {
            return axios.get('initiative', {params})
        },
        find(params) {
            return axios.get('initiative/' + params.id)
        },
        getOptions() {
            return axios.get('initiative/options')
        },
    },
    auth: {
        login(params) {
            return axios.post('auth/password', params)
        },
        setAxiosDefaults(params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + params.access_token;
        },
        getUser() {
            return axios.get('user')
        },
        facebook() {
            return axios.get('auth/facebook')
        }
    },
}