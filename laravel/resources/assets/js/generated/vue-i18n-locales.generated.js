export default {
    "sl": {
        "email": {
            "greeting": "Živjo!",
            "greeting_error": "Ojoj!",
            "regards": "Lep pozdrav,",
            "action_trouble": "Če imate težave z odpiranjem povezave \"{action}\", kopirajte naslednjo povezavo v vaš brskalnik:",
            "all_rights_reserved": "Vse pravice pridržane.",
            "password": {
                "subject": "Ponastavitev gesla",
                "description_start": "To pošto ste prejeli, ker je bila za vaš e-poštni naslov zahtevana ponastavitev gesla.",
                "action": "Ponastavi geslo",
                "description_end": "Če ponastavitve niste zahtevali, lahko to sporočilo ignorirate."
            }
        },
        "initiative": {
            "map": {
                "sidebar": {
                    "search": {
                        "placeholder": "Iskanje po organizacijah ...",
                        "action": "Išči"
                    },
                    "filters": {
                        "title": "Nastavitev filtrov",
                        "choose_categories": "Izberi željene katergorije",
                        "choose_audience": "Ciljna publika organizacije",
                        "clear": "Odstrani vse filtre in iskalne besede"
                    },
                    "list": {
                        "no_results": "Vašim iskalnim kriterijem ne ustreza nobena organizacija."
                    }
                }
            },
            "location_type": {
                "urban": "Urbano območje",
                "rural": "Ruralno območje",
                "urban_and_rural": "Urbano in ruralno območje"
            },
            "status": {
                "germinating": "Začetna idejno-načrtovalna faza",
                "taking_root": "Ukoreninjanje in izvrševanje zadanih ciljev",
                "fruiting": "Razvidni plodovi dela",
                "seeding": "Ustvarjanje aktivnih podmladkov"
            }
        },
        "register": {
            "title": "Registracija",
            "name": "Ime in priimek",
            "email": "E-poštni naslov",
            "password": "Geslo",
            "confirm_password": "Ponovi geslo",
            "submit": "Registracija"
        },
        "passwords": {
            "password": "Geslo se mora ujemati, dolgo pa mora biti vsaj 6 znakov.",
            "reset": "Vaše geslo je bilo uspešno spremenjeno.",
            "sent": "Na vaš e-poštni naslov smo vam poslali povezavo do ponastavitve gesla.",
            "token": "Neveljaven ključ za ponastavitev gesla.",
            "user": "Uporabnik s tem e-poštnim naslovom ne obstaja.",
            "email_page": {
                "title": "Ponastavitev gesla",
                "email": "E-poštni naslov",
                "submit": "Pošlji povezavo do ponastavitve gesla"
            },
            "reset_page": {
                "title": "Ponastavitev gesla",
                "email": "E-poštni naslov",
                "password": "Geslo",
                "confirm_password": "Ponovi geslo",
                "submit": "Ponastavi geslo"
            }
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "navbar": {
            "login": "Prijava",
            "administration": "Administracija",
            "logout": "Odjava"
        },
        "login": {
            "title": "Prijava",
            "register": "Registracija",
            "email": "E-poštni naslov",
            "password": "Geslo",
            "forgot_password": "Pozabljeno geslo?",
            "remember_me": "Zapomni si me",
            "submit": "Prijava",
            "social_or_not": "-- Ali pa se prijavi z e-poštnim naslovom in geslom --",
            "social": {
                "facebook": "Prijava s Facebook profilom",
                "google": "Prijava z Google računom"
            }
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "validation": {
            "accepted": "{attribute} mora biti sprejet.",
            "active_url": "{attribute} ni pravilen.",
            "after": "{attribute} mora biti za datumom {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "{attribute} lahko vsebuje samo črke.",
            "alpha_dash": "{attribute} lahko vsebuje samo črke, številke in črtice.",
            "alpha_num": "{attribute} lahko vsebuje samo črke in številke.",
            "array": "{attribute} mora biti polje.",
            "before": "{attribute} mora biti pred datumom {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "{attribute} mora biti med {min} in {max}.",
                "file": "{attribute} mora biti med {min} in {max} kilobajti.",
                "string": "{attribute} mora biti med {min} in {max} znaki.",
                "array": "{attribute} mora imeti med {min} in {max} elementov."
            },
            "boolean": "{attribute} polje mora biti 1 ali 0",
            "confirmed": "{attribute} potrditev se ne ujema.",
            "date": "{attribute} ni veljaven datum.",
            "date_format": "{attribute} se ne ujema z obliko {format}.",
            "different": "{attribute} in {other} mora biti drugačen.",
            "digits": "{attribute} mora imeti {digits} cifer.",
            "digits_between": "{attribute} mora biti med {min} in {max} ciframi.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "{attribute} mora biti veljaven e-poštni naslov.",
            "exists": "izbran {attribute} je neveljaven.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field is required.",
            "image": "{attribute} mora biti slika.",
            "in": "izbran {attribute} je neveljaven.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "{attribute} mora biti število.",
            "ip": "{attribute} mora biti veljaven IP naslov.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "{attribute} ne sme biti večje od {max}.",
                "file": "{attribute} ne sme biti večje {max} kilobajtov.",
                "string": "{attribute} ne sme biti večje {max} znakov.",
                "array": "{attribute} ne smejo imeti več kot {max} elementov."
            },
            "mimes": "{attribute} mora biti datoteka tipa: {values}.",
            "mimetypes": "{attribute} mora biti datoteka tipa: {values}.",
            "min": {
                "numeric": "{attribute} mora biti vsaj dolžine {min}.",
                "file": "{attribute} mora imeti vsaj {min} kilobajtov.",
                "string": "{attribute} mora imeti vsaj {min} znakov.",
                "array": "{attribute} mora imeti vsaj {min} elementov."
            },
            "not_in": "izbran {attribute} je neveljaven.",
            "numeric": "{attribute} mora biti število.",
            "present": "The {attribute} field must be present.",
            "regex": "Format polja {attribute} je neveljaven.",
            "required": "Polje {attribute} je zahtevano.",
            "required_if": "Polje {attribute} je zahtevano, ko {other} je {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "Polje {attribute} je zahtevano, ko je {values} prisoten.",
            "required_with_all": "Polje {attribute} je zahtevano, ko je {values} prisoten.",
            "required_without": "Polje {attribute} je zahtevano, ko {values} ni prisoten.",
            "required_without_all": "Polje {attribute} je zahtevano, ko nobenih od {values} niso prisotni.",
            "same": "Polje {attribute} in {other} se morata ujemati.",
            "size": {
                "numeric": "{attribute} mora biti {size}.",
                "file": "{attribute} mora biti {size} kilobajtov.",
                "string": "{attribute} mora biti {size} znakov.",
                "array": "{attribute} mora vsebovati {size} elementov."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "{attribute} je že zaseden.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "{attribute} format je neveljaven.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    },
    "en": {
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field is required.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    }
}
