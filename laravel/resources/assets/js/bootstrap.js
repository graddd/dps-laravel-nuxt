/**
 * Globally available packages
 */

window.$ = window.jQuery = require('jquery')
require('sticky-kit/dist/sticky-kit')
window._ = require('lodash')
require('./facebook')

require('bootstrap-sass')

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
window.axios.defaults.baseURL = '/api/';
