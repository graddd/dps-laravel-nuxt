import store from '../store/'

export default {
    /**
     * This is a service that helps wrap axios promise resolvement into a developer-friendly form that helps evade
     * repetitive code. Also helps Vue router prevent fetching content when initial load was supplied with
     * initial state data.
     *
     * Use example: Place wherever you would like to make an axios call (typically in vuex action).
     * ApiService.fetch({
            call: [Api.article.find, params], // or just Api.article.find or {article: Api.article.find} or {article: [Api.article.find, params]}
            callback(response) { // not necessary if autoCommit option present
                let article = response.data.article;
                commit('article/update_article', {article}, {root: true});
            },
            context: {commit, to, next},
            autoCommit: false
        });
     */

    metadataInfo: {
        commitName: 'update_metadata',
        headerAttr: 'x-metadata-json'
    },
    prependedSpaURL: '/api/spa',
    fetch({call, callback, context, autoCommit = true}) {
        // initialLoad is true by default in store/index.js
        if (store.state.initialLoad) {
            context.commit('updateInitialLoad', false, {root: true});
            context.next();
        } else {
            /**
             * Call (whether there is a single call or multiple calls) must be performed here and not
             * before, otherwise it will fetch on initial load as well.
             */
            let callerPromise = this.resolveCall(call);

            callerPromise.then(response => {
                let resolvedResponse = this.resolveResponse({call, response});
                if(callback === Function){
                    callback(resolvedResponse);
                }
                if(autoCommit){
                    this.autoCommit({context, response});
                }
                context.commit('success', null, {root: true});
                context.next();
            }, error => {
                console.error(error);
                context.commit('fail', error, {root: true});
                context.next()
            });
        }
    },

    /**
     * Inputs a call argument and spits out correspondent promise.
     *
     * @param call
     */
    resolveCall(call) {
        let callerPromise;

        switch (call.constructor) {
            case Function:
                callerPromise = call();
                break;
            case Array:
                callerPromise = this.callerFromArray(call);
                break;
            case Object:
                callerPromise = Promise.all(Object.keys(call).map((key) => {
                    return call[key].constructor === Array ? this.callerFromArray(call[key]) : call[key]();
                }));
                break;
            default:
                console.error('api-service: Call attribute must be either a function, array or an object.');
                return false;
                break;
        }
        return callerPromise;
    },

    callerFromArray(array) {
        // helper function
        let fn = array.find((el) => {return el.constructor === Function}),
            params = array.find((el) => {return el.constructor !== Function}); // whatever is passed to array as non-function is params

        return fn(params);
    },

    /**
     * Makes an object with call names as keys when executing multiple ajax calls per one Promise.
     *
     * @param call
     * @param response
     */
    resolveResponse({call, response}) {
        if(response.constructor === Array){
            let responseKeyed = {};
            Object.keys(response).forEach((key) => {
                let namedKey = Object.keys(call)[key];
                responseKeyed[namedKey] = response[key];
            });
            response = responseKeyed
        }

        return response;
    },

    /**
     * Automatically commits response data. Response data must be an object of form
     * {first_commit_name: first_commit_data, ...} for single call or [{first_commit_name: first_commit_data, ...}, ...]
     * for multuple ajax calls. Meta data also gets committed.
     *
     * @param context
     * @param response
     */
    autoCommit({context, response}) {
        let self = this;
        function commitSingleReponse(singleResponse) {
            let singleResponseData = singleResponse.data;
            Object.keys(singleResponseData).forEach((commitName) => {
                if(store._mutations[commitName]) {
                    context.commit(commitName, singleResponseData[commitName], {root: true});
                    console.log('api-service autocommit: Mutation', commitName, 'committed with value', singleResponseData[commitName])
                } else { console.error('api-service autocommit: Mutation', commitName, 'does not exist.') }
            });
            // commit metadata, stored in response header is useless because meta is always present in response body

            // if(singleResponse.headers[self.metadataInfo.headerAttr]){
            //     context.commit(self.metadataInfo.commitName, JSON.parse(singleResponse.headers[self.metadataInfo.headerAttr]), {root: true})
            // }
        }
        if(response.constructor === Array) {
            // mutiple callers
            for(var singleResponse of response){
                commitSingleReponse(singleResponse);
            }
        }else{
            commitSingleReponse(response);
        }
    },
    makeCacheURL(url, params) {
        if (params)
            url += '?' + $.param(params);
        url = url.replace(this.prependedSpaURL, ''); // ajax calls must be cached with the same key as initial state
        return url;
    }
}


