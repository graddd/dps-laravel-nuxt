import * as _ from "lodash";
export default class InitiativeMapService {
    /**
     * Constructor.
     */
    constructor() {
    }

    /**
     * Returns objects in store state that correspond to given id-keyed dictionary of objects.
     *
     * @param entities
     * @param objectsKeyed
     */
    static findCorrespondingEntities (entities, objectsKeyed) {
        return _.reduce(entities, function(entities, e) {
            if(e.id in objectsKeyed){
                entities[e.id] = e;
            }
            return entities;
        }, {});
    }

    /**
     * Returns ratio of sidebar width to body width.
     *
     * @returns {number}
     */
    static sidebarToBodyRatio() {
        return $('.sidebar').outerWidth() / $('body').width()
    }

    /**
     * Returns sidebar width in degrees.
     *
     * @param bounds
     * @param proportionStretch
     * @returns {number}
     */
    static sidebarWidthDeg(bounds, proportionStretch = false) {
        let ratio = this.sidebarToBodyRatio(),
            westLng = bounds.getSouthWest().lng(),
            eastLng = bounds.getNorthEast().lng(),
            divisor = proportionStretch ? (1 - ratio) : 1;
        return (eastLng - westLng) * ratio / divisor
    }

    /**
     * Returns LatLngBounds used for focusing multiple markers that correspond to an initiative.
     *
     * @param markers
     * @returns {L.LatLngBounds|o.LatLngBounds}
     */
    static multipleMarkerBounds(markers) {
        // fill bounds with markers
        let bounds = new google.maps.LatLngBounds();
        _.each(markers, function (marker) {
            bounds.extend(marker.position)
        });
        return this.extendBoundsForSidebar(bounds);
    }

    /**
     * Returns LatLngBounds appropriated for sidebar that lies within the map.
     *
     * @param bounds
     * @returns {L.LatLngBounds|o.LatLngBounds}
     */
    static extendBoundsForSidebar(bounds) {
        // add sidebar's width in lng (degrees)
        let sidebarSpaceLng = InitiativeMapService.sidebarWidthDeg(bounds, true);
        bounds.extend({
            lat: bounds.getSouthWest().lat(),
            lng: bounds.getSouthWest().lng() - sidebarSpaceLng * 1.2 // factor here only to extend a bit further.
        });

        return bounds
    }
}