import Vue from 'vue'
import VueRouter from 'vue-router'

/**
 * Containers
 */
import Index from './containers/Index.vue'
import Main from './containers/Main.vue'
// import User from './containers/User.vue'

/**
 * Index view
 */
import Home from './views/index/Home.vue'
//
// /**
//  * Pages
//  */
// import Ethics from './views/pages/Ethics.vue'
// import AboutUs from './views/pages/AboutUs.vue'
// import Planning from './views/pages/Planning.vue'
// import Fields from './views/pages/Fields.vue'
// import Literature from './views/pages/Literature.vue'
import Article from './views/pages/Article.vue'
import Calendar from './views/pages/Events.vue'
import SearchView from './views/pages/SearchView.vue'
//
// /**
//  * Initiative routes
//  */
import InitiativeIndex from './views/pages/initiative/Index.vue'
import InitiativeShow from './views/pages/initiative/Show.vue'
import InitiativeMicelij from './views/pages/initiative/Micelij.vue'
//
// /**
//  * User routes
//  */
import AuthLogin from './views/auth/Login.vue'
import AuthRegister from './views/auth/Register.vue'
// import UserProfile from './views/user/Profile.vue'
// import InitiativeCreate from './views/user/initiative/Create.vue'
// import InitiativeUpdate from './views/user/initiative/Update.vue'
// import UserInitiativeIndex from './views/user/initiative/Index.vue'

/**
 * Vuex store
 *
 * used before entering the user sub-domain under user_routes
 */
import store from './store'

Vue.use(VueRouter);

const index_routes = [
    {
        path: '/',
        component: Index,
        children: [
            {
                path: '',
                name: 'index-home',
                component: Home,
                beforeEnter(to, from, next) {
                    store.dispatch('routed/home/index', {next})
                }
            }
        ],
    }
]

const main_routes = [
    {
        path: '/',
        component: Main,
        children: [
//             {
//                 path: 'ethics',
//                 name: 'pages-ethics',
//                 component: Ethics
//             },
//             {
//                 path: 'about-us',
//                 name: 'pages-about-us',
//                 component: AboutUs
//             },
//             {
//                 path: 'planning',
//                 name: 'pages-planning',
//                 component: Planning
//             },
//             {
//                 path: 'fields',
//                 name: 'pages-fields',
//                 component: Fields
//             },
//             {
//                 path: 'literature',
//                 name: 'pages-literature',
//                 component: Literature
//             },
            {
                path: 'article/:slug',
                name: 'pages-article-show',
                component: Article,
                beforeEnter(to, from, next) {
                    let params = to.params;
                    store.dispatch('routed/article/find', {params, next})
                }
            },
            {
                path: 'calendar',
                name: 'pages-calendar',
                component: Calendar
            },

            {
                path: 'search',
                name: 'pages-search',
                component: SearchView,
                beforeEnter(to, from, next) {
                    let query = to.query;
                    store.dispatch('routed/search/index', {query, next})
                }
            },

            /**
             * Auth routes
             */
            {
                path: 'login',
                name: 'auth-login',
                component: AuthLogin
            },
            {
                path: 'register',
                name: 'auth-register',
                component: AuthRegister
            },

            /**
             * Initiative routes
             */
            {
                path: 'initiative',
                name: 'pages-initiative-index',
                component: InitiativeIndex
            },
            {
                path: 'initiative/:id',
                name: 'pages-initiative-show',
                component: InitiativeShow
            },
        ],
    },
]
//
// const user_routes = [
//     {
//         path: '/user',
//         component: User,
//         children: [
//             {
//                 path: '',
//                 redirect: 'profile'
//             },
//             {
//                 path: 'profile',
//                 name: 'user-profile',
//                 component: UserProfile
//             },
//             {
//                 path: 'initiative',
//                 name: 'user-initiative-index',
//                 component: UserInitiativeIndex
//             },
//             {
//                 path: 'initiative/create',
//                 name: 'user-initiative-create',
//                 component: InitiativeCreate
//             },
//             {
//                 path: 'initiative/update/:id',
//                 name: 'user-initiative-update',
//                 component: InitiativeUpdate
//             },
//             {
//                 path: 'initiative/:id',
//                 name: 'user-initiative-show',
//                 component: InitiativeCreate
//             },
//             {
//                 path: '*', redirect: 'profile'
//             }
//         ],
//         beforeEnter: (to, from, next) => {
//             store.dispatch('auth/checkLogin').then(response => {
//                 if (response) next()
//                 else next('/login')
//             })
//         }
//     },
// ]

const other_routes = [
    {
        path: '/micelij',
        name: 'pages-initiative-micelij',
        component: InitiativeMicelij
    },
]

const routes = [
    ...index_routes,
    ...main_routes,
    // ...user_routes,
    ...other_routes,
]

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        // return { x: 0, y: 0 }
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
});

export default router