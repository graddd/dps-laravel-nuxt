<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>{{ $meta['document_title'] }}</title>
<meta data-vmid="description" data-vue-meta="true" name='description' content='{{ $meta['description']  }}' />
<meta data-vmid="keywords" data-vue-meta="true" name='keywords' content='{{ $meta['keywords'] }}' />
<meta data-vmid="og_title" data-vue-meta="true" property="og:title" content="{{ $meta['title'] }}" />
<meta data-vmid="og_description" data-vue-meta="true" property='og:description' content='{{ $meta['description']  }}' />
<meta data-vmid="og_image" data-vue-meta="true" property='og:image' content='{{ url($meta['image'])  }}' />
<meta data-vmid="og_url" data-vue-meta="true" property="og:url" content="{{ URL::current() }}" />
<meta data-vmid="og_site_name" data-vue-meta="true" property="og:site_name" content="{{ $meta['domain'] }}">
<meta data-vmid="og_type" data-vue-meta="true" property="og:type" content="{{ $meta['type'] }}">
<meta data-vmid="og_locale" data-vue-meta="true" property="og:locale" content="sl_SI" />
{{-- Favicon/device-specific "pinned" icons and such --}}
<link rel="apple-touch-icon" sizes="180x180" href="{{ url('/images/icons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('/images/icons/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ url('/images/icons/favicon-16x16.png') }}">
<link rel="manifest" href="{{ url('/images/icons/manifest.json') }}">
<link rel="mask-icon" href="{{ url('/images/icons/safari-pinned-tab.svg') }}" color="#6aab88">
<meta name="msapplication-TileColor" content="#2a4047">
<meta name="msapplication-TileImage" content="{{ url('/images/icons//mstile-144x144.png') }}">
<meta name="theme-color" content="#2a4047">
{{-- CSRF token for ajax requests --}}
<meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ url('/css/app.css') }}" media="screen">