<script>
    @isset($initial_state)
        window.__ENV__ = '{{ getenv('APP_ENV') }}';
        window.__INITIAL_STATE__ = ({!! $initial_state !!});
    @endisset
</script>
<script src="{{ url('/js/app.js') }}" type="text/javascript"></script>
