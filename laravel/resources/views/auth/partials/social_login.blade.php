<div class="social-auth-links text-center">
{{--    <a href="{{ url('/auth/github') }}" class="btn btn-block btn-social btn-github btn-flat"><i class="fa fa-github"></i> Sign in using Github</a>--}}
    <a href="{{ url('/auth/facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat"><span class="fa fa-facebook"></span> {{ trans('login.social.facebook') }}</a>
{{--    <a href="{{ url('/auth/twitter') }}" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Sign in using Twitter</a>--}}
    <a href="{{ url('/auth/google') }}" class="btn btn-block btn-social btn-google btn-flat"><span class="fa fa-google"></span> {{ trans('login.social.google') }}</a>
    <div class="social-or-not">
        {{ trans('login.social_or_not') }}
    </div>
</div>

