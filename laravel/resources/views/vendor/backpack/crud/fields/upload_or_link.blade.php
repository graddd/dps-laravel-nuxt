<!-- PAGE OR LINK field -->
<!-- Used in Backpack\MenuCRUD -->

<?php
$field['options'] = ['attachment' => trans('backpack::crud.attachment'), 'internal_link' => trans('backpack::crud.internal_link'), 'external_link' => trans('backpack::crud.external_link')];
//    $field['allows_null'] = false;
// add upload
$hasAttachment = isset($entry) && $entry->type=='attachment' && isset($entry->link) && $entry->link!='';
?>

<div @include('crud::inc.field_wrapper_attributes') >
    <label>{{ $field['label'] }}</label>
    <div class="clearfix"></div>

    <div class="col-sm-3">
        <select
                id="upload_or_link_select"
                name="{{ $field['name'] or 'type' }}"
                @include('crud::inc.field_attributes')
        >

            @if (isset($field['allows_null']) && $field['allows_null']==true)
                <option value="">-</option>
            @endif

            @if (count($field['options']))
                @foreach ($field['options'] as $key => $value)
                    <option value="{{ $key }}"
                            @if (isset($field['value']) && $key==$field['value'])
                            selected
                            @endif
                    >{{ $value }}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-9">
        <!-- external link input -->
        <div class="upload_or_link_value <?php if (!isset($entry) || $entry->type != 'external_link') { echo 'hidden'; } ?>" id="upload_or_link_external_link">
            <input
                    type="url"
                    class="form-control"
                    name="link"
                    placeholder="{{ trans('backpack::crud.page_link_placeholder') }}"

                    @if (!isset($entry) || $entry->type!='external_link')
                    disabled="disabled"
                    @endif

                    @if (isset($entry) && $entry->type=='external_link' && isset($entry->link) && $entry->link!='')
                    value="{{ $entry->link }}"
                    @endif
            >
        </div>
        <!-- internal link input -->
        <div class="upload_or_link_value <?php if (!isset($entry) || $entry->type != 'internal_link') { echo 'hidden'; } ?>" id="upload_or_link_internal_link">
            <input
                    type="text"
                    class="form-control"
                    name="link"
                    placeholder="{{ trans('backpack::crud.internal_link_placeholder', ['url', url('admin/page')]) }}"

                    @if (!isset($entry) || $entry->type!='internal_link')
                    disabled="disabled"
                    @endif

                    @if (isset($entry) && $entry->type=='internal_link' && isset($entry->link) && $entry->link!='')
                    value="{{ $entry->link }}"
                    @endif
            >
        </div>
        <!-- page slug input -->
        <div class="upload_or_link_value <?php if (isset($entry) && $entry->type != 'attachment') { echo 'hidden'; } ?>" id="upload_or_link_attachment">

            {{-- Show the file name and a "Clear" button on EDIT form. --}}
            @if ($hasAttachment)
                <div class="well well-sm">
                    <a target="_blank" href="{{ isset($field['disk'])?asset(\Storage::disk($field['disk'])->url($entry->link)):asset($entry->link) }}">{{ $entry->link }}</a>
                    <a id="attachment_file_clear_button" href="#" class="btn btn-default btn-xs pull-right" title="Clear file"><i class="fa fa-remove"></i></a>
                    <div class="clearfix"></div>
                </div>
            @endif

            {{-- Show the file picker on CREATE form. --}}
            <input
                    type="file"
                    id="attachment_file_input"
                    name="link"
                    value="{{ old('link') ? old('link') : (isset($entry->link) ? $entry->link : (isset($field['default']) ? $field['default'] : '' )) }}"
                    @include('crud::inc.field_attributes', ['default_class' => $hasAttachment ?'form-control hidden':'form-control'])
            >

            {{-- HINT --}}
            @if (isset($field['hint']))
                <p class="help-block">{!! $field['hint'] !!}</p>
            @endif


        </div>
    </div>
    <div class="clearfix"></div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif

</div>


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <script>
        jQuery(document).ready(function($) {

            $("#upload_or_link_select").change(function(e) {
                $(".upload_or_link_value input").attr('disabled', 'disabled');
                $(".upload_or_link_value select").attr('disabled', 'disabled');
                $(".upload_or_link_value").removeClass("hidden").addClass("hidden");

                console.log($(this).val())

                switch($(this).val()) {
                    case 'external_link':
                        $("#upload_or_link_external_link input").removeAttr('disabled');
                        $("#upload_or_link_external_link").removeClass('hidden');
                        break;

                    case 'internal_link':
                        $("#upload_or_link_internal_link input").removeAttr('disabled');
                        $("#upload_or_link_internal_link").removeClass('hidden');
                        break;

                    default: // page_link
                        $("#upload_or_link_attachment input").removeAttr('disabled');
                        $("#upload_or_link_attachment").removeClass('hidden');
                }
            });

            $("#attachment_file_clear_button").click(function(e) {
                e.preventDefault();
                $(this).parent().addClass('hidden');
                var input = $("#attachment_file_input");
                input.removeClass('hidden');
                input.attr("value", "").replaceWith(input.clone(true));
                // add a hidden input with the same name, so that the setXAttribute method is triggered
                $("<input type='hidden' name='link' value=''>").insertAfter("#attachment_file_input");
            });
            $("#attachment_file_input").change(function() {
                console.log($(this).val());
                // remove the hidden input, so that the setXAttribute method is no longer triggered
                $(this).next("input[type=hidden]").remove();
            });

        });
    </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
