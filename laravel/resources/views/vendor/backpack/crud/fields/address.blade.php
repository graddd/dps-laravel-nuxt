<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @php
        $location = old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value']->first() : null);
        $values = [
            'name' => ['label' => 'Name', 'mutable' => true],
            'lat' => ['label' => 'Latitude', 'mutable' => false],
            'lng' => ['label' => 'Longitude', 'mutable' => false],
            'formatted_address' => ['label' => 'Full address', 'mutable' => false],
            'route' => ['label' => 'Route', 'mutable' => false],
            'street_number' => ['label' => 'Street number', 'mutable' => false],
            'postal_code' => ['label' => 'Postal code', 'mutable' => false],
            'administrative_area_level_1' => ['label' => 'State', 'mutable' => false],
            'country' => ['label' => 'Country', 'mutable' => false],
            'url' => ['label' => 'URL', 'mutable' => false]
        ]
    @endphp
    <input
            type="text"
            id="geocomplete"
            name="{{ $field['name'] }}[geocomplete_value]"
            value="{{ !isset($location['geocomplete_value']) ? "": $location['geocomplete_value'] }}"
            @include('crud::inc.field_attributes')
    >
    <div class="location-appends">
        <div class="map_canvas"></div>
    </div>

    <a href="#" class="toggle-location-details">Prikaži / skrij podatke o izbrani lokaciji</a>
    <fieldset class="location-details" style="display: none;">
        @foreach($values as $key => $value)
            <label>{{ $value['label'] }}</label>
            <input name="{{ $field['name'] }}[{{ $key }}]" type="text"
                   value="{{ !isset($location[$key]) ? "": $location[$key] }}"
                   {{ $value['mutable'] ?: 'readonly' }} class="form-control">
        @endforeach
    </fieldset>

    {{-- for now, we use $field[value]->first()->json because locations are a collection (in future one initiative will be able to have multiple locations) --}}
    {{--<input type="hidden" value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value']->first()->json : (isset($field['default']) ? $field['default'] : '' )) }}" name="{{ $field['name'] }}">--}}

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

{{-- Note: you can use  to only load some CSS/JS once, even though there are multiple instances of it --}}

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    <style>
        .ap-input-icon.ap-icon-pin {
            right: 5px !important;
        }

        .ap-input-icon.ap-icon-clear {
            right: 10px !important;
        }

        .location-appends {
            height: 0px;
            overflow: hidden;
        }

        .map_canvas {
            height: 250px;
        }

        .location-details input {
            margin-bottom: 15px;
        }

    </style>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEMwPgIXE6ErEXMBmQ1viQk5j5w2Z6RAM&sensor=false&amp;libraries=places"></script>
    <script src="/vendor/backpack/geocomplete/jquery.geocomplete.js"></script>

    <script>
        $(function () {
            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form",
                {{--                location: "{{ isset($location) ? $location['formatted_address'] : $field['default']['formatted_address'] }}",--}}
                types: ["geocode", "establishment"]
            })
                .bind("geocode:result", function (event, result) {
                    $('.location-appends').height('auto')
                });
            $('.toggle-location-details').click(function () {
                $('.location-details').toggle();
                return false;
            });
        });
    </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}