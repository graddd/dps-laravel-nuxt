<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.head')
</head>
<body>
<div id="app">

    @section('content')
    @show

</div>
@include('partials.scripts')
</body>
</html>
