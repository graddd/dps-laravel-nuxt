<?php

return [
    'Unauthorized' => 'Manjka Sendinblue API ključ - prosim, obvestite info@permakultura.si',
    'Invalid email address' => 'E-poštni naslov ni pravilen, poskusite znova',
    'duplicate_parameter' => 'Vaš e-poštni naslov že imamo!',
    'missing_parameter' => 'Vnesite e-poštni naslov',
    'created' => 'Prijava uspešna! Hvala.',
    'general_error' => 'Prišlo je do napake, poskusite znova'
];