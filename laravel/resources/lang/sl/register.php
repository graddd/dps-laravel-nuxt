<?php

return [
    'title' => 'Registracija',
    'name' => 'Ime in priimek',
    'email' => 'E-poštni naslov',
    'password' => 'Geslo',
    'confirm_password' => 'Ponovi geslo',
    'submit' => 'Registracija'
];