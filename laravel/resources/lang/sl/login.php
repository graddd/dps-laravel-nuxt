<?php

return [
    'title' => 'Prijava',
    'register' => 'Registracija',
    'email' => 'E-poštni naslov',
    'password' => 'Geslo',
    'forgot_password' => 'Pozabljeno geslo?',
    'remember_me' => 'Zapomni si me',
    'submit' => 'Prijava',
    'social_or_not' => '-- Ali pa se prijavi z e-poštnim naslovom in geslom --',

    'social' => [
        'facebook' => 'Prijava s Facebook profilom',
        'google' => 'Prijava z Google računom'
    ]
];