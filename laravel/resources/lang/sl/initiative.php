<?php

 return [
     "map" => [
         'sidebar' => [
             'search' => [
                 'placeholder' => 'Iskanje po organizacijah ...',
                 'action' => 'Išči'
             ],
             'filters' => [
                 'title' => 'Nastavitev filtrov',
                 'choose_categories' => 'Izberi željene katergorije',
                 'choose_audience' => 'Ciljna publika organizacije',
                 'clear' => 'Odstrani vse filtre in iskalne besede'
             ],
             'list' => [
                 'no_results' => 'Vašim iskalnim kriterijem ne ustreza nobena organizacija.',
             ]
         ]
     ],
    "location_type" => [
        "urban" => "Urbano območje",
        "rural" => "Ruralno območje",
        "urban_and_rural" => "Urbano in ruralno območje"
    ],
    "status" => [
        "germinating" => "Začetna idejno-načrtovalna faza",
        "taking_root" => "Ukoreninjanje in izvrševanje zadanih ciljev",
        "fruiting" => "Razvidni plodovi dela",
        "seeding" => "Ustvarjanje aktivnih podmladkov"
    ]
];