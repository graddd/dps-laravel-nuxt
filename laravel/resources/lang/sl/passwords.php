<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Geslo se mora ujemati, dolgo pa mora biti vsaj 6 znakov.',
    'reset' => 'Vaše geslo je bilo uspešno spremenjeno.',
    'sent' => 'Na vaš e-poštni naslov smo vam poslali povezavo do ponastavitve gesla.',
    'token' => 'Neveljaven ključ za ponastavitev gesla.',
    'user' => "Uporabnik s tem e-poštnim naslovom ne obstaja.",

    'email_page' => [
        'title' => 'Ponastavitev gesla',
        'email' => 'E-poštni naslov',
        'submit' => 'Pošlji povezavo do ponastavitve gesla',
    ],
    'reset_page' => [
        'title' => 'Ponastavitev gesla',
        'email' => 'E-poštni naslov',
        'password' => 'Geslo',
        'confirm_password' => 'Ponovi geslo',
        'submit' => 'Ponastavi geslo'
    ]

];
