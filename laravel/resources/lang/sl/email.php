<?php

return [
    'greeting' => 'Živjo!',
    'greeting_error' => 'Ojoj!',
    'regards' => 'Lep pozdrav,',
    'action_trouble' => 'Če imate težave z odpiranjem povezave ":action", kopirajte naslednjo povezavo v vaš brskalnik:',
    'all_rights_reserved' => 'Vse pravice pridržane.',

    'password' => [
        'subject' => 'Ponastavitev gesla',
        'description_start' => 'To pošto ste prejeli, ker je bila za vaš e-poštni naslov zahtevana ponastavitev gesla.',
        'action' => 'Ponastavi geslo',
        'description_end' => 'Če ponastavitve niste zahtevali, lahko to sporočilo ignorirate.'
    ]
];