<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateArticlesTableAndEventsTableAddForeignKeyContraintForCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function(Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('events', function(Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });
    }
}
