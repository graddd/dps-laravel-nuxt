<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role_users', function(Blueprint $table)
		{
			$table->integer('role_id')->unsigned();
			$table->integer('user_id')->unsigned()->index('role_users_user_id_foreign');
			$table->primary(['role_id','user_id']);

            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('role_users');
	}

}
