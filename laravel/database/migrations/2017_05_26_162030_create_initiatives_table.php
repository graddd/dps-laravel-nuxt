<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiativesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiatives', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('initiative_category_id')->unsigned();
            $table->string('slug');
            $table->string('name');
            $table->string('short_description', 140);
            $table->text('description');
            $table->string('keywords');
            $table->integer('status');
            $table->string('url');
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->string('video_url');
            $table->integer('audience_size');
            $table->integer('group_size');
            $table->integer('area_size');
            $table->integer('location_type');
            $table->boolean('is_approved');
            $table->timestamp('start_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiatives');
	}

}
