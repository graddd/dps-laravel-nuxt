<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLocationsTableAddGoogleMapFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function(Blueprint $table)
        {
            $table->string('geocomplete_value');
            $table->string('formatted_address');
            $table->string('route');
            $table->string('street_number');
            $table->string('postal_code');
            $table->string('administrative_area_level_1');
            $table->string('country');
            $table->string('url');
            $table->dropColumn('json');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function(Blueprint $table) {
            $table->dropColumn('geocomplete_value');
            $table->dropColumn('formatted_address');
            $table->dropColumn('route');
            $table->dropColumn('street_number');
            $table->dropColumn('postal_code');
            $table->dropColumn('administrative_area_level_1');
            $table->dropColumn('country');
            $table->dropColumn('url');
            $table->longText('json');
        });
    }
}
