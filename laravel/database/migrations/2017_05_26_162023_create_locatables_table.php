<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocatablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locatables', function(Blueprint $table)
		{
			$table->integer('location_id')->unsigned()->index();
			$table->integer('locatable_id')->unsigned()->index();
			$table->string('locatable_type');

            $table->foreign('location_id')
                ->references('id')->on('locations')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locatables');
	}

}
