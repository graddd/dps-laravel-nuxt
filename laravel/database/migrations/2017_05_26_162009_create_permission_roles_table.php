<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePermissionRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission_roles', function(Blueprint $table)
		{
			$table->integer('permission_id')->unsigned();
			$table->integer('role_id')->unsigned()->index('permission_roles_role_id_foreign');
			$table->primary(['permission_id','role_id']);

            $table->foreign('permission_id')
                ->references('id')->on('permissions')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission_roles');
	}

}
