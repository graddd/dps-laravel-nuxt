<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_user', function(Blueprint $table)
		{
			$table->integer('event_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_user');
	}

}
