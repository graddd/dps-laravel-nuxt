<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_event', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('article_id')
                ->references('id')->on('articles')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_event');
    }
}
