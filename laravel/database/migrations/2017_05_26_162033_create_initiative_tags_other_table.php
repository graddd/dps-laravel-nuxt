<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiativeTagsOtherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiative_tags_other', function(Blueprint $table)
		{
			$table->integer('tag_id')->unsigned()->index('initiative_tags_other_tag_id_foreign');
			$table->string('name');

            $table->foreign('tag_id')
                ->references('id')->on('initiative_tags')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiative_tags_other');
	}

}
