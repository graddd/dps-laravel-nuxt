<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('slug');
            $table->integer('category_id')->unsigned();
            $table->string('website');
            $table->longText('content');
            $table->longText('content_short');
            $table->string('image')->nullable();
            $table->timestamp('start_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('end_at')->default('0000-00-00 00:00:00');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
