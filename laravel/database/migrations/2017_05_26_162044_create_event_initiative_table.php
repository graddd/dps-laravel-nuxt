<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventInitiativeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_initiative', function(Blueprint $table)
		{
			$table->integer('event_id')->unsigned()->index();
			$table->integer('initiative_id')->unsigned()->index();
			$table->timestamps();

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('initiative_id')
                ->references('id')->on('initiatives')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_initiative');
	}

}
