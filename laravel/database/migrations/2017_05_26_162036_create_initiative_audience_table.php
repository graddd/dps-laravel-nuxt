<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiativeAudienceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiative_audience', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('initiative_id')->unsigned()->index('initiative_audience_initiative_id_foreign');
            $table->integer('audience_id')->unsigned()->index('initiative_audience_audience_id_foreign');
            $table->string('name');
            $table->timestamps();

            $table->foreign('audience_id')
                ->references('id')->on('audience')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('initiative_id')
                ->references('id')->on('initiatives')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiative_audience');
	}

}
