<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiativeInitiativeTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiative_initiative_tags', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('initiative_id')->unsigned();
            $table->integer('initiative_tag_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('initiative_id')
                ->references('id')->on('initiatives')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('initiative_tag_id')
                ->references('id')->on('initiative_tags')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('initiative_initiative_tags');
    }
}
