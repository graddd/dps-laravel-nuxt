<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referenceables', function(Blueprint $table)
        {
            $table->integer('reference_id')->unsigned()->index();
            $table->integer('referenceable_id')->unsigned()->index();
            $table->string('referenceable_type');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('reference_id')
                ->references('id')->on('references')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referenceables');
    }
}
