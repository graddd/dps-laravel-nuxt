<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiativeUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiative_user', function(Blueprint $table)
		{
			$table->integer('initiative_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->timestamps();

            $table->foreign('initiative_id')
                ->references('id')->on('initiatives')
                ->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiative_user');
	}

}
