<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
            $types = collect(config('dps.categories.types'))->pluck('key')->toArray();

            $table->increments('id');
			$table->string('name');
			$table->string('slug')->unique();
            $table->string('description');
            $table->enum('type', $types)->default('regular');
            $table->tinyInteger('is_project')->default(0);
            $table->integer('parent_id')->nullable()->default(0);
            $table->integer('lft')->unsigned()->nullable();
            $table->integer('rgt')->unsigned()->nullable();
            $table->integer('depth')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
