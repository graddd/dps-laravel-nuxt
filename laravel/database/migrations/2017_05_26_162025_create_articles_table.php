<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
		    $featured_options = collect(config('dps.articles.featured_options'))->pluck('key')->toArray();

			$table->increments('id');
			$table->integer('category_id')->unsigned();
			$table->string('name');
			$table->string('slug');
            $table->longText('content');
            $table->longText('content_short');
			$table->string('image')->nullable();
            $table->string('image_cover')->nullable();
            $table->text('images')->nullable();
            $table->string('link')->nullable();
            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('PUBLISHED');
			$table->date('date');
			$table->enum('featured', $featured_options)->default('regular');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}
