<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitiativeAudienceOtherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiative_audience_other', function(Blueprint $table)
		{
			$table->integer('audience_id')->unsigned()->index('initiative_audience_other_audience_id_foreign');
			$table->string('name');

            $table->foreign('audience_id')
                ->references('id')->on('initiative_audience')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiative_audience_other');
	}

}
