<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Initiative\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug()
    ];
});

$factory->define(\App\Models\Initiative::class, function (Faker\Generator $faker) {
    $words = implode(',', $faker->words(3));
    $content = '<p>' . implode(' ', $faker->sentences(10)) . '</p>' .
        '<p>' . implode(' ', $faker->sentences(15)) . '</p>';

    $name = ucfirst($faker->words(3, true));
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'keywords' => $words,
        'short_description' => ucfirst($faker->sentences(3, true)),
        'description' => $content,
        'url' => $faker->url,
        'logo' => $faker->imageUrl(100, 100),
        'cover' => $faker->imageUrl(800, 600),
        'audience_size' => random_int(0, 6),
        'location_type' => random_int(0, 2),
        'status' => random_int(0, 3),
        'is_approved' => true
    ];
});