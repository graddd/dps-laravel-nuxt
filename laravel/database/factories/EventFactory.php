<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 6/14/17
 * Time: 6:49 PM
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Event::class, function (Faker\Generator $faker) {

    $start_at = $faker->dateTimeBetween('-60 days', '+60 days');
    $end_at = (\Carbon\Carbon::createFromTimestamp($start_at->getTimestamp()))->addDays(random_int(0, 2));

    $name = ucfirst($faker->words(4, true));
    return [
        'name' => $name,
        'website' => $faker->url,
        'content' => $faker->sentences(25, true),
        'content_short' => $faker->text(200),
        'image' => $faker->imageUrl(500, 500),
        'start_at' => $start_at,
        'end_at' => $end_at
    ];
});