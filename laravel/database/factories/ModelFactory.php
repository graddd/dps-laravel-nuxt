<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */


/**
 * Users
 */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Models\User\Profile::class, function(Faker\Generator $faker) {
   return [
       'user_id' => function () {
           return factory(App\Models\User::class)->create()->id;
       },
       'bio' => $faker->sentences(4, true),
       'photo_url' => $faker->imageUrl(),
       'website' => $faker->url,
       'facebook' => $faker->url,
       'twitter' => $faker->url
   ];
});

/**
 * Category
 */
$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->words(2, true)),
        'description' => $faker->sentences(2, true),
        'type' => collect(config('dps.categories.types'))
            ->pluck('name', 'key')
            ->keys()
            ->random()
    ];
});

/**
 * Initiative Category
 */
$factory->define(App\Models\Initiative\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->words(2, true)),
        'description' => $faker->sentences(2, true),
        'slug' => str_random(10)
    ];
});

/**
 * Initiative tags
 */
$factory->define(\App\Models\Initiative\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->slug()
    ];
});

/**
 * Audience
 */
$factory->define(App\Models\Audience::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});

/**
 * Locations
 */
$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    $lat = $faker->randomFloat(6, 45.488, 46.700);
    $lng = $faker->randomFloat(6, 13.640, 16.600);

    return [
        'name' => $faker->address,
        'lat' => $lat,
        'lng' => $lng
    ];
});


$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'website' => $faker->url,
    ];
});

