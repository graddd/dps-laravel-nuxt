<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Tag::class, function (Faker\Generator $faker) {
    $name = $faker->word;
    return [
        'name' => $name,
        'slug' => str_slug($name)
    ];
});

$factory->define(\App\Models\Article::class, function (Faker\Generator $faker) {
    $sectionsCount = random_int(3,5);
    $content = '';
    for($i = 0; $i < $sectionsCount; $i++){
        $image_width = random_int(400, 500);
        $image_height = random_int(200, 700);
        $image = [
            'alt' => ucfirst($faker->words(random_int(3, 10), true)),
            'width' => $image_width,
            'height' => $image_height,
            'src' => $faker->imageUrl($image_width, $image_height),
            'float' => random_int(0, 1) ? 'left' : 'right'
        ];
        $imageHtml = '<img alt="' . $image['alt'] . '" src="' . $image['src'] . '" style="float:' . $image['float'] . '; height:' . $image['height'] . 'px; width:' . $image['width'] . 'px" />';
        $content .= '<h2>' . ucfirst($faker->words(4, true)) . '</h2>' .
            '<p>'. $imageHtml . $faker->paragraph(13) . '</p>' .
            '<p>' . $faker->paragraph(15) . '</p>' .
            '<p>' . $faker->paragraph(10) . '</p>';
    }
    $name = ucfirst($faker->words(3, true));

    return [
        'name' => $name,
        'content' => $content,
        'content_short' => $faker->text(700),
        'image' => $faker->imageUrl(500, 500),
        'status' => 'PUBLISHED',
        'date' => $faker->dateTimeBetween('-3 days', '+10 days'),
        'created_at' => $faker->dateTimeBetween('-3 days', '+3 days')
    ];
});