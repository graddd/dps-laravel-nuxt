<?php

use Illuminate\Database\Seeder;

class ElasticsearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * delete elasticsearch entries
         */
        $client = new GuzzleHttp\Client();
        try {
            $client->delete('localhost:9200/' . config('scout.elasticsearch.index'));
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::info('Already empty...');
        }

        $this->command->call('elastic:mappings');
        $this->command->call('scout:import', ['model' => \App\Models\Article::class]);
        $this->command->call('scout:import', ['model' => \App\Models\Event::class]);
        $this->command->call('scout:import', ['model' => \App\Models\Initiative::class]);
    }
}
