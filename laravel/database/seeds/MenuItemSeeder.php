<?php

use Illuminate\Database\Seeder;
use App\Models\MenuItem;

class MenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('menu_items')->truncate();

        $parent_items = [
            1 => [
                'name' => 'Permakultura',
                'description' => 'Kaj je permakultura?',
                'link' => '/permakultura'
            ],
            2 => [
                'name' => 'O društvu',
                'description' => 'Osnovne informacije društva',
                'link' => '/o-drustvu'
            ],
            3 => [
                'name' => 'Ostalo',
                'description' => 'Opis za ostalo',
                'link' => ''
            ]
        ];
        $children_items = [
            1 => [
                [
                    'name' => 'Etike in načela',
                    'description' => 'Temelji permakulture',
                    'link' => '/etike-in-nacela',
                ],
                [
                    'name' => 'Permakulturno načrtovanje',
                    'description' => 'Orodja in metode',
                    'link' => '/permakulturno-nacrtovanje',
                ],
                [
                    'name' => 'Področja delovanja',
                    'description' => 'Ekosistemi in širše',
                    'link' => '/podrocja-delovanja',
                ],
                [
                    'name' => 'Literatura',
                    'description' => 'Povezave in gradiva',
                    'link' => '/literatura',
                ]
            ],
            2 => [
                [
                    'name' => 'Podmeni 1',
                    'description' => 'Opis podmeni 1',
                    'link' => '',
                ],
                [
                    'name' => 'Podmeni zwei',
                    'description' => 'Opis podmeni 2',
                    'link' => '',
                ],
                [
                    'name' => 'Podmeni tri3',
                    'description' => 'Opis podmeni 3',
                    'link' => '',
                ],
                [
                    'name' => 'Podmeni quaddro',
                    'description' => 'Opis podmeni 4',
                    'link' => '',
                ],
            ],
            3 => [
                [
                    'name' => 'Koledar dogodkov',
                    'description' => 'Dogodki društva in organizacij',
                    'link' => '/calendar',
                ],
                [
                    'name' => 'Trajnostne organizacije',
                    'description' => 'Slovenske in obmejne trajnostne organizacije',
                    'link' => '/initiatives',
                ],
                [
                    'name' => 'Micelij',
                    'description' => 'Aplikacija in zemljevid organizacij',
                    'link' => '/micelij',
                ],
            ]

        ];

        $parents = [];
        foreach ($parent_items as $key => $parent){
            $parent['type'] = 'internal_link';
            $parent['in_navigation'] = 1;
            $parents[$key] = MenuItem::create($parent);
        }
        foreach ($children_items as $parentKey => $children){
            foreach ($children as $child){
                $child['type'] = 'internal_link';
                $child['in_navigation'] = 1;
                $menu = new MenuItem($child);
                $menu->parent()->associate($parents[$parentKey]);
                $menu->save();
            }
        }

    }
}
