<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        /**
         * Premade categories
         */

        factory(Category::class, 5)->make()->each(function (Category $category) {
            if(random_int(0,3) == 0) {
                // 25% chance of creating a project
                $category->is_project = 1;
            }
            $category->save();
        });
    }
}
