<?php

use Backpack\PermissionManager\app\Models\Permission;
use Backpack\PermissionManager\app\Models\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        $permissions = [
            'access administration',
            'manage users',
            'manage initiatives',
            'manage events',
            'manage menu',
            'manage settings',
            'manage files',
            'manage translations',
            'manage backups',
            'manage articles',
            'manage categories and tags'
        ];

        collect($permissions)->each(function($name) {
            Permission::create(compact('name'));
        });
    }
}
