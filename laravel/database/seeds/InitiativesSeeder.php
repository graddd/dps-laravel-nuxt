<?php

use App\Models\Audience;
use App\Models\Initiative\Category;
use App\Models\Initiative\Tag;
use App\Models\Initiative;
use App\Models\Location;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class InitiativesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quantity = 25;

        $audience = Audience::all();
        $categories = Category::all();
        $tags = Tag::all();

        $this->command->getOutput()->progressStart($quantity);

        DB::table('initiatives')->delete();
        factory(Initiative::class, $quantity)
            ->make()
            ->each(function (Initiative $initiative) use ($audience, $categories, $tags) {
                $initiative->category()->associate($categories->random());
                $initiative->save();
                $initiative->contacts()->saveMany(factory(\App\Models\Contact::class, random_int(1, 2))->make());
                $initiative->audience()->saveMany($audience->random(random_int(1, 2)));
                $initiative->tags()->attach($tags->random(random_int(1, 3)));
                $initiative->locations()->saveMany(factory(Location::class, random_int(1, 2))->make());

                $this->command->getOutput()->progressAdvance();
            });

        $this->command->getOutput()->progressFinish();

        /** @var \App\Models\User $user */
        $user = \App\Models\User::find(2);

        $user->initiatives()->sync(Initiative::all()->random(4)->pluck('id'));

    }
}
