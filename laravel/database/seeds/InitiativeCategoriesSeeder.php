<?php

use App\Models\Initiative\Category;
use Illuminate\Database\Seeder;

class InitiativeCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('initiative_categories')->delete();

        factory(Category::class, 8)->create();
    }
}
