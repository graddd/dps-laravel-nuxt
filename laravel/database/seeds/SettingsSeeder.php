<?php

use Backpack\Settings\app\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('settings')->delete();

        if(!Setting::where(['key' => 'contact_email'])->exists()){
            DB::table('settings')->insert([
                'key'           => 'contact_email',
                'name'          => 'Contact form email address',
                'description'   => 'The email address that all emails from the contact form will go to.',
                'value'         => 'info@permakultura.si',
                'field'         => '{"name":"value","label":"Value","type":"email"}',
                'active'        => 1,
            ]);
        }

        if(!Setting::where(['key' => 'website_name'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'website_name',
                'name' => 'Name',
                'description' => 'Gets shown in website homepage title, page footer, etc.',
                'value' => 'Društvo za permakulturo Slovenije',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'website_domain'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'website_domain',
                'name' => 'Website domain',
                'description' => 'Shown in the end of title, e.g. "Name of article | Domain.com"',
                'value' => 'Permakultura.si',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'website_description'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'website_description',
                'name' => 'Website description',
                'description' => 'Gets shown in Google result description.',
                'value' => 'Permakultura.si je portal člankov o trajnostnem razvoju in sonaravnem ravnanju. S platformo Micelij ponujamo zemljevid in koledar dogodkov slovenskih trajnostnih organizacij.',
                'field' => '{"name":"value","label":"Value","type":"text", "hint":"Max 170 characters recommended, otherwise google will truncate it."}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'website_keywords'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'website_keywords',
                'name' => 'Website keywords',
                'description' => 'Relevant for Google and other search engines.',
                'value' => 'permakultura, DPS, društvo za permakulturo, trajnostni razvoj, micelij, etike, načela, načrtovanje',
                'field' => '{"name":"value","label":"Value","type":"text","hint":"Besede naj bodo ločene z vejicami."}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'og_image'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'og_image',
                'name' => 'OG image',
                'description' => 'Defaut image that gets shown when sharing on facebook.',
                'value' => '/images/assets/og_image.jpg',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'address'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'address',
                'name' => 'Address',
                'description' => 'Address (location); shown in website footer.',
                'value' => 'Metelkova ulica 6, 1000 Ljubljana',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'privacy_cookies_link'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'privacy_cookies_link',
                'name' => 'Privacy and cookies link',
                'description' => 'Link to page with info about privacy; shown in website footer.',
                'value' => '/article/o-zasebnosti-in-piskotkih',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'contact_phone'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'contact_phone',
                'name' => 'Contact phone',
                'description' => 'Contact phone that is shown in footer',
                'value' => '031 885 075',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }

        if(!Setting::where(['key' => 'newsletter_description'])->exists()) {
            DB::table('settings')->insert([
                'key' => 'newsletter_description',
                'name' => 'Newsletter description',
                'description' => 'Description of newsletter for on front page',
                'value' => 'S permakulturnim novičnikom enkrat mesečno obveščamo naše sledilce o prihodnjih dogodkih in permakulturnih novitetah v Sloveniji in po svetu.',
                'field' => '{"name":"value","label":"Value","type":"text"}',
                'active' => 1,
            ]);
        }
    }
}
