<?php

use Illuminate\Database\Seeder;

class InitiativeTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('initiative_tags')->delete();

        factory(\App\Models\Initiative\Tag::class, 10)->create();
    }
}
