<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UsersSeeder::class);

        $this->call(TagsSeeder::class);

        $this->call(CategoriesSeeder::class);
        $this->call(AudienceSeeder::class);

        $this->call(InitiativeCategoriesSeeder::class);
        $this->call(InitiativeTagsSeeder::class);
        $this->call(InitiativesSeeder::class);

        $this->call(EventsSeeder::class);
        $this->call(ArticlesSeeder::class);

        $this->call(ElasticsearchSeeder::class);
        $this->call(MenuItemSeeder::class);
        $this->call(SettingsSeeder::class);
    }
}
