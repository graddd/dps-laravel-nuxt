<?php

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->delete();

        $categories = \App\Models\Category::all()->whereNotIn('slug', ['']);
        $tags = \App\Models\Tag::all();
        $events = \App\Models\Event::all();
        $featured_opts = collect(config('dps.articles.featured_options'))
            ->pluck('name', 'key')
            ->forget('regular')
            ->keys();

        factory(Article::class, 25)->make()->each(function (Article $article) use ($categories, $tags) {
            $article->featured = 'regular';
            $article->category()->associate($categories->random());
            $article->save();
            $article->tags()->attach($tags->random(random_int(1, 3)));
        });

        factory(Article::class, 12)->make()->each(function (Article $article) use ($categories, $tags, $events, $featured_opts) {
            $article->featured = $featured_opts->random();

            $article->category()->associate($categories->random());

            //spotlight
            if($article->featured == 'spotlight'){
                $article->image_cover = '/images/banners/' . random_int(1, 3) . '.JPG';
            }
            $article->save();

            if(in_array($article->featured, ['spotlight', 'top_three'])){
                if(random_int(0, 1)){
                    //50% chance of getting an event report
                    $article->events()->attach($events->random(random_int(1,2)));
                }
            }
            $article->tags()->attach($tags->random(random_int(1, 3)));
        });
    }
}
