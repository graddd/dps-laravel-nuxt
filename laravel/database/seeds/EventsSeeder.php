<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();

        $categories = \App\Models\Category::all()->whereNotIn('slug', ['']);
        $tags = \App\Models\Tag::all();

        factory(\App\Models\Event::class, 30)->make()->each(function(\App\Models\Event $event) use($categories, $tags) {
            $event->category()->associate($categories->random());
            $event->save();
            $event->tags()->attach($tags->random(random_int(1,3)));
            $event->locations()->save(factory(Location::class)->make());
        });
    }
}
