<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::statement("ALTER TABLE `users` AUTO_INCREMENT = 1;"); //needed because users' ids are used for seeding

        $users = [
            'graddd' => [
                'name' => 'Administrator',
                'email' => 'graddd@gmail.com',
                'password' => bcrypt('secret'),
            ],
            'krofek' => [
                'name' => 'Krofek',
                'email' => 'vrabec.matej@gmail.com',
                'password' => bcrypt('secret'),
            ]
        ];

        collect($users)->each(function($userData) {
            $user = App\Models\User::create($userData);
            $user->assignRole('administrator');
        });

        $this->command->call('passport:install');
    }
}
