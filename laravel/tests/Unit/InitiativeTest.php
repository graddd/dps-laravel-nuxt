<?php

namespace Tests\Unit;

use App\Models\Audience;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Initiative;
use App\Models\Location;
use App\Models\User;
use App\Repositories\InitiativeRepository;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InitiativeTest extends TestCase
{
//    use DatabaseMigrations;
    /**
     * @return Initiative[]|Collection
     */
    private function createInitiatives()
    {
        $audience = factory(Audience::class, 4)->create();
        $categories = factory(Category::class, 5)->create();

        /** @var Initiative[]|Collection $initiatives */
        factory(Initiative::class, 25)
            ->create()
            ->each(function (Initiative $initiative) use ($audience, $categories) {
                $initiative->contacts()->saveMany(factory(\App\Models\Contact::class, 1)->make());
                $initiative->audience()->saveMany($audience->random(random_int(2, 3)));
                $initiative->categories()->saveMany($categories->random(random_int(3, 4)));
                $initiative->locations()->saveMany(factory(Location::class, random_int(1, 2))->make());
                $initiative->save();
            });

        \Artisan::call('scout:import', ['model' => 'App\Models\Initiative']);

        return Initiative::all();
    }

    public function testSearch()
    {
        $initiatives = $this->createInitiatives();
        $initiative = $initiatives->random(1)->first();

        $result = app(InitiativeRepository::class)->search($initiative->name);

        $this->assertEquals($initiative->name, $result->first()->name);

    }

    public function testAudienceFilter()
    {
        $initiatives = $this->createInitiatives();
        $initiative = $initiatives->random(1)->first();
        $audience = $initiative->audience->first()->name;

        $result = app(InitiativeRepository::class)
            ->get('', ['audience' => [$audience]]);

        $result->each(function(Initiative $initiative) use ($audience) {
            $this->assertContains($audience, $initiative->audience->pluck('name'));
        });
    }

    public function testCategoriesFilter()
    {
        $initiatives = $this->createInitiatives();
        $initiative = $initiatives->random(1)->first();
        $category = $initiative->categories->first()->name;

        $result = app(InitiativeRepository::class)
            ->get('', ['categories' => [$category]]);

        $result->each(function(Initiative $initiative) use ($category) {
            $this->assertContains($category, $initiative->categories->pluck('name'));
        });


    }

    public function testFiltersAndSearch()
    {
        $initiatives = $this->createInitiatives();
        $initiative = $initiatives->random(1)->first();
        $category = $initiative->categories->first()->name;
        $audience = $initiative->audience->first()->name;

        $result = app(InitiativeRepository::class)->get(
            $initiative->name,
            [
                'categories' => [$category],
                'audience' => [$audience]
            ]
        );

        $result->each(function(Initiative $initiative) use ($category, $audience) {
            $this->assertContains($category, $initiative->categories->pluck('name'));
            $this->assertContains($audience, $initiative->audience->pluck('name'));
        });

        $this->assertEquals($initiative->name, $result->first()->name);
    }

    public function testCreateInitiative()
    {
        $aud = factory(Audience::class, 1)->create()->first();
        $cat = factory(Category::class, 1)->create()->first();

        $input = [
            "name" => "lalalala",
            "start_at" => "2017-06-30T18:37:53.321Z",
            "status" => 0,
            "group_size" => "21",
            "audience_size" => 3,
            "location_type" => 0,
            "area_size" => "12",
            "logo" => null,
            "cover" => null,
            "keywords" => "fdsfdsafdsa",
            "short_description" => "fdsafdsfads fsafdfasfds",
            "description" => "fdsafdsfads fsafdfasfds fdsafdsfads fsafdfasfds fdsafdsfads fsafdfasfds fdsafdsfads 
                fsafdfasfds fdsafdsfads fsafdfasfds fdsafdsfads fsafdfasfds fdsafdsfads fsafdfasfds fdsafdsfads 
                fsafdfasfds fdsafdsfads fsafdfasfds",

        ];

        $categoriesInput = [
            0 => [
                "id" => $cat->id,
                "name" => $cat->name,
                "slug" => $cat->slug,
                "description" => $cat->description,
            ]
        ];
        $audienceInput = [
            0 => [
                "id" => $aud->id,
                "name" => $aud->name,
            ]
        ];
        $contactsInput = [
            0 => [
                "name" => "fdsafdfsa",
                "phone" => null,
                "email" => "fdsa@fdsafa.fds",
                "website" => null,
                "facebook" => null
            ]
        ];
        $locationInput = [
            "name" => "Koprska ulica 2, 1000 Ljubljana",
            "lat" => 46.0414,
            "lng" => 14.4817
        ];

        /** @var Initiative $initiative */
        $initiative = Initiative::create($input);

        $categories = [];
        foreach ($categoriesInput as $category) {
            $categories[] = $category['id'];
        }
        $initiative->categories()->sync($categories);

        $audience = [];
        foreach($audienceInput as $a) {
            $audience[] = $a['id'];
        }

        $initiative->audience()->sync($audience);

        $contacts = [];
        foreach($contactsInput as $contact) {
            $contacts[] = new Contact($contact);
        }

        $initiative->contacts()->saveMany($contacts);

        $initiative->locations()->save(new Location($locationInput));

        $this->assertTrue($initiative->exists);
    }

}
