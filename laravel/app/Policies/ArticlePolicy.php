<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Those who manage articles can create, update and delete them.
     *
     * @param User $user
     * @return bool|null
     */
    public function before(User $user)
    {
        return $user->can('manage articles') ? true : null; # muy importante "null"
    }

    public function update()
    {
        return false;
    }

    public function create()
    {
        return false;
    }
}
