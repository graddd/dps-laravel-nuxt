<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Event;
use App\Models\Initiative;
use App\Policies\ArticlePolicy;
use App\Policies\EventPolicy;
use App\Policies\InitiativePolicy;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Initiative::class => InitiativePolicy::class,
        Event::class => EventPolicy::class,
        Article::class => ArticlePolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensExpireIn(Carbon::now()->addDays(15));
    }
}
