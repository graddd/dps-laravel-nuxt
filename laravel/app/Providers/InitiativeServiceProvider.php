<?php

namespace App\Providers;

use App\Services\CrudService;
use App\Services\EmailService;
use Illuminate\Http\Request;
use App\Interfaces\InitiativeRepositoryInterface;
use App\Interfaces\MenuItemRepositoryInterface;
use App\Repositories\InitiativeRepository;
use App\Repositories\MenuItemRepository;
use App\Services\EventService;
use App\Services\InitiativeService;
use App\Services\SpaService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Translation\Translator;

class InitiativeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Interfaces\InitiativeRepositoryInterface', 'App\Repositories\InitiativeRepository');
        $this->app->bind('App\Interfaces\LocationRepositoryInterface', 'App\Repositories\LocationRepository');
        $this->app->bind('App\Interfaces\CategoryRepositoryInterface', 'App\Repositories\CategoryRepository');
        $this->app->bind('App\Interfaces\MenuItemRepositoryInterface', 'App\Repositories\MenuItemRepository');
        $this->app->bind('App\Interfaces\ArticleRepositoryInterface', 'App\Repositories\ArticleRepository');

        $this->app->singleton(InitiativeService::class);
        $this->app->singleton(EventService::class);
        $this->app->singleton(CrudService::class);
        $this->app->singleton('spa', function(Application $app){
            return new SpaService(
                $app->make(Request::class),
                $app->make(MenuItemRepositoryInterface::class)
            );
        });
        $this->app->singleton('sendinblue', function(Application $app) {
            return new EmailService(
                $app->make(Translator::class)
            );
        });

    }

//    /**
//     * Get the services provided by the provider.
//     *
//     * @return array
//     */
//    public function provides()
//    {
//        return [InitiativeService::class];
//    }
}
