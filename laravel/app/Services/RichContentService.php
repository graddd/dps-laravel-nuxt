<?php

namespace App\Services;

class RichContentService
{
    /**
     * Rich content parsing
     * --------------------
     */

    /**
     * @param \DOMDocument $dom
     * @return string
     */
    static function DOMinnerHTML(\DOMDocument $dom)
    {
        $body = $dom->getElementsByTagName('body')[0];
        $html = '';
        foreach ($body->childNodes as $node) {
            $html .= $dom->saveHTML($node);
        }
        return $html;
    }

    static function prepareContent($content)
    {

        $dom = new \DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);
        $images = $dom->getElementsByTagName('img');

        /** @var \DOMElement $image */
        foreach ($images as $image) {
            $left = !!strpos($image->getAttribute('style'), 'left');
            $right = !!strpos($image->getAttribute('style'), 'right');
            $styleArray = static::parseStyle($image->getAttribute('style'));

            $div_wrapper = $dom->createElement('span');
            if ($left) {
                $div_wrapper->setAttribute('class', 'imageWrapper left');
            } elseif ($right) {
                $div_wrapper->setAttribute('class', 'imageWrapper right');
            } else {
                $div_wrapper->setAttribute('class', 'imageWrapper');
            }
            if(isset($styleArray['width'])){
                $div_wrapper->setAttribute('style', 'width:' . $styleArray['width']);
            }
            $image->parentNode->replaceChild($div_wrapper, $image);
            $div_wrapper->appendChild($image);

            if ($image->hasAttribute('alt') && $image->getAttribute('alt')) {
                $span_caption = $dom->createElement('span');
                $span_caption->textContent = $image->getAttribute('alt');
                $div_wrapper->appendChild($span_caption);
            }
        }
        // make headings with slugs for ids
        $dom = self::makeHeadings($dom);

        return self::DOMinnerHTML($dom);
    }

    public static function getHeadings(string $content)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);
        return self::makeHeadings($dom, true);
    }

    public static function makeHeadings(\DOMDocument $dom, $returnHeadings = false)
    {
        $h2_elements = $dom->getElementsByTagName('h2');
        $headings = collect();
        /** @var \DOMElement $h2 */
        foreach ($h2_elements as $i => $h2) {
            $id = str_slug('heading-' . $i . '-' . $h2->textContent);
            $headings = $headings->push([
                'id' => $id,
                'name' => $h2->textContent
            ]);
            $h2->setAttribute('id', $id);
        }

        return $returnHeadings ? $headings : $dom;
    }

    static function parseStyle($style)
    {
        $results = array();
        foreach (explode(';', $style) AS $attr){
            if (strlen($attr) > 0) // for missing semicolon on last element, which is legal
            {
                // Explode on the CSS attributes defined
                list($name, $value) = explode(':', $attr);
                $results[trim($name)] = trim($value);
            }
        }
        return $results;
    }
}