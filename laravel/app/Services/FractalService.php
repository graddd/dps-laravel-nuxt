<?php

namespace App\Services;

use App\Transformers\ArticleTransformer;
use App\Transformers\CardTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\EventTransformer;
use Illuminate\Support\Collection;

class FractalService
{
    /**
     * @param Collection $featuredArticles
     * @return mixed
     */
    public static function transformFeaturedArticles($featuredArticles){
        return $featuredArticles->mapWithKeys(function($item, $key){
            return [$key => fractal($item, new CardTransformer())->toArray()];
        });
    }

    public static function transformFeaturedCategories($featuredCategories)
    {
        return fractal()
            ->collection($featuredCategories, new CategoryTransformer())
            ->parseIncludes('articles')
            ->toArray();
    }

    public static function transformCards($entity)
    {
        return fractal()
            ->collection($entity, new CardTransformer())
            ->toArray();
    }

    public static function transformArticle($article)
    {
        return fractal()
            ->item($article, new ArticleTransformer())
            ->toArray();
    }

    public static function transformEvent($event)
    {
        return fractal()
            ->item($event, new EventTransformer())
            ->toArray();
    }
}