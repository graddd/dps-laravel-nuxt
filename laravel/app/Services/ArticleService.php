<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use App\Traits\CrudTrait;
use Illuminate\Support\Collection;
use PHPHtmlParser\Dom;

class ArticleService
{
    /**
     * C R U D
     * -------
     * Many fields are non-standard Backpack\CRUD fields and must thus be treated differently.
     * Sync pivoting relationships via trait.
     */
    protected $pivotRelations = ['tags', 'events', 'references'];
    use CrudTrait { syncPivotRelations as protected; }

    /**
     *
     * Crud create
     * @param array $data
     * @return Article
     */
    public function crudCreate($data){
        $article = \DB::transaction(function () use ($data) {
            $article = new Article($data);
            $article->category()->associate($data['category_id']);
            $article->save();

            $this->syncPivotRelations($article, $data);

            return $article;
        });
        return $article;
    }

    /**
     * @param array $data
     * @param Article $article
     * @return Article
     */
    public function crudUpdate($article, $data){
        $article = \DB::transaction(function () use ($data, $article) {
            $article->category()->associate($data['category_id']);
            $this->syncPivotRelations($article, $data);
            $article->update($data);
            return $article;
        });

        return $article;
    }

    /**
     * Helpers for api
     * ---------------
     */

    /**
     * Gets article filtering options
     */
    public static function getOptions() {
        return [
            'categories' => Category::all()->toArray(),
            'tags' => Tag::all()->toArray(),
            'sort_order' => collect(config('dps.articles.sort_order_options'))->values()
        ];
    }

    /**
     * Get sort/order option from config
     * @param $key
     * @return
     */
    public static function getSortOrderOption($key){
        return config('dps.articles.sort_order_options')[$key];
    }

    /**
     * @param $params
     * @param string $defaultSortOrder
     * @return Collection
     */
    public static function createSearchParams($params, $defaultSortOrder)
    {
        $params = SearchService::prepareParams($params, 'article');
        if(!$params->has('sort_order') || collect($params['sort_order'])->isEmpty()) {
            $params['sort_order'] = static::getSortOrderOption($defaultSortOrder);
        }
        if(!$params->has('filters')) {
            $params['filters'] = ['category' => '', 'tags' => []];
        }
        return $params;
    }
}