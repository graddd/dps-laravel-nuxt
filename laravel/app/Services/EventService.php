<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Event;
use App\Models\Tag;
use App\Repositories\EventRepository;
use App\Repositories\LocationRepository;
use App\Traits\CrudTrait;
use Auth;
use Illuminate\Support\Collection;

class EventService
{
    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    /**
     * C R U D
     * -------
     * Many fields are non-standard Backpack\CRUD fields and must thus be treated differently.
     * Sync pivoting relationships via trait.
     */
    protected $pivotRelations = ['tags', 'articles', 'initiatives', 'locations', 'references'];
    use CrudTrait { syncPivotRelations as protected; }

    /**
     *
     * Crud create
     * @param array $data
     * @return Event
     */
    public function crudCreate($data){
        $event = \DB::transaction(function () use ($data) {
            $event = new Event($data);
            $event->category()->associate($data['category_id']);
            $event->save();

            $this->syncPivotRelations($event, $data);
            $event->users()->attach(Auth::user()->id);

            return $event;
        });
        return $event;
    }

    /**
     * @param array $data
     * @param Event $event
     * @return Event
     */
    public function crudUpdate($event, $data){
        $event = \DB::transaction(function () use ($data, $event) {
            $event->category()->associate($data['category_id']);
            $this->syncPivotRelations($event, $data);
            $event->update($data);
            // TODO: making it possible for other users to edit event

            return $event;
        });

        return $event;
    }

    /**
     * Helpers for api
     * ---------------
     */

    /**
     * Gets article filtering options
     */
    public static function getOptions() {
        return [
            'categories' => Category::all()->toArray(),
            'tags' => Tag::all()->toArray(),
            'sort_order' => collect(config('dps.events.sort_order_options'))->values()
        ];
    }

    /**
     * Get sort/order option from config
     * @param $key
     * @return
     */
    public static function getSortOrderOption($key){
        return config('dps.events.sort_order_options')[$key];
    }

    /**
     * @param $params
     * @param string $defaultSortOrder
     * @return Collection
     */
    public static function createSearchParams($params, $defaultSortOrder)
    {
        $params = SearchService::prepareParams($params, 'event');
        if(!$params->has('sort_order') || collect($params['sort_order'])->isEmpty()) {
            $params['sort_order'] = static::getSortOrderOption($defaultSortOrder);
        }
        if(!$params->has('filters')) {
            $params['filters'] = ['category' => '', 'tags' => []];
        }
        return $params;
    }
}