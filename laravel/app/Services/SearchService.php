<?php

namespace App\Services;

use Illuminate\Support\Collection;

class SearchService
{
    /**
     * @param Collection $collection
     * @return mixed
     */
    public static function spread($collection) {
        $spreaded = collect()->merge($collection['sort_order'])->only(['sort', 'order'])->merge($collection['filters']);
        return $spreaded;
    }

    public static function prepareParams($params, $string)
    {
        return $params ? collect(json_decode($params, true)[$string]) : collect();
    }
}