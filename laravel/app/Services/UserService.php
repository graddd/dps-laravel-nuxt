<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 6/21/17
 * Time: 3:08 PM
 */

namespace App\Services;


use App\Models\User;
use Facebook\Exceptions\FacebookSDKException;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class UserService
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function manageFacebookUser(\Facebook\GraphNodes\GraphUser $facebook_user)
    {
        $email = $facebook_user->getEmail();

        /** @var User $user */
        $user = User::updateOrCreate(compact('email'), [
            'name' => $facebook_user->getName(),
            'email' => $email
        ]);

        $user->profile()->updateOrCreate([
            'facebook' => $facebook_user->getId()
        ], [
            'facebook' => $facebook_user->getId(),
            'photo_url' => $facebook_user->getPicture()['url']
        ]);

        return $user;
    }

    public function manageFacebookToken(LaravelFacebookSdk $facebook)
    {
        $token = null;
        try {
            $token = $facebook->getJavaScriptHelper()->getAccessToken();
        } catch (FacebookSDKException $e) {
            // Failed to obtain access token
            dd($e->getMessage());
        }

        if (! $token) {
            // Get the redirect helper
            $helper = $facebook->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        // extend token life
        if (! $token->isLongLived()) {
            $oauth_client = $facebook->getOAuth2Client();

            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        return $token;
    }
}