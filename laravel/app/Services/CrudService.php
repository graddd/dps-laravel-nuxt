<?php
namespace App\Services;

use Illuminate\Support\Collection;

class CrudService
{

    /**
     * @param string $value
     * @return Collection
     */
    public static function getAjaxSelect2MultipleValue($value){
        $parsed = collect(json_decode("[".$value."]"));
        return $parsed->forget(0)->values();
    }

    public function select2MultipleOldOrValue($field, $connected_entity_entry){
        if(old($field["name"])){
            return in_array($connected_entity_entry->getKey(), old( $field["name"]));
        }elseif(isset($field['value'])){
            return in_array($connected_entity_entry->getKey(),
                $field['value']->pluck($connected_entity_entry->getKeyName())->toArray());
        }else{
            return false;
        }
    }
}