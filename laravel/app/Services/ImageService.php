<?php
namespace App\Services;

class ImageService
{
    /**
     * @param $entity
     * @param $value
     * @param array $options
     * @return null|string
     */
    public static function makeImage($entity, $value, $options = ['path' => '']){
        $disk = config('dps.uploads_disk');

        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($entity->image);

            // set null in the database column
            $store_value = null;
        }
        // if a base64 was sent, store it in the db
        elseif (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value)->resize($options['width'], $options['height']);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $isUploaded = \Storage::disk($disk)->put($options['path'].$filename, $image->stream('jpg', $options['quality']));
            if(!$isUploaded){
                \Log::alert('Image not uploaded for article ' . $entity->name . '! Check ' . $options['path'].$filename);
            }
            // 3. Save the path to the database
            $store_value = '/'.$options['path'].$filename;
        }
        // seeders
        else
        {
            $store_value = $value;
        }
        return $store_value;
    }
}