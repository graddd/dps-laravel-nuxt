<?php

namespace App\Services;

use Illuminate\Translation\Translator;

class EmailService
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function translate($key)
    {
        $trans = $this->translator->trans('sendinblue');
        if(array_key_exists($key, $trans)){
            return $trans[$key];
        }else{
            return $trans['general_error'];
        }
    }
}