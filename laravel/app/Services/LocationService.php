<?php
namespace App\Services;

use App\Models\Location;
use App\Repositories\LocationRepository;

class LocationService
{
    public function __construct(LocationRepository $locationRepository)
    {
        $this->location = $locationRepository;
    }

    static public function dataFromInitativeForm($location)
    {
        return array_merge(static::dataFromJSON($location['json']), array_except($location, 'json'));
    }

    /**
     * @param $data
     * @return Location
     * @internal param $json
     */
    public function whereMatchesAllOrFirst($data)
    {
        $data = $this->roundFloats($data);
        Location::create($data);
        $existingLocation = $this->location->matchAll($data);
        if($existingLocation){
            return $existingLocation;
        }else{
            return Location::create($data);
        }
    }

    private function roundFloats($data)
    {
        // lat, lng in $data need to be rounded to 6 decimal places because they are stored as such in DB
        $floats = ['lat', 'lng'];
        foreach ($floats as $float) {
            $data[$float] = round((float) $data[$float], 6);
        }
        return $data;
    }
}