<?php
namespace App\Services;

use App\Repositories\MenuItemRepository;
use App\Interfaces\MenuItemRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SpaService {

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Collection
     */
    private $commits;

    /**
     * @var Collection
     */
    private $meta;

    private static $nonChangeableMetaKeys = ['domain'];

    private static $documentTitleGlue = " | ";

    private static $metadataInfo = [
        'commit_name' => 'update_metadata',
        'header_attr' => 'x-metadata-json'
    ];

    /**
     * @var bool
     */
    private $injectMetadata = true;

    /**
     * SpaService constructor. Here, default commits are defined, e.g. navigation links.
     *
     * @param MenuItemRepositoryInterface $menuItem
     * @param Request $request
     */
    public function __construct(Request $request, MenuItemRepositoryInterface $menuItem)
    {
        $this->request = $request;
        // These commits are always present, e.g. navigation links.
        $defaultCommits = [
            'header/update_links' => $menuItem->hierarchy(true),
            'footer/update_information' => [
                'website_name' => config('settings.website_name'),
                'address' => config('settings.address'),
                'contact_email' => config('settings.contact_email'),
                'privacy_cookies_link' => config('settings.privacy_cookies_link'),
            ]
        ];
        $this->commits = collect($defaultCommits);

        // This metadata is defaulted to if left empty.
        // Preferably, use config('settings.x') as this is connected to BackpackSettings and can be filled out in
        // administration. To provide such options in administration, seed with SettingsSeeder.
        $defaultMeta = [
            'title' => config('settings.website_name'),
            'description' => config('settings.website_description'),
            'keywords' => config('settings.website_keywords'),
            'image' => config('settings.og_image'),
            'domain' => config('settings.website_domain'),
            'type' => 'website', // you can override this with 'article'
            'breadcrumb' => [
                ['name' => config('settings.website_domain'), 'to' => ['name' => 'index']]
            ]
        ];
        $this->meta = collect($defaultMeta);
    }

    /**
     * Adds commits to $commits variable. May be used throughout application, e.g. in middlewares etc.
     *
     * @param array $commits
     */
    public function mergeCommits(array $commits) {
        $this->commits = $this->commits->merge($commits);
    }

    /**
     * Merges with $meta variable. May also be used throughout application.
     * @param array $meta
     */
    public function meta(array $meta){
        $meta = collect($meta)->diffKeys(self::$nonChangeableMetaKeys);
        $this->meta = $this->meta->merge($meta);
    }

    public function crumb(array $crumbs){
        $this->meta['breadcrumb'] = collect($this->meta['breadcrumb'])->merge($crumbs);
    }

    /**
     * Makes document title from title and domain.
     */
    private function makeDocumentTitle()
    {
        if(!$this->meta['title']){
            $documentTitle = ['document_title' => $this->meta['domain']];
        }else{
            $documentTitle = [
                'document_title' => implode(self::$documentTitleGlue, [$this->meta['title'], $this->meta['domain']])
            ];
        }
        $this->meta = $this->meta->merge($documentTitle);
    }

    private function commitMeta() {
        $this->commits = $this->commits->merge([
            self::$metadataInfo['commit_name'] => $this->meta
        ]);
    }

    public function respond(array $commits){
        $this->makeDocumentTitle();
        $this->mergeCommits($commits);
        $this->commitMeta(); // we have to commit meta to store as well if we wish for vue-meta to update meta tags

        return $this->json(); //$this->isAjax() ? $this->json() : $this->view();
    }

    /**
     * Returns ajax json response with metadata in response header.
     * @return JsonResponse
     */
    private function json(){
        /** @var JsonResponse $jsonResponse */
        $jsonResponse = response()->json($this->commits);

        return $jsonResponse;
    }

    public function isAjax() {
        return $this->request->headers->contains('X-Requested-With', 'XMLHttpRequest');
    }

    /**
     * Getter/setter for injectMetadata property.
     *
     * @param bool|null $bool
     * @return bool|null
     */
    public function injectMetadata($bool = null)
    {
        if($bool instanceof bool) {
            $this->injectMetadata = $bool;
        }elseif($bool === null){
            return $this->injectMetadata;
        }
    }
}