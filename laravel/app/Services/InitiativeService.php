<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 7/8/16
 * Time: 8:33 PM
 */

namespace App\Services;


use App\Models\Initiative;
use App\Models\Audience;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Location;
use App\Repositories\InitiativeRepository;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class InitiativeService
{

    public function __construct(Initiative $initiative)
    {
        $this->initiative = $initiative;
    }

    /**
     * C R U D
     * -------
     * Location, audience and contact are non-standard Backpack\CRUD fields and must thus be treated differently.
     * Methods are defined in Services\InitiativeService and Repositories\InitiativeRepository.
     *
     * Crud create
     * @param array $data
     * @return Initiative
     */
    public function crudCreate($data){
        $data = static::prepareData($data);
        $initiative = \DB::transaction(function () use ($data) {
            /** @var Initiative $initiative */
            $initiative = new Initiative($data);
            $initiative->category()->associate($data['category']);
            $initiative->save();
            /**
             * TODO:
             * - create url
             */
            $contact = new Contact($data['contact']);
            $initiative->contacts()->save($contact);
            /** @var Location $location */
            # in future, $data['locations'] will be array of JSON strings because one initiative will be able to have multiple locations
            $location = LocationService::whereJsonOrFirst($data['locations']);
            $initiative->locations()->sync([$location->id]);
            $audience = static::audienceForSync($data['audience'], $data['audience_other']);
            $initiative->audience()->sync($audience);
            $initiative->users()->attach(Auth::user()->id);
            return $initiative;
        });
        return $initiative;
    }

    /**
     * @param array $data
     * @param Initiative $initiative
     * @return Initiative
     */
    public function crudUpdate($initiative, $data){
        $data = static::prepareData($data);
        $initiative = \DB::transaction(function () use ($data, $initiative) {
            $initiative->contacts()->sync($data['contact']);
            /** @var Location $location */
            # in future, $data['locations'] will be array of JSON strings because one initiative will be able to have multiple locations
            $location = LocationService::whereJsonOrFirst($data['locations']);
            $initiative->locations()->sync([$location->id]);
            $initiative->audience()->sync(InitiativeService::audienceForSync($data['audience'], $data['audience_other']));
            $initiative->category()->associate($data['category']);
            $initiative->update($data);
            // TODO: making it possible for other users to edit event
            return $initiative;
        });

        return $initiative;
    }

    /**
     * Prepares array if audience ids for sync with potential "name" column filled out for "Other" audience.
     *
     * @param array $audience
     * @param string $audience_other
     * @return array
     */
    public static function audienceForSync($audience, $audience_other = NULL)
    {
        if($audience_other != NULL){
            $other_id = config('initiatives.audience_other_id');
            $return = [$other_id => ['name' => $audience_other]];
            foreach ($audience as $id){
                $return[] = $id;
            }
        }else{
            $return = $audience;
        }
        return $return;
    }

    /**
     * Adds empty array values for audience and categories when no multiple-select option is chosen.
     * (Multiple select form element does not get submitted at all (not even as empty array).)
     * Also, creates url slug.
     *
     * @param array $data
     * @return array
     */
    public static function prepareData($data)
    {
        if(!isset($data['tags'])){
            $data['tags'] = [];
        }
        if(!isset($data['audience'])){
            $data['audience'] = [];
        }
        return $data;
    }

    /**
     * Api helper functions.
     */
    
    

    /**
     * @param Collection $initiatives
     * @return Collection
     */
    public static function prepareForApi(Collection $initiatives)
    {
        $initiatives = $initiatives->keyBy('id');
        return $initiatives->map(function (Initiative $initiative) {
            /** @var Collection $collection */
            $collection = collect($initiative);
            /**
             * When fetching initiatives, we have to see whether or not it is within boundary.
             * If fetching initiatives, then $initiative->location is present, if fetching markers, it is not.
             */
            if(isset($collection['locations'])){
                $locationCollection = collect($collection['locations']);
//                $collection['within_bounds'] = false;
//                foreach($locationCollection as $location){
//                    /** @var Location $location */
//                    if($location['within_boundary']){
//                        $collection['within_bounds'] = true;
//                        break;
//                    }
//                }
                $collection['locations'] = $locationCollection->keyBy('id');
            }
            return $collection->all();
        });
    }

    /**
     * @param Request $request
     * @return Initiative
     * @throws \Exception
     */
    public function createFromRequest(Request $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->except(['audience', 'categories', 'contacts', 'location', 'logo', 'cover']);
            /** @var Initiative $initiative */
            $initiative = Initiative::create($input);

            $this->setupCategories($request->get('categories'), $initiative);
            $this->setupAudience($request->get('audience'), $initiative);
            $this->setupContacts($request->get('contacts'), $initiative);

            $initiative->users()->attach(\Auth::guard('api')->user()->id);
            $initiative->locations()->save(new Location($request->get('location')));

            // all good
        } catch (\Exception $e) {
            // something went wrong
            DB::rollback();

            dd($e->getTraceAsString());

            throw new \Exception('Error creating initiative: ' . $e->getMessage());
        }

        DB::commit();

        return $initiative;
    }

    private function setupCategories($input, Initiative $initiative)
    {
        $categories = [];
        foreach ($input as $category) {
            $categories[] = $category['id'];
        }
        $initiative->categories()->sync($categories);
    }

    private function setupAudience($input, Initiative $initiative)
    {
        $audience = [];
        foreach($input as $a) {
            $audience[] = $a['id'];
        }

        $initiative->audience()->sync($audience);

    }

    private function setupContacts($input, Initiative $initiative)
    {

        $contacts = [];
        foreach($input as $contact) {
            $contacts[] = new Contact($contact);
        }

        $initiative->contacts()->saveMany($contacts);

    }
}