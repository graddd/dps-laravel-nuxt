<?php

namespace App\Transformers;

use App\Models\Event;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category',
        'tags',
        'articles',
        'references',
        'location'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Event $event
     * @return array
     */
    public function transform(Event $event)
    {
        return [
            'name' => $event->name,
            'slug' => $event->slug,
            'start_at' => $event->start_at->toDateTimeString(),
            'end_at' => $event->end_at->toDateTimeString(),
            'content_short' => $event->content_short,
            'content_rich' => $event->content_rich,
            'image' => $event->image,
            'headings' => $event->headings,
        ];
    }

    public function includeCategory(Event $event) {
        return $this->item($event->category, new CategoryTransformer());
    }

    public function includeLocation(Event $event) {
        return $this->item($event->locations[0], new LocationTransformer());
    }

    public function includeTags(Event $event) {
        return $this->collection($event->tags, new TagTransformer());
    }

    public function includeArticles(Event $event) {
        return $this->collection($event->articles, new CardTransformer());
    }

    public function includeReferences(Event $event) {
        return $this->collection($event->references, new ReferenceTransformer());
    }
}
