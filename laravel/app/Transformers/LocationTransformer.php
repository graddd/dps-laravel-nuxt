<?php
namespace App\Transformers;

use App\Models\Location;
use League\Fractal\TransformerAbstract;

class LocationTransformer extends TransformerAbstract
{

    /**
     * LocationTransformer constructor.
     * @param Location $location
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'name' => $location->name,
            'position' => $location->position,
            'address' => explode(", ", $location->formatted_address),
            'url' => $location->url
        ];
    }
}