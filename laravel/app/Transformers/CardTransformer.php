<?php

namespace App\Transformers;

use App\Models\Article;
use App\Models\Event;
use League\Fractal\TransformerAbstract;

class CardTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Article $entity
     * @return array
     */
    public function transform($entity)
    {
        $model = class_basename($entity);

        switch ($model) {
            case 'Article':
                /** @var Article $entity */
                if($entity->status == 'REDIRECT TO EVENT' && $entity->events->count() === 1){
                    return $this->transform($entity->events[0]);
                }

                $specific = [
                    'date' => $entity->date->toDateTimeString()
                ];
                break;
            case 'Event':
                /** @var Event $entity */
                $specific = [
                    'start_at' => $entity->start_at->toDateTimeString(),
                    'end_at' => $entity->end_at->toDateTimeString()
                ];
                break;
            default:
                $specific = [];
        }

        $common = [
            'model' => $model,
            'name' => $entity->name,
            'slug' => $entity->slug,
            'category' => $entity->category,
            'content_short' => $entity->content_short,
            'image' => $entity->image,
            'image_cover' => $entity->image_cover
        ];

        return array_merge($common, $specific);
    }

    /**
     * @param \App\Models\Category|\App\Models\Initiative\Category $entity
     * @return \League\Fractal\Resource\Item
     */
    public function includeCategory($entity)
    {
        return $this->item($entity->category, new CategoryTransformer());
    }
}
