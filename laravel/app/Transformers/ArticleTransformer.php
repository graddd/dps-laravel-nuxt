<?php

namespace App\Transformers;

use App\Models\Article;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category',
        'tags',
        'events',
        'references'
    ];

    /**
     * A Fractal transformer.
     *
     * @param Article $article
     * @return array
     */
    public function transform(Article $article)
    {
        return [
            'name' => $article->name,
            'slug' => $article->slug,
            'date' => $article->date->toDateTimeString(),
            'content_short' => $article->content_short,
            'content_rich' => $article->content_rich,
            'image' => $article->image,
            'image_cover' => $article->image_cover,
            'headings' => $article->headings,
            'status' => $article->status,
        ];
    }

    public function includeCategory($article) {
        return $this->item($article->category, new CategoryTransformer());
    }

    public function includeTags($article) {
        return $this->collection($article->tags, new TagTransformer());
    }

    public function includeEvents($article) {
        return $this->collection($article->events, new CardTransformer());
    }

    public function includeReferences($article) {
        return $this->collection($article->references, new ReferenceTransformer());
    }
}
