<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'articles'
    ];

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Category $category
     * @return array
     */
    public function transform($category)
    {
        return [
            'name' => $category->name,
            'slug' => $category->slug,
            'description' => $category->description,
            'is_project' => $category->is_project
        ];
    }

    /**
     * @param $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeArticles($category){
        return $this->collection($category->articles, new CardTransformer());
    }
}
