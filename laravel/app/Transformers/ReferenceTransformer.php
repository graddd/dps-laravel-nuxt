<?php

namespace App\Transformers;

use App\Models\Reference;
use League\Fractal\TransformerAbstract;

class ReferenceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Reference $reference
     * @return array
     */
    public function transform(Reference $reference)
    {
        return [
            'name' => $reference->name,
            'link' => $reference->link,
            'description' => $reference->description,
            'type' => $reference->type
        ];
    }
}
