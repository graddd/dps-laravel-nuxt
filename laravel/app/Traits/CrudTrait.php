<?php

namespace App\Traits;

use App\Services\LocationService;

trait CrudTrait
{
    protected function syncPivotRelations($entity, $data)
    {
        foreach ($this->pivotRelations as $relation) {
            if ($relation === 'locations' && isset($data[$relation])) {
                // currently, $data['locations'] must be a json string, thus only one location is supported,
                // although models relating to locations are always in morphToMany relation to them

                /** @var LocationService $locationService */
                $locationService = $this->locationService;
                $location = $locationService->whereMatchesAllOrFirst($data[$relation]);
                $entity->{$relation}()->sync([$location->id]);
            } else {
                if (!isset($data[$relation])) {
                    $data[$relation] = [];
                }
                $entity->{$relation}()->sync($data[$relation]);
            }
        }

    }
}