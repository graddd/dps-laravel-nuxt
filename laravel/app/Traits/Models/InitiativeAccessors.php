<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 6/4/17
 * Time: 2:03 PM
 */

namespace App\Traits\Models;

/**
 * Class InitiativeAccessors
 * @package App\Traits\Models
 * @mixin \App\Models\Initiative
 */
trait InitiativeAccessors
{
    public function getAudienceSizeValueAttribute()
    {
        return trans(config('initiatives.audience_size')[$this->attributes['audience_size']]);
    }

    public function getStatusValueAttribute()
    {
        return trans(config('initiatives.status')[$this->attributes['status']]);
    }

//    public function getLogoAttribute()
//    {
//        $path = 'initiative/' . $this->attributes['id'] . '/logo';
//        return $this->attributes['logo_url'] ?: \Storage::url($path);
//    }

    public function getLocationTypeValueAttribute()
    {
        return trans(config('initiatives.location_type')[$this->attributes['location_type']]);
    }

    public function getAudienceOtherAttribute()
    {
        if(!$this->audience->where('id', config('initiatives.audience_other_id'))){
            return $this->audience->where('id', config('initiatives.audience_other_id'))->first()->pivot->name;
        }else{
            return null;
        }
    }
}