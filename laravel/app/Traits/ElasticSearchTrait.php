<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 8/21/17
 * Time: 8:53 PM
 */

namespace App\Traits;

trait ElasticSearchTrait
{
    public function elasticSearch($search, $params)
    {
        if(empty($params['sort'])) $params['sort'] = '_score';
        if(empty($params['order'])) $params['order'] = 'desc';
        $esQuery = $this->createElasticSearchQuery($search, $params);

        return $this->search($esQuery, function ($query) {
                return $this->hydrateElasticSearchQuery($query);
            })
            ->orderBy($params['sort'], $params['order']);
    }


    private function createElasticSearchQuery($search, $params)
    {
        $filters = [];
        foreach($this->elasticFilters as $filter) {
            if (!empty($params[$filter])) {
                $term = 'terms';
                if($filter === 'category') $term = 'term';
                $filters[][$term][$filter.'.keyword'] = $params[$filter];
            }
        }

        if (empty($filters)) return $this->prepareESQueryString($search);

        return [
            'bool' => [
                'must' => $this->prepareESQueryString($search),
                'filter' => $filters
            ],
        ];
    }

    abstract public function hydrateElasticSearchQuery($query);

    private function prepareESQueryString($search)
    {
        $search = $this->prepareSearchQueryString($search);
        return [
            "query_string" => [
                'fields' => $this->elasticFields,
                'query' => $search,
                'use_dis_max' => true,
                'default_operator' => 'AND',
                'rewrite' => 'scoring_boolean'
            ],
        ];
    }

    /**
     * Returns string with escaped elastic reserved characters.
     * These are + - = && || > < ! ( ) { } [ ] ^ " ~ * ? : \ /
     *
     * @param $string
     * @return mixed
     */
    private function escapeElasticSearchQueryString($string) {
        $regex = "/[\\+\\-\\=\\&\\|\\!\\(\\)\\{\\}\\[\\]\\^\\\"\\~\\*\\<\\>\\?\\:\\\\\\/]/";
        return preg_replace($regex, addslashes('\\$0'), $string);
    }

    /**
     * Prepares (i.e. formats and escapes) search query
     * @param string $query
     * @return string
     */
    private function prepareSearchQueryString($query) {
        $query = trim(mb_strtolower($this->escapeElasticSearchQueryString($query)));
        $query .= "*";
        return $query;
    }

}