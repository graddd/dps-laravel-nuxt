<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use stdClass;

/**
 * App\Models\Location
 *
 * @property int $id
 * @property string $name
 * @property float $lat
 * @property float $lng
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $geocomplete_value
 * @property string $formatted_address
 * @property string $route
 * @property string $street_number
 * @property string $postal_code
 * @property string $administrative_area_level_1
 * @property string $country
 * @property string $url
 * @property-read mixed $position
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereAdministrativeAreaLevel1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereFormattedAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereGeocompleteValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereStreetNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location whereUrl($value)
 * @mixin \Eloquent
 */
class Location extends Model
{
    use Searchable;

    protected $table = 'locations';

    protected $fillable = [
        'name', 'lat', 'lng', 'geocomplete_value', 'formatted_address', 'route', 'street_number', 'postal_code',
        'administrative_area_level_1', 'country', 'url'
    ];

    protected $hidden = [];
    protected $appends = ['position'];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float',
    ];

    protected $googleLocation;

    public function initiatives()
    {
        return $this->morphedByMany(Initiative::class, 'locatable');
    }

    public function events()
    {
        return $this->morphedByMany(Event::class, 'locatable');
    }

    public function getPositionAttribute()
    {
        return [
            'lat' => (float) $this->attributes['lat'],
            'lng' => (float) $this->attributes['lng']
        ];
    }

    /**
     * Returns true if Location's [lat, lng] point is within given boundaries.
     * Boundaries are in form stdObject{south: Float, west: Float, north: Float, east: Float}
     *
     * @param mixed $boundaries
     * @return bool
     */
    public function isWithinBounds($boundaries)
    {
        return $this->lat >= $boundaries->south && $this->lat <= $boundaries->north
            && $this->lng >= $boundaries->west && $this->lng <= $boundaries->east;
    }

}
