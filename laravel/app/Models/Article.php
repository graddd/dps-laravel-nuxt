<?php

namespace App\Models;

use App\Services\ImageService;
use App\Services\RichContentService;
use App\Traits\ElasticSearchTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Date;
use Illuminate\Database\Query\Builder;
use Laravel\Scout\Searchable;

/**
 * App\Models\Article
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 * @property string $content
 * @property string $content_short
 * @property string|null $image
 * @property string|null $image_cover
 * @property string|null $images
 * @property string|null $link
 * @property string|null $status
 * @property \Carbon\Carbon $date
 * @property string $featured
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $content_rich
 * @property-read mixed $headings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reference[] $references
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article featured()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article publishedOrRedirectToEvent()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereContentShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereImageCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class Article extends Model
{
    use CrudTrait;
    use Sluggable, SluggableScopeHelpers;
    use Searchable;
    use ElasticSearchTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'articles';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = ['slug', 'name', 'content', 'content_short', 'image', 'image_cover', 'status', 'category_id',
        'featured', 'date', 'link'];

    protected $visible = ['id', 'name', 'slug', 'image', 'image_cover', 'content_short', 'date', 'status', 'created_at',
        'category', 'tags', 'link', 'featured', 'headings', 'content_rich', 'events', 'references'];

    protected $appends = ['headings', 'content_rich'];

    protected $dates = ['date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'article_event');
    }

    public function references()
    {
        return $this->morphToMany(Reference::class, 'referenceable');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopePublished($query)
    {
        return $query->where('status', 'PUBLISHED');
    }

    public function scopePublishedOrRedirectToEvent($query)
    {
        return $query
            ->where('status', 'PUBLISHED')
            ->orWhere('status', 'REDIRECT TO EVENT');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', '!=', 'regular');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getHeadingsAttribute() {
        if(!is_null($this->content)) {
            return RichContentService::getHeadings($this->content);
        }
    }

    public function getContentRichAttribute() {
        if(!is_null($this->content)) {
            return RichContentService::prepareContent($this->content);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $options = config('dps.articles.images.square');
        $this->attributes[$attribute_name] = ImageService::makeImage($this, $value, $options);

    }

    public function setImageCoverAttribute($value)
    {
        $attribute_name = "image_cover";
        $options = config('dps.articles.images.cover');
        $this->attributes[$attribute_name] = ImageService::makeImage($this, $value, $options);
    }

    /*
    |--------------------------------------------------------------------------
    | Elastic search
    |--------------------------------------------------------------------------
    */

    protected $elasticFilters = ['tags', 'category'];
    protected $elasticFields = ['name^3', 'content_short^2', 'content'];

    public function searchableAs()
    {
        return 'articles';
    }

    public function toSearchableArray()
    {
        $array = array_only($this->toArray(), ['name']);
        $array['category'] = $this->category->name;
        $array['tags'] = $this->tags->pluck('name');
        $array['date'] = $this->date->format('Y-m-d H:i:s');
        $array['content_short'] = $this->content_short;
        $array['content'] = $this->content;

        return $array;
    }

    /**
     * @param self $query
     * @return mixed
     */
    public function hydrateElasticSearchQuery($query)
    {
        return $query->with(['category', 'tags'])->published();
    }
}
