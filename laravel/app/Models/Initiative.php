<?php

namespace App\Models;

use App\Traits\Models\InitiativeAccessors;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * App\Models\Initiative
 *
 * @property int $id
 * @property int $initiative_category_id
 * @property string $slug
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property string $keywords
 * @property int $status
 * @property string $url
 * @property string|null $logo
 * @property string|null $cover
 * @property string $video_url
 * @property int $audience_size
 * @property int $group_size
 * @property int $area_size
 * @property int $location_type
 * @property int $is_approved
 * @property string $start_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Audience[] $audience
 * @property-read \App\Models\Initiative\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $audience_other
 * @property-read mixed $audience_size_value
 * @property-read mixed $location_type_value
 * @property-read mixed $status_value
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $locations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Initiative\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative filterAudience($audience = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative filterCategories($categories = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereAreaSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereAudienceSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereGroupSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereInitiativeCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereIsApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereLocationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative whereVideoUrl($value)
 * @mixin \Eloquent
 */
class Initiative extends Model
{
    use CrudTrait, Searchable, InitiativeAccessors, Sluggable;

    protected $table = 'initiatives';

    protected $fillable = [
        'name', 'slug', 'url', 'logo', 'cover', 'start_at', 'audience_size',
        'group_size', 'area_size', 'location_type', 'location_id', 'contact_id', 'description', 'status',
        'keywords', 'short_description', 'is_approved'
    ];

    protected $visible = [
        'id', 'name', 'slug', 'url', 'start_at', 'audience_size', 'status', 'description', 'short_description', 'keywords',
        'category', 'audience', 'locations', 'cover', 'logo', 'audience_size_value', 'status_value', 'group_size',
        'location_type_value', 'is_approved', 'contacts', 'tags'
    ];

    protected $appends = [
        'audience_size_value', 'status_value', 'location_type_value'
    ];

    protected $notNullables = ['contact_id', 'location_id', 'category_id', 'audience_id', 'name'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /* ******************************************
     * ********** MODEL RELATIONS ***************
     * ******************************************/


    public function category()
    {
        return $this->belongsTo(Initiative\Category::class, 'initiative_category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Initiative\Tag::class, 'initiative_initiative_tags', 'initiative_id', 'initiative_tag_id');
    }
    
    public function users()
    {
        return $this->belongsToMany(User::Class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Audience
     */
    public function audience()
    {
        return $this->belongsToMany(Audience::class, 'initiative_audience')->withPivot('name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'initiative_id');
    }

    public function locations()
    {
        return $this->morphToMany(Location::class, 'locatable');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_initiative');
    }

    public function delete()
    {
        $this->locations()->detach();
        return parent::delete();
    }

    /* ******************************************
     * ********** MODEL ACCESSORS ***************
     * ******************************************/

//    public function getCategoryNameAttribute($value)
//    {
//        return $this->category->name === 'other' ? $this->otherCategory->name : trans('categories.' . $value);
//    }



    /**
     * Mutators
     */
//    public function setLogoUrlAttribute($value)
//    {
//        $attribute_name = "logo_url";
//        $disk = "public";
//        $destination_path = "uploads/logos";
//
//        // if the image was erased or new one uploaded, delete old one
//        if ($value==null || (isset($this->{$attribute_name}) && starts_with($value, 'data:image'))) {
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//            $this->attributes[$attribute_name] = null;
//        }
//
//        if (starts_with($value, 'data:image'))
//        {
//            $w = config('initiatives.images.logo.size');
//            $image = \Image::make($value)->resize($w, $w);
//            $filename = md5($value.time()).'.jpg';
//            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
//            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
//        }
//    }
//
//    public function setCoverPhotoUrlAttribute($value)
//    {
//        $attribute_name = "cover_photo_url";
//        $disk = "public";
//        $destination_path = "uploads/covers";
//
//        // if the image was erased or new one uploaded, delete old one
//        if ($value==null || (isset($this->{$attribute_name}) && starts_with($value, 'data:image'))) {
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//            $this->attributes[$attribute_name] = null;
//        }
//
//        if (starts_with($value, 'data:image'))
//        {
//            $w = config('initiatives.images.cover_photo.width');
//            $h = config('initiatives.images.cover_photo.height');
//            $image = \Image::make($value)->resize($w, $h);
//            $filename = md5($value.time()).'.jpg';
//            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
//            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
//        }
//    }

    public function searchableAs()
    {
        return 'initiatives';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [
            'name' => $this->name,
            'short_description' => $this->short_description,
            'tags' => $this->tags->pluck('name'),
            'category' => $this->category ? $this->category->name : null,
            'audience' => $this->audience->pluck('name'),
            'contact' => $this->contact ? $this->contact->name : null,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];


        $array['keywords'] = array_map(function($word) {
            return trim($word);
        }, explode(',', $this->keywords));

        $array['locations'] = collect($this->locations)->map(function (Location $location){
            $collection = collect($location);
            $collection['position'] = [(float) $location->lng, (float) $location->lat];
            return $collection->only(['name', 'position'])->all();
        })->toArray();

        return $array;
    }

    // scopes

    public function scopeFilterCategories($query, $categories = null) {
        if (empty($categories)) return $query;

        return $query->whereHas('categories', function ($query) use ($categories) {
            $query->whereIn('id', $categories);
        });
    }

    public function scopeFilterAudience($query, $audience = null) {
        if (empty($audience)) return $query;

        return $query->whereHas('audience', function ($query) use ($audience) {
            $query->whereIn('id', $audience);
        });
    }
}
