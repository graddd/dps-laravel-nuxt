<?php

namespace App\Models;

use App\Services\ImageService;
use App\Services\RichContentService;
use App\Traits\ElasticSearchTrait;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $category_id
 * @property string $website
 * @property string $content
 * @property string $content_short
 * @property string|null $image
 * @property \Carbon\Carbon $start_at
 * @property \Carbon\Carbon $end_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $image_cover
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article[] $articles
 * @property-read \App\Models\Category $category
 * @property-read mixed $content_rich
 * @property-read mixed $headings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Initiative[] $initiatives
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $locations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reference[] $references
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereContentShort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereImageCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereWebsite($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use CrudTrait, Searchable, Sluggable;
    use Searchable;
    use ElasticSearchTrait;

    protected $table = 'events';

    protected $fillable = ['name', 'slug', 'start_at', 'end_at', 'website', 'content', 'content_short', 'image', 'image_cover', 'category_id'];

    protected $visible = [
        'name', 'slug', 'start_at', 'end_at', 'website', 'content_short', 'content_rich', 'image', 'image_cover',
        'category', 'tags', 'headings', 'references'
    ];

    protected $appends = ['headings', 'content_rich'];

    protected $dates = ['start_at', 'end_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Relations
     */

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function initiatives()
    {
        return $this->belongsToMany(Initiative::class, 'event_initiative');
    }

    public function locations()
    {
        return $this->morphToMany(Location::class, 'locatable');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'event_user');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_event');
    }

    public function references()
    {
        return $this->morphToMany(Reference::class, 'referenceable');
    }

    public function delete()
    {
        $this->locations()->detach();
        return parent::delete();
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getHeadingsAttribute() {
        return RichContentService::getHeadings($this->content);
    }

    public function getContentRichAttribute() {
        return RichContentService::prepareContent($this->content);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $options = config('dps.events.images.square');
        $this->attributes[$attribute_name] = ImageService::makeImage($this, $value, $options);
    }

    public function setImageCoverAttribute($value)
    {
        $attribute_name = "image_cover";
        $options = config('dps.events.images.cover');
        $this->attributes[$attribute_name] = ImageService::makeImage($this, $value, $options);
    }

    /*
    |--------------------------------------------------------------------------
    | Elastic search
    |--------------------------------------------------------------------------
    */

    protected $elasticFilters = ['tags', 'category'];
    protected $elasticFields = ['name^3', 'content_short^2', 'content'];

    public function searchableAs()
    {
        return 'events';
    }

    public function toSearchableArray()
    {
        $array = array_only($this->toArray(), ['name']);
        $array['category'] = $this->category->name;
        $array['tags'] = $this->tags->pluck('name');
        $array['date'] = $this->start_at->format('Y-m-d H:i:s');
        $array['content_short'] = $this->content_short;
        $array['content'] = $this->content;

        return $array;
    }

    public function hydrateElasticSearchQuery($query)
    {
        return $query->with(['category', 'tags']);
    }
}
