<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User\Profile
 *
 * @property int $id
 * @property int $user_id
 * @property string $bio
 * @property string $photo_url
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereBio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile wherePhotoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Profile whereWebsite($value)
 * @mixin \Eloquent
 */
class Profile extends Model
{
    protected $table = 'user_profiles';

    protected $fillable = [
        'bio', 'photo_url', 'website', 'facebook', 'twitter'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
