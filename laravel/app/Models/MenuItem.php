<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MenuItem
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string|null $type
 * @property string|null $link
 * @property bool $in_navigation
 * @property int|null $article_id
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Article|null $article
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuItem[] $children
 * @property-read \App\Models\MenuItem|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereInNavigation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MenuItem extends Model
{
    use CrudTrait;

    protected $table = 'menu_items';
    protected $fillable = ['name', 'description', 'type', 'link', 'in_navigation', 'article_id', 'parent_id'];
    protected $visible = ['name', 'description', 'type', 'link', 'in_navigation', 'article', 'parent', 'children'];
    protected $casts = [
        'in_navigation' => 'boolean',
    ];

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }

    public function url()
    {
        switch ($this->type) {
            case 'external_link':
                return $this->link;
                break;

            case 'internal_link':
                return is_null($this->link) ? '#' : url($this->link);
                break;

            default: //page_link
                if ($this->page) {
                    return url($this->page->slug);
                }
                break;
        }
    }
}
