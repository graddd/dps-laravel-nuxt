<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reference
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $link
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reference whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reference extends Model
{
    use CrudTrait;

    protected $table = 'references';

    protected $fillable = ['name', 'link', 'description', 'type'];

    protected $visible = ['id', 'name', 'link', 'description', 'type', 'articles'];

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'referenceable');
    }

    /**
     * Mutators
     */

    /**
     * @param $value
     */
    public function setLinkAttribute($value)
    {
        $attribute_name = "link";
        $disk = config('dps.uploads_disk');
        $destination_path = "/uploads/references";

        if($this->type == 'attachment'){
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
        }else{
            // delete file, set value
            if(\Storage::disk($disk)->has($this->{$attribute_name})){
                // delete previous file if exists
                $this->uploadFileToDisk(null, $attribute_name, $disk, $destination_path);
            }
            $this->attributes[$attribute_name] = $value;
        }
    }

    /**
     * Handle file upload and DB storage for a file:
     * - on CREATE
     *     - stores the file at the destination path
     *     - generates a name
     *     - stores the full path in the DB;
     * - on UPDATE
     *     - if the value is null, deletes the file and sets null in the DB
     *     - if the value is different, stores the different file and updates DB value.
     *
     * @param $value
     * @param $attribute_name
     * @param $disk
     * @param $destination_path
     * @throws \Exception
     * @internal param $ [type] $value            Value for that column sent from the input.
     * @internal param $ [type] $attribute_name   Model attribute name (and column in the db).
     * @internal param $ [type] $disk             Filesystem disk used to store files.
     * @internal param $ [type] $destination_path Path in disk where to store the files.
     */
    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path)
    {
        $request = \Request::instance();

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($attribute_name) &&
            $this->{$attribute_name} &&
            $this->{$attribute_name} != null
        ) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $this->{$attribute_name} != null) {
            \Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = str_slug($this->name . ' ' . Carbon::now()->format('Y-m-d-H-i-s')) . '.' . $file->getClientOriginalExtension();

            // 2. Move the new file to the correct path
            if($file_path = $file->storeAs($destination_path, $new_file_name, $disk)){
                // 3. Save the complete path to the database
                $this->attributes[$attribute_name] = "/" . $file_path;
            }else{
                throw new \Exception('File not uploaded.');
            }
        }
    }

    /**
     * Delete file when model deleted
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function(Reference $reference) {
            $disk = config('dps.uploads_disk');
            \Storage::disk($disk)->delete($reference->link);
        });
    }
}
