<?php

namespace App\Models;

use App\Models\Initiative;
use App\Models\Initiative\Audience\Other;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Audience
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Initiative[] $initiatives
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Audience whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Audience whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Audience whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Audience whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Audience extends Model
{
    use CrudTrait;
    protected $table = 'audience';

    protected $fillable = ['name'];

    protected $visible = ['id', 'name'];

    public function initiatives()
    {
        return $this->belongsToMany(Initiative::class, 'initiative_audience')->withPivot('name');
    }
    
}
