<?php

namespace App\Models\Initiative;

use App\Models\Initiative;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Initiative\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rgt
 * @property int|null $depth
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Initiative\Category[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Initiative[] $initiatives
 * @property-read \App\Models\Initiative\Category|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Initiative\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use CrudTrait;
    protected $table = 'initiative_categories';

    protected $fillable = [
        'name', 'description'
    ];

    protected $visible = ['id', 'name', 'slug', 'description'];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function initiatives()
    {
        return $this->hasMany(Initiative::class, 'initiative_category_id');
    }
}
