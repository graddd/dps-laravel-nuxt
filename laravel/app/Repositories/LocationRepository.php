<?php
namespace App\Repositories;

use App\Interfaces\LocationRepositoryInterface;
use App\Models\Location;
use App\Services\InitiativeService;
use App\Services\MapService;

class LocationRepository implements LocationRepositoryInterface
{
    /**
     * @var Location
     */
    protected $location;

    public function __construct()
    {
        $this->location = new Location();
    }

    public function getAll()
    {
        return $this->location->all();
    }

    public function find($id)
    {
        return $this->location->findOrFail($id);
    }

    public function mapMarkers($filters)
    {
        return $this->location
            ->with(['initiatives'])
            ->whereHas('initiatives.category', function($query) use ($filters) {
                if(isset($filters['categories'])){
                    $query->whereIn('id', $filters['categories']);
                }
            })
            ->whereHas('initiatives.audience', function($query) use ($filters) {
                if(isset($filters['audience'])){
                    $query->whereIn('audience.id', $filters['audience']);
                }
            })
            ->get()
            ->keyBy('id')
            ->map(function (Location $marker) use ($filters){
                $initiatives = $marker->initiatives;
                $marker = collect($marker)->all();
                $marker['position'] = [
                    'lat' => (float) $marker['lat'],
                    'lng' => (float) $marker['lng']
                ];
                unset($marker['lat'], $marker['lng']);
                $marker['initiatives'] = InitiativeService::prepareForApi($initiatives)->all();
                return $marker;
            })->all();
    }

    public function matchAll($data)
    {
        $whereData = array_map(null, array_keys($data), array_values($data));
        $result = Location::where($whereData)->get();

        if($result->isEmpty()){
            return null;
        }else{
            return $result->first();
        }
    }
}