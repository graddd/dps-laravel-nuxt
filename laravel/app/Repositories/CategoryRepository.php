<?php

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Category;
use function foo\func;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getAll()
    {
        return $this->category->all();
    }

    public function find($id)
    {
        return $this->category
            ->with(['articles'])
            ->findOrFail($id);
    }

    public function getFeaturedCategories()
    {
        $categories = $this->category
            ->with(['articles' => function ($query) {
                $query
                    ->where('status', 'PUBLISHED')
                    ->where('featured', 'regular')
                    ->orderBy('date', 'DESC');
                // if you put a limit here, it will limit all articles instead of articles of each category.
            }, 'articles.category', 'articles.tags'])
            ->whereHas('articles', function ($query) {
                $query->where('articles.status', 'PUBLISHED');
                $query->where('articles.featured', 'regular');
            })
            ->where('type', 'featured')
            ->orderBy('lft', 'ASC')
            ->get();

        // eager load limiting must be dealt with a workaround
        $categories->each(function ($category) {
            $category->articles = $category->articles->take(6);
//            $category->sixArticles = $category->articles->take(6);
        });
//        // now just put what we have in sixArticles to articles
//        $categories = $categories->map(function ($category){
//            $category['articles'] = $category['sixArticles'];
//            unset($category['sixArticles']);
//            return $category;
//        });

        return $categories;
    }
}