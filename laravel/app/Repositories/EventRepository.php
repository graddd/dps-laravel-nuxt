<?php
/**
 * Created by PhpStorm.
 * User: krofek
 * Date: 7/30/17
 * Time: 9:05 PM
 */

namespace App\Repositories;


use App\Models\Event;
use Exception;

class EventRepository
{
    /**
     * @var Event
     */
    private $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function get($search = '', $params = [], $limit = 1e4)
    {
        $query = $this->event->elasticSearch($search, $params);
        $query->limit = $limit;

        return $query->get();
    }

    /**
     * @param $slug
     * @return Event|Exception
     */
    public function findBySlug($slug)
    {
        return $this->event->with(['category', 'tags', 'articles'])->where('slug', $slug)->firstOrFail();
    }

}