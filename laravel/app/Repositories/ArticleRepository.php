<?php

namespace App\Repositories;

use App\Interfaces\ArticleRepositoryInterface;
use App\Models\Article;
use App\Models\Category;
use Exception;
use Illuminate\Support\Collection;

class ArticleRepository implements ArticleRepositoryInterface
{
    /**
     * @var Article
     */
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function getAll()
    {
        return $this->article->all();
    }

    public function find($id)
    {
        return $this->article
            ->with(['category', 'tags'])
            ->findOrFail($id);
    }

    /**
     * @param $slug
     * @return Article|Exception
     */
    public function findBySlug($slug)
    {
        return $this->article->published()->with(['category', 'tags', 'events' => function ($query) {
            $query->orderBy('start_at', 'DESC');
        }])->where('slug', $slug)->firstOrFail();
    }

    /**
     * @return Collection
     */
    public function getFeaturedArticles()
    {
        $featured_options = collect(config('dps.articles.featured_options'))->except('regular')->keyBy('key');

        $articles_featured = $this->article
            ->publishedOrRedirectToEvent()
            ->featured()
            ->with(['category', 'tags'])
            ->orderBy('date', 'DESC')
            ->where('featured', '!=', 'regular')
            ->get()
            ->groupBy('featured')
            ->map(function (Collection $featured_chunk, $key) use ($featured_options) {
                return $featured_chunk->slice(0, $featured_options[$key]['limit']);
            });

        return $articles_featured;
    }

    /**
     * @param string $search
     * @param array $params
     * @param float $limit
     * @return \Illuminate\Database\Eloquent\Collection
     * @internal param \Closure|null $callback
     */
    public function get($search = '', $params = [], $limit = 1e4)
    {
        $query = $this->article->elasticSearch($search, $params);
        $query->limit = $limit;

        return $query->get();
    }

    /**
     * @param Category $category
     * @param int $limit
     * @return Collection
     */
    public function latestInCategory(Category $category, $limit = 5)
    {
        return $this->article
            ->published()
            ->where('category_id', $category->id)
            ->orderByDesc('date')
            ->limit($limit)
            ->get();
    }
}