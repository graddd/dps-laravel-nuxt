<?php

namespace App\Repositories;

use App\Interfaces\MenuItemRepositoryInterface;
use App\Models\MenuItem;

class MenuItemRepository implements MenuItemRepositoryInterface
{
    /**
     * @var MenuItem
     */
    protected $menuItem;

    public function __construct(MenuItem $menuItem)
    {
        $this->menuItem = $menuItem;
    }

    public function getAll()
    {
        return $this->menuItem->all();
    }

    public function find($id)
    {
        return $this->menuItem->findOrFail($id);
    }

    /**
     * Creates menu item hierarchy
     * @param bool $navigation
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function hierarchy($navigation = false)
    {
        $with = ['children' => function ($query) {
            $query->orderBy('lft', 'ASC');
        }];
        if($navigation) {
            array_push($with, 'article:id,slug', 'children.article:id,slug');
        } else {
            array_push($with, 'article', 'children.article');
        }
        return $this->menuItem
            ->with($with)
            ->where('parent_id', '=', null)
            ->orderBy('lft', 'ASC')
            ->get();
    }
}