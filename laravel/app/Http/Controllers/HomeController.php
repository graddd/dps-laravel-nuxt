<?php

namespace App\Http\Controllers;

use App\Interfaces\ArticleRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Services\FractalService;

class HomeController extends Controller
{
    /**
     * @var ArticleRepository
     */
    private $article;

    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(ArticleRepositoryInterface $article, CategoryRepositoryInterface $category)
    {
        $this->article = $article;
        $this->category = $category;
    }

    public function index()
    {
        // initial state must be given in form ['mutation' => $data] just as in vuex
        $commits = [
            'routed/home/update_articles_featured' => FractalService::transformFeaturedArticles($this->article->getFeaturedArticles()),
            'routed/home/update_articles_categorized' => FractalService::transformFeaturedCategories($this->category->getFeaturedCategories())
        ];
        return app('spa')->respond($commits);
    }
}
