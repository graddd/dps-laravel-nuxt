<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Repositories\EventRepository;
use App\Repositories\InitiativeRepository;
use App\Services\ArticleService;
use App\Services\EventService;
use App\Services\FractalService;
use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $article;
    private $initiative;
    private $event;

    public function __construct(ArticleRepository $article, InitiativeRepository $initiative, EventRepository $event)
    {
        $this->article = $article;
        $this->initiative = $initiative;
        $this->event = $event;
    }

    function search(Request $request) {
        $search = $request->input('search');
        $params = $request->input('params');
        /**
         * Articles
         */
        $article_options = ArticleService::getOptions();
        $article_params = ArticleService::createSearchParams($params, 'relevance-desc');
        $articles = $this->article->get($search, SearchService::spread($article_params));
        /**
         * Events
         */
        $event_options = EventService::getOptions();
        $event_params = EventService::createSearchParams($params, 'relevance-desc');
        $events = $this->event->get($search, SearchService::spread($event_params));
        /**
         * Initiatives
         */
        // $initiatives = $this->initiative->get($search, $params);


        app('spa')->meta([
            'title' => 'Iskanje',
            'description' => 'Iskanje po vseh člankih, dogodkih in organizacijah.',
        ]);

        app('spa')->crumb([
            ['name' => 'Iskanje', 'to' => ['name' => 'search']]
        ]);

        return app('spa')->respond([
            'search/UPDATE_QUERY' => $search,

            'search/UPDATE_ARTICLE_LIST' => FractalService::transformCards($articles),
            'search/UPDATE_ARTICLE_PARAMS' => $article_params,
            'search/UPDATE_ARTICLE_OPTIONS' => $article_options,

            'search/UPDATE_EVENT_LIST' => FractalService::transformCards($events),
            'search/UPDATE_EVENT_PARAMS' => $event_params,
            'search/UPDATE_EVENT_OPTIONS' => $event_options,
        ]);

    }
}
