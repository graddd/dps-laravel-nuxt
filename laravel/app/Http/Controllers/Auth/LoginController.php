<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\UserService;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Socialite;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var UserService
     */
    private $userService;

    /**
     * Create a new controller instance.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest', ['except' => 'logout']);

        $this->userService = $userService;
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param $provider
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.
     *
     * @param $provider
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if (!in_array($provider, ['facebook', 'google'])) {
            throw new AuthorizationException('Social authentication only possible with facebook or google.');
        }
        /** @var \Laravel\Socialite\Two\User $socialUser */
        $socialUser = Socialite::driver($provider)->user();
        /** @var \App\Models\User $user */
        $user = User::whereEmail($socialUser->getEmail())->first();
        if ($user) {
            $user->update(['name' => $socialUser->getName()]);
            $profile = $user->profile;
            if (!$profile) {
                $user->profile()->create([
                    'photo_url' => $socialUser->getAvatar(),
                    $provider => $socialUser->getId()
                ])->save();
            }
        } else {
            $user = User::create([
                'name' => $socialUser->getName(),
                'email' => $socialUser->getEmail(),
                'api_token' => str_random(60),
                'password' => bcrypt(str_random(16))
            ]);
            /** @var \App\Models\User\Profile $userProfile */
            $userProfile = $user->profile()->create([
                'photo_url' => $socialUser->getAvatar(),
                $provider => $socialUser->getId()
            ]);
            $userProfile->save();
        }

        Auth::login($user);
        return redirect()->to('/');
    }

    public function facebook(LaravelFacebookSdk $facebook)
    {
        $token = $this->userService->manageFacebookToken($facebook);

        $facebook->setDefaultAccessToken($token);

        // in case it is neeeded
        \Session::put('fb_user_access_token', (string) $token);

        $response = null;
        try {
            $response = $facebook->get('/me?fields=id,name,email,picture');
        } catch (FacebookSDKException $e) {
            dd($e->getMessage());
        }

        /** @var \Facebook\GraphNodes\GraphUser $facebook_user */
        $facebook_user = $response->getGraphUser();

        $user = $this->userService->manageFacebookUser($facebook_user);

        $access_token = $user->createToken('access_token', ['*'])->accessToken;

        return response()->json(compact('access_token'));

    }
}