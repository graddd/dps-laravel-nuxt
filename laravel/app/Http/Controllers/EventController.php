<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Repositories\EventRepository;
use App\Services\EventService;
use App\Services\FractalService;
use App\Services\SearchService;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * @var EventRepository
     */
    private $event;
    private $article;

    public function __construct(EventRepository $event, ArticleRepository $article)
    {
        $this->event = $event;
        $this->article = $article;
    }

    function index(Request $request) {
        $search = $request->input('search');
        $params = $request->input('params');

        $event_options = EventService::getOptions();
        $event_params = EventService::createSearchParams($params, 'relevance-desc');
        $events = $this->event->get($search, SearchService::spread($event_params));

        app('spa')->meta([
            'title' => 'Dogodki',
            'description' => 'Iskanje po dogodkih Društva za permakulturo slovenije -- izbiraj med kategorijami ter oznakami.',
        ]);

        app('spa')->crumb([
            ['name' => 'Dogodki', 'to' => ['name' => 'events']]
        ]);

        return app('spa')->respond([
            'search/UPDATE_QUERY' => $search,

            'search/UPDATE_EVENT_LIST' => FractalService::transformCards($events),
            'search/UPDATE_EVENT_PARAMS' => $event_params,
            'search/UPDATE_EVENT_OPTIONS' => $event_options,
        ]);
    }

    public function find($slug)
    {
        $event = $this->event->findBySlug($slug);
        $latestInCategory = $this->article->latestInCategory($event->category, 5);

        app('spa')->meta([
            'title' => $event->name,
            'description' => $event->content_short,
            'image' => url($event->image),
        ]);

        app('spa')->crumb([
            ['name' => 'Dogodki', 'to' => ['name' => 'events']],
            ['name' => $event->name, 'to' => ['name' => 'event-slug', 'params' => ['slug' => $event->slug]]]
        ]);

        return app('spa')->respond([
            'routed/event/update_event' => FractalService::transformEvent($event),
            'routed/event/update_latest_in_category' => FractalService::transformCards($latestInCategory)
        ]);
    }

    public function calendar()
    {
        $params = [
            'sort' => 'date',
            'order' => 'asc'
        ];
        $events = $this->event->get('', $params);

        app('spa')->meta([
            'title' => 'Koledar dogodkov',
            'description' => 'Koledar preteklih in prihodnjih dogodkov.',
        ]);

        app('spa')->crumb([
            ['name' => 'Dogodki', 'to' => ['name' => 'events']],
            ['name' => 'Koledar', 'to' => ['name' => 'calendar']]
        ]);

        return app('spa')->respond([
            'calendar/update_event_list' => FractalService::transformCards($events),
        ]);
    }
}
