<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Interfaces\ArticleRepositoryInterface;
use App\Services\ArticleService;
use App\Services\FractalService;
use App\Services\SearchService;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;


class ArticleController extends Controller
{
    /**
     * @var ArticleRepository
     */
    private $article;

    public function __construct(ArticleRepositoryInterface $article)
    {
        $this->article = $article;
    }

    function index(Request $request) {
        $search = $request->input('search');
        $params = $request->input('params');

        $article_options = ArticleService::getOptions();
        $article_params = ArticleService::createSearchParams($params, 'date-desc');
        $articles = $this->article->get($search, SearchService::spread($article_params));

        app('spa')->meta([
            'title' => 'Članki',
            'description' => 'Iskanje po arhivu vseh člankov o permakulturi in širše -- izbiraj med kategorijami ter oznakami.',
        ]);

        app('spa')->crumb([
            ['name' => 'Članki', 'to' => ['name' => 'articles']]
        ]);

        return app('spa')->respond([
            'search/UPDATE_QUERY' => $search,

            'search/UPDATE_ARTICLE_LIST' => FractalService::transformCards($articles),
            'search/UPDATE_ARTICLE_PARAMS' => $article_params,
            'search/UPDATE_ARTICLE_OPTIONS' => $article_options,
        ]);
    }

    public function find($slug)
    {
        $article = $this->article->findBySlug($slug);
        $latestInCategory = $this->article->latestInCategory($article->category, 5);

        app('spa')->meta([
            'title' => $article->name,
            'description' => $article->content_short,
            'image' => url($article->image),
            'type' => 'article'
        ]);

        app('spa')->crumb([
            ['name' => 'Članki', 'to' => ['name' => 'articles']],
            ['name' => $article->name, 'to' => ['name' => 'article-slug', 'params' => ['slug' => $article->slug]]]
        ]);

        return app('spa')->respond([
            'routed/article/update_article' => FractalService::transformArticle($article),
            'routed/article/update_latest_in_category' => FractalService::transformCards($latestInCategory),
        ]);
    }
}
