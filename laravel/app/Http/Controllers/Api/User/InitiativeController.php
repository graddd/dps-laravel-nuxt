<?php

namespace App\Http\Controllers\Api\User;

use App\Interfaces\InitiativeRepositoryInterface;
use App\Models\Initiative;
use App\Models\User;
use App\Repositories\InitiativeRepository;
use App\Services\InitiativeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InitiativeController extends Controller
{
    /**
     * @var InitiativeRepository
     */
    private $repository;

    /**
     * @var InitiativeService
     */
    private $service;

    public function __construct(InitiativeRepositoryInterface $repository, InitiativeService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
        /** @var User $user */
        $user = \Auth::guard('api')->user();

        $initiatives = $user->initiatives()
            ->with(['categories', 'audience', 'locations', 'contacts'])
            ->get()
            ->toArray();

        return response()->json(compact('initiatives'));
    }

    public function show($initiative_id)
    {
        $initiative = $this->repository->find($initiative_id);

        /**
         * todo: Policy or request authorization
         * @var User $user
         */
        $user = \Auth::guard('api')->user();

        if(!$initiative->users->contains('id', $user->id)) {
            abort(403);
        }

        return response()->json(['initiative' => $initiative->toArray()]);
    }

    public function store(Request $request)
    {
        $initiative = $this->service->createFromRequest($request);

        return response()->json(['status' => 'ok', 'initiative' => $initiative]);
    }
}
