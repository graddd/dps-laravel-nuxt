<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\InitiativeRepositoryInterface;
use App\Interfaces\LocationRepositoryInterface;
use App\Models\Audience;
use App\Models\Initiative\Category;
use App\Models\Contact;
use App\Models\Initiative;
use App\Models\Initiative\Tag;
use App\Models\Location;
use App\Repositories\InitiativeRepository;
use App\Repositories\LocationRepository;
use App\Services\InitiativeService;
use App\Services\MapService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InitiativeController extends Controller
{
    /**
     * @var InitiativeRepository
     */
    private $repository;

    /**
     * @var InitiativeService
     */
    private $service;
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    public function __construct(InitiativeRepositoryInterface $repository, LocationRepositoryInterface $locationRepository, InitiativeService $service)
    {
        $this->repository = $repository;
        $this->locationRepository = $locationRepository;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $params = $request->only(['categories', 'tags', 'audience']);
        $params['sort'] = $request->input('sort', '_score');
        $params['order'] = $request->input('order', 'desc');
        $search = $request->input('search');

        $initiatives = $this->repository->get($search, $params)->toArray();
//        $markers = $this->locationRepository->mapMarkers($params);

        return response()->json(compact('initiatives'));
    }

    public function show($id)
    {
        $initiative = $this->repository->find($id)->toArray();

        return response()->json(compact('initiative'));
    }

    public function options()
    {
        $options = [
            'categories' => Category::all()->toArray(),
            'tags' => Tag::all()->toArray(),
            'audience' => Audience::all()->toArray(),
            'status' => config('initiatives.status'),
            'audience_size' => config('initiatives.audience_size'),
            'location_type' => config('initiatives.location_type')
        ];

        return response()->json($options);
    }
}
