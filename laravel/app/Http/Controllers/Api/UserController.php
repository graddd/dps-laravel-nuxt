<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function current() {
        $user = \Auth::guard('api')->user();

        return response()->json(compact('user'));
    }

    public function profile() {
        /** @var User $user */
        $user = \Auth::guard('api')->user();
        $profile = $user->profile->toArray();

        return response()->json(compact('profile'));
    }
}
