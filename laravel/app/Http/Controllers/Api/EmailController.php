<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class EmailController extends Controller
{
    function subscribe(Request $request) {
        $client = new Client();
        $sendinblue = app('sendinblue');

        try {
            $clientResponse = $client->request('POST', config('dps.mailerlite.uri'), [
                'headers' => [
                    'X-MailerLite-ApiKey' => config('dps.mailerlite.api_key')
                ],
                'json' => [
                    'email' => $request->input('email')
                ]
            ]);

            # Id of created user : $message = json_decode($clientResponse->getBody()->getContents())->id;
            return response()->json([
                'status' => 'success',
                'message' => $sendinblue->translate('created')
            ], $clientResponse->getStatusCode());
        } catch (RequestException $e){
            if($e->hasResponse()){
                $clientResponse = json_decode($e->getResponse()->getBody());
                $key = $clientResponse->error->message;
            } else {
                $key = $e->getMessage();
            }

            // "Contact already exists" is interpreted as success.
            if($key == 'duplicate_parameter'){
                return response()->json([
                    'status' => 'success',
                    'message' => $sendinblue->translate($key)
                ], 200);
            }

            return response()->json([
                'status' => 'error',
                'message' => $sendinblue->translate($key)
            ], $e->getCode());
        }
    }
}