<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\ArticleRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Services\FractalService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * @var ArticleRepository
     */
    private $article;

    /**
     * @var CategoryRepository
     */
    private $category;

    /**
     * ArticleController constructor.
     * @param ArticleRepositoryInterface $article
     */
    public function __construct(ArticleRepositoryInterface $article)
    {
        $this->article = $article;
    }

    public function options()
    {
        $categories = Category::all()->toArray();
        $tags = Tag::all()->toArray();

        return response()->json(compact('categories', 'tags'));
    }

    public function search(Request $request)
    {
        $params = $request->only(['category', 'tags']);
        $params['sort'] = $request->input('sort', '_score');
        $params['order'] = $request->input('order', 'desc');

        $search = $request->input('search');

        $articles = FractalService::transformCards($this->article->get($search, $params));

        return response()->json(compact('articles'));
    }

//    public function show($slug) {
//        $article = $this->article->findBySlug($slug);
//
//        return response()->json(compact('article'));
//    }
}
