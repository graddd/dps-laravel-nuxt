<?php

namespace App\Http\Controllers\Api;

use App\Models\Event;
use App\Repositories\EventRepository;
use App\Services\FractalService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * @var Event
     */
    private $events;

    public function __construct(EventRepository $events)
    {
        $this->events = $events;
    }

    public function search(Request $request)
    {
        $params = $request->only(['category', 'tags']);
        $params['sort'] = $request->input('sort', '_score');
        $params['order'] = $request->input('order', 'desc');

        $events = FractalService::transformCards($this->events->get($request->input('search'), $params));

        return response()->json(compact('events'));
    }
}
