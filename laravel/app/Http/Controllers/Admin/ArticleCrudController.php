<?php

namespace App\Http\Controllers\Admin;

use Alert;
use App\Models\Article;
use App\Services\ArticleService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Article\StoreRequest;
use App\Http\Requests\Article\UpdateRequest;
use Redirect;

class ArticleCrudController extends CrudController
{
    private $service;

    public function __construct(ArticleService $service)
    {
        $this->service = $service;

        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/article');
        $this->crud->setEntityNameStrings('article', 'articles');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        $featured_opts = collect(config('dps.articles.featured_options'))->pluck('name', 'key')->toArray();

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date',
        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Status',
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'featured',
            'label' => 'Featured',
            'type' => 'radio',
            'options' => $featured_opts
        ]);
        $this->crud->addColumn([
            'label' => 'Category',
            'type' => 'select',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'model' => 'App\Models\Category',
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Name',
            'type' => 'text',
            'placeholder' => 'Your title here',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your title, if left empty.',
             'disabled' => 'disabled'
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date',
            'value' => date('Y-m-d'),
        ], 'create');
        $this->crud->addField([    // TEXT
            'name' => 'date',
            'label' => 'Date',
            'type' => 'date',
        ], 'update');

        $this->crud->addField([
            'type'  => 'division',
            'name'  => 'division_category_tags',
            'label' => 'Category & tags'
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Category',
            'type' => 'select2',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'model' => 'App\Models\Category',
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            // n-n relationship
            'label' => 'Tags',
            'type' => 'select2_multiple',
            'name' => 'tags', // the method that defines the relationship in your Model
            'entity' => 'tags', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Tag', // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?

//            This is a potential ajax-select2-multiple example
//            'label' => "Tags", // Table column heading
//            'type' => "select2_from_ajax_multiple",
//            'name' => 'tags', // the column that contains the ID of that connected entity
//            'entity' => 'tags', // the method that defines the relationship in your Model
//            'attribute' => "name", // foreign key attribute that is shown to user
//            'model' => 'App\Models\Tag', // foreign key model
//            'data_source' => url("admin/ajax/tags"), // url to controller search function (with /{id} should return model)
//            'placeholder' => "Select tags", // placeholder for the select
//            'minimum_input_length' => 0, // minimum characters to type before querying results
//            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([
            'type'  => 'division',
            'name'  => 'division_content',
            'label' => 'Content'
        ]);
        $this->crud->addField([    // WYSIWYG
            'name' => 'content_short',
            'label' => 'Short content',
            'type' => 'textarea',
            'attributes' => [
                'maxlength' => 700
            ],
            'placeholder' => '',
        ]);
        $this->crud->addField([    // WYSIWYG
            'name' => 'content',
            'label' => 'Content',
            'type' => 'ckeditor',
            'placeholder' => '',
        ]);

        $this->crud->addField([
            'type'  => 'division',
            'name'  => 'division_category_images_references',
            'label' => 'Images and references'
        ]);
        $this->crud->addField([ // image
            'label'        => "Main image",
            'name'         => "image",
            'type'         => 'image',
            'upload'       => true,
            'crop'         => true, // set to true to allow cropping, false to disable,
            'placeholder'  => 'http://via.placeholder.com/500x500',
            'prefix'       => config('dps.articles.images.square.path'),
            'aspect_ratio' => config('dps.articles.images.square.width') / config('dps.articles.images.square.height')
        ]);

        $this->crud->addField([ // image
            'label'        => "Cover image (optional)",
            'hint'         => "Used for \"in the spotlight\" mode on home page.",
            'name'         => "image_cover",
            'type'         => 'image',
            'upload'       => true,
            'crop'         => true, // set to true to allow cropping, false to disable,
            'placeholder'  => 'http://via.placeholder.com/1905x450',
            'prefix'       => config('dps.articles.images.cover.path'),
            'aspect_ratio' => config('dps.articles.images.cover.width') / config('dps.articles.images.cover.height')
        ]);

        $this->crud->addField([       // Select2Multiple = n-n relationship (with pivot table)
            // n-n relationship
            'label' => 'References',
            'type' => 'select2_multiple',
            'name' => 'references', // the method that defines the relationship in your Model
            'entity' => 'references', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Reference', // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([
            'type'  => 'division',
            'name'  => 'division_events',
            'label' => 'Events'
        ]);
        $this->crud->addField([// Select2Multiple = n-n relationship (with pivot table)
            'label' => 'Events (optional)',
            'hint' => 'Events will be shown on article details -- fill if article is announcement/report of an event etc.',
            'type' => 'select2_multiple',
            'name' => 'events', // the method that defines the relationship in your Model
            'entity' => 'events', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => 'App\Models\Event', // foreign key model
            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        $this->crud->addField([
            'type'  => 'division',
            'name'  => 'division_category_status',
            'label' => 'Status'
        ]);
        $this->crud->addField([    // ENUM
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum',
            'required' => true
        ]);
        $this->crud->addField([    // CHECKBOX
            'name' => 'featured',
            'label' => 'Featured item',
            'type' => 'radio',
            'default' => 'regular',
            'options' => $featured_opts
        ]);

        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        $this->service->crudCreate($request->except(['redirect_after_save', 'password']));
        Alert::success(trans('backpack::crud.insert_success'))->flash();
        return Redirect::to($this->crud->route);
    }

    public function update(UpdateRequest $request, Article $article)
    {
        $this->service->crudUpdate($article, $request->except('redirect_after_save', 'password'));
        Alert::success(trans('backpack::crud.update_success'))->flash();
        return Redirect::to($this->crud->route);
    }
}
