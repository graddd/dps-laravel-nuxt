<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as Request;

class ReferenceCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Reference');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/reference');
        $this->crud->setEntityNameStrings('reference', 'references');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        $types = collect(config('dps.references.types'))->pluck('name', 'key')->toArray();


        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'type' => 'radio',
            'label' => 'Type',
            'options' => $types
        ]);
        $this->crud->addColumn([
            'name' => 'link',
            'label' => 'Link',
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addField([
            'name' => 'type',
            'label' => 'File',
            'type' => 'upload_or_link',
            'upload' => true
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => 'Description (optional)',
            'type' => 'textarea',
            'attributes' => [
                'maxlength' => 200
            ],
            'placeholder' => '',
        ]);
    }

    public function store(Request $request)
    {
        return parent::storeCrud();
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
