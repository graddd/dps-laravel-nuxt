<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Models\Reference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferenceController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Reference::where('name', 'LIKE', '%'.$search_term.'%')->paginate(5);
        }
        else
        {
            $results = Reference::paginate(5);
        }

        return $results;
    }

    public function show($id)
    {
        return Reference::find($id);
    }
}
