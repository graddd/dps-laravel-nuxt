<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Models\Article;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Tag::where('name', 'LIKE', '%'.$search_term.'%')->paginate(5);
        }
        else
        {
            $results = Tag::paginate(5);
        }

        return $results;
    }

    public function show($id)
    {
        return Tag::find($id);
    }
}
