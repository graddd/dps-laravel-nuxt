<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\NewsCRUD\app\Http\Requests\CategoryRequest as StoreRequest;
use Backpack\NewsCRUD\app\Http\Requests\CategoryRequest as UpdateRequest;

class CategoryCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel(Category::class);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/category');
        $this->crud->setEntityNameStrings('category', 'categories');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */
        $types = collect(config('dps.categories.types'))->pluck('name', 'key')->toArray();


        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 2);

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'slug',
            'label' => 'Slug',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'label' => 'Type',
            'type' => 'radio',
            'options' => $types
        ]);
//        $this->crud->addColumn([
//            'label' => 'Parent',
//            'type' => 'select',
//            'name' => 'parent_id',
//            'entity' => 'parent',
//            'attribute' => 'name',
//            'model' => "Backpack\NewsCRUD\app\Models\Category",
//        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Name',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your name, if left empty.',
            // 'disabled' => 'disabled'
        ]);
//        $this->crud->addField([
//            'label' => 'Parent',
//            'type' => 'select',
//            'name' => 'parent_id',
//            'entity' => 'parent',
//            'attribute' => 'name',
//            'model' => "Backpack\NewsCRUD\app\Models\Category",
//        ]);

        $this->crud->addField([    // CHECKBOX
            'name' => 'type',
            'label' => 'Type',
            'type' => 'radio',
            'default' => 'regular',
            'options' => $types
        ]);
        $this->crud->addField([    // CHECKBOX
            'name' => 'is_project',
            'label' => 'This category is a project',
            'type' => 'checkbox'
        ]);

        $this->crud->addField([
            'name' => 'color',
            'label' => 'Color',
            'type' => 'color_picker',
            'hint' => 'Shown in calendar days. Examples of recommended colors: #6aab88, #a05956, #4f8394, #aaaaaa. Choose wisely, go for pastel colors.'
        ]);

    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
