<?php

namespace App\Http\Middleware\Api;

use Closure;

class RequestedWith
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if($request->server('HTTP_X_REQUESTED_WITH') !== 'XMLHttpRequest') {
//            abort(404);
//        }

        return $next($request);
    }
}
