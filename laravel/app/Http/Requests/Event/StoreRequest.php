<?php

namespace App\Http\Requests\Event;

use App\Http\Requests\Request;
use App\Models\Event;
use Gate;
use Illuminate\Contracts\Validation\Validator;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create', Event::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'slug' => 'unique:events,slug,'.\Request::get('id'),
            'content' => 'required|min:20',
            'content_short' => 'required|min:20',
            'category_id' => 'required',
            'start_at'    => 'required|date',
            'end_at'      => 'required|date|after:start_at',
            'locations.lat' => 'required_with_all:locations.geocomplete_value,locations.name',
            'locations.lng' => 'required_with_all:locations.geocomplete_value,locations.name',
            'locations.formatted_address' => 'required_with_all:locations.geocomplete_value,locations.name',
            'locations.geocomplete_value'   => 'required_with_all:locations.lat,locations.lng,locations.name,locations.formatted_address',
            'locations.name'   => 'required_with_all:locations.lat,locations.lng,locations.geocomplete_value,locations.formatted_address',
            'initiatives' => 'array',
        ];
    }
}
