<?php

namespace App\Http\Requests\Article;

use App\Http\Requests\Request;
use App\Models\Article;
use Gate;
use Illuminate\Validation\Rule;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create', Article::class);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'slug' => 'unique:articles,slug,'.\Request::get('id'),
            'content' => 'required|min:20',
            'content_short' => 'required|min:20',
            'date' => 'required|date',
            'status' => 'required',
            'category_id' => 'required'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->sometimes('events', 'required|size:1', function($data){
            return $data->status == 'REDIRECT TO EVENT';
        });
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'events.size' => 'V primeru, da izberete kot status članka "REDIRECT TO EVENT", mora biti prisoten točno en dogodek.'
        ];
    }
}
