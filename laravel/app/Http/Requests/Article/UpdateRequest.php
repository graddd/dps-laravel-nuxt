<?php

namespace App\Http\Requests\Article;

use App\Http\Requests\Request;
use App\Models\Article;
use Gate;

class UpdateRequest extends StoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update', Article::class);
    }
}
