<?php

return [
    'articles' => [
//        'types' => [
//            [
//                'key' => 'news',
//                'name' => 'News'
//            ],
//            [
//                'key' => 'page',
//                'name' => 'Page'
//            ]
//        ],
        'featured_options' => [
            [
                'key' => 'regular',
                'name' => 'Regular (not featured)',
                'limit' => null
            ],
            [
                'key' => 'spotlight',
                'name' => 'In the spotlight',
                'limit' => null
            ],
            [
                'key' => 'top_three',
                'name' => 'Amongst top three articles',
                'limit' => 3
            ],
            [
                'key' => 'presentation',
                'name' => 'Homepage presentation',
                'limit' => 3
            ]
        ],
        'images' => [
            'square' => ['width' => 350, 'height' => 350, 'path' => 'uploads/articles/squares/', 'quality' => 70],
            'cover' => ['width' => 1905, 'height' => 450, 'path' => 'uploads/articles/covers/', 'quality' => 70]
        ],
        'sort_order_options' => [
            'relevance-desc' => [
                'label' => 'Najbolj relevantni najprej',
                'sort' => '_score',
                'order' => 'desc'
            ],
            'date-desc' => [
                'label' => 'Najnovejši najprej',
                'sort' => 'date',
                'order' => 'desc'
            ],
            'date-asc' => [
                'label' => 'Najstarejši najprej',
                'sort' => 'date',
                'order' => 'asc'
            ]
        ]
    ],
    'categories' => [
        'types' => [
            [
                'key' => 'regular',
                'name' => 'Regular category (not shown on home page)',
                'limit' => null
            ],
            [
                'key' => 'featured',
                'name' => 'Featured category (shown on home page)',
                'limit' => 6
            ]
        ]
    ],
    'events' => [
        'images' => [
            'square' => ['width' => 350, 'height' => 350, 'path' => 'uploads/events/squares/', 'quality' => 70],
            'cover' => ['width' => 1905, 'height' => 450, 'path' => 'uploads/events/covers/', 'quality' => 70]
        ],
        'sort_order_options' => [
            'relevance-desc' => [
                'label' => 'Najbolj relevantni najprej',
                'sort' => '_score',
                'order' => 'desc'
            ],
            'date-desc' => [
                'label' => 'Prihodnji najprej',
                'sort' => 'date',
                'order' => 'desc'
            ],
            'date-asc' => [
                'label' => 'Pretekli najprej',
                'sort' => 'date',
                'order' => 'asc'
            ]
        ]
    ],
    'references' => [
        'types' => [
            [
                'key' => 'attachment',
                'name' => 'Attachment'
            ],
            [
                'key' => 'external_link',
                'name' => 'External link'
            ],
            [
                'key' => 'internal_link',
                'name' => 'Internal link'
            ]
        ]
    ],
    'uploads_disk' => 'public',
    'mailerlite' => [
        'uri' => 'https://api.mailerlite.com/api/v2/groups/106869049/subscribers',
        'api_key' => env('MAILERLITE_API_KEY', null)
    ]
];
