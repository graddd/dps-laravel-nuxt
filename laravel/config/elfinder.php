<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public)
    |
    */
    'dir' => ['images'],

    /*
    |--------------------------------------------------------------------------
    | Filesystem disks (Flysytem)
    |--------------------------------------------------------------------------
    |
    | Define an array of Filesystem disks, which use Flysystem.
    | You can set extra options, example:
    |
    | 'my-disk' => [
    |        'URL' => url('to/disk'),
    |        'alias' => 'Local storage',
    |    ]
    */
    'disks' => [
        'local'
    ],

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */

    'route' => [
        'prefix' => 'admin/elfinder'
        //'middleware' => 'replace-t    his-with-your-middleware', //Set to null to disable middleware filter
    ],

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

    'roots' => array(
        array(
            'driver' => 'LocalFileSystem',
            'path' => 'files/',
            'accessControl' => 'Serverfireteam\Panel\libs\AppHelper::access',
            'URL' => config('app.url') . '/files',
            'uploadAllow' => array('image/png', 'image/jpeg', 'image/pjpeg', 'image/gif'),
            'uploadDeny' => array('all'),
            'uploadOrder' => array('deny', 'allow'),
            'acceptedName' => 'Serverfireteam\Panel\libs\AppHelper::validName'
        )
    ),
//    'roots' => [
//        [
//            'driver'        => 'GalleryCrudLocalFileSystem',         // driver for accessing file system (REQUIRED)
//            'path'          => 'galleries',                 // path to files (REQUIRED)
//            'URL'           => '/galleries', // URL to files (REQUIRED)
//            'accessControl' => 'Barryvdh\Elfinder\Elfinder::checkAccess',
//            'autoload'      => true,
//            'tmbPath'       => 'thumbnails',
//            'tmbSize'       => 150,
//            'tmbCrop'       => false,
//            'tmbBgColor'    => '#000',
//        ],
//    ],


    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => array()
);
