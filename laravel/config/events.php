<?php

return [
    'input' => [
        'name'        => 'required',
        'start_at'    => 'required|date',
        'end_at'      => 'required|date|after:start_at',
        'content'     => 'required|min:20',
        'locations.lat' => 'required_with_all:locations.geocomplete_value,locations.name',
        'locations.lng' => 'required_with_all:locations.geocomplete_value,locations.name',
        'locations.formatted_address' => 'required_with_all:locations.geocomplete_value,locations.name',
        'locations.geocomplete_value'   => 'required_with_all:locations.lat,locations.lng,locations.name,locations.formatted_address',
        'locations.name'   => 'required_with_all:locations.lat,locations.lng,locations.geocomplete_value,locations.formatted_address',
        'initiatives' => 'array',
    ]
];