const state = {
    // get
    scoped: {
        query: '',
        params: {
            article: {
                filters: {
                    category: '',
                    tags: []
                },
                sort_order: {}
            }
        }
    },
    article_opts_visible: false,

    // find
    article: {},
    latestInCategory: [],
};

const getters = {
    // get
    articleOptsVisible: state => state.article_opts_visible,
    // find
    article: state => state.article,
    latestInCategory: state => state.latestInCategory
};

const mutations = {
    //  get
    UPDATE_SCOPED_QUERY(state, value) {
        state.scoped.query = value
    },
    UPDATE_SCOPED_ARTICLE_PARAMS(state, value) {
        state.scoped.params.article = value
    },
    TOGGLE_ARTICLE_OPTS(state) {
        state.article_opts_visible = !state.article_opts_visible
    },
    // find
    update_article(state, article) {
        state.article = article
    },
    update_latest_in_category(state, latestInCategory) {
        state.latestInCategory = latestInCategory
    },
    empty_article(state) {
        state.article = {}
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
}