const state = {
    // get
    scoped: {
        query: '',
        params: {
            event: {
                filters: {
                    category: '',
                    tags: []
                },
                sort_order: {}
            }
        }
    },
    event_opts_visible: false,

    // find
    event: {},
    latestInCategory: []
};

const getters = {
    // get
    eventOptsVisible: state => state.event_opts_visible,
    // find
    event: state => state.event,
    latestInCategory: state => state.latestInCategory
};

const mutations = {
    //  get
    UPDATE_SCOPED_QUERY(state, value) {
        state.scoped.query = value
    },
    UPDATE_SCOPED_ARTICLE_PARAMS(state, value) {
        state.scoped.params.article = value
    },
    TOGGLE_EVENT_OPTS(state) {
        state.event_opts_visible = !state.event_opts_visible
    },
    // find
    update_event(state, event) {
        state.event = event
    },
    update_latest_in_category(state, latestInCategory) {
        state.latestInCategory = latestInCategory
    },
    empty_event(state) {
        state.event = {}
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
}