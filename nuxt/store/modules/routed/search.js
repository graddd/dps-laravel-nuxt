const state = {
    scoped: {
        query: '',
        params: {
            article: {
                filters: {
                    category: '',
                    tags: []
                },
                sort_order: {}
            },
            event: {
                filters: {
                    category: '',
                    tags: []
                },
                sort_order: {}
            }
        }
    },
    article_opts_visible: false,
    event_opts_visible: false
}

const getters = {
    articleOptsVisible: state => state.article_opts_visible,
    eventOptsVisible: state => state.event_opts_visible,
}

const mutations = {
    UPDATE_SCOPED_QUERY(state, value) {
        state.scoped.query = value
    },
    UPDATE_SCOPED_ARTICLE_PARAMS(state, value) {
        state.scoped.params.article = value
    },
    UPDATE_SCOPED_EVENT_PARAMS(state, value) {
        state.scoped.params.event = value
    },
    EMPTY_SCOPED_PARAMS (state) {
        state.scoped.params.article = {}
        state.scoped.params.event = {}
    },
    TOGGLE_ARTICLE_OPTS(state) {
        state.article_opts_visible = !state.article_opts_visible
    },
    TOGGLE_EVENT_OPTS(state) {
        state.event_opts_visible = !state.event_opts_visible
    },
    RESET_PARAMS(state) {
        mutations.EMPTY_SCOPED_PARAMS(state)
        state.article_opts_visible = state.event_opts_visible = false;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations
}