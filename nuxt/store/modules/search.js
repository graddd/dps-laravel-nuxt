import _ from 'lodash'
import Api from '../../api'

const state = {
    query: '',
    resultsPerPage: 6,
    article: {
        options: {},
        list: [],
        results: [],
        params: {
            filters: {
                category: '',
                tags: []
            },
            sort_order: {}
        }
    },
    event: {
        options: {},
        list: [],
        results: [],
        params: {
            filters: {
                category: '',
                tags: []
            },
            sort_order: {}
        }
    },
    initiative: {
        toggle: false,
        filters: {
            categories: [],
            audience: [],
            tags: []
        },
        list: [],
        results: [],
        sort: '_score',
        order: 'desc'
    },

}

const getters = {
    indexQuery: state => state.query,
    // article
    indexArticleOptions: state => state.article.options,
    indexArticleResults: state => state.article.results,
    indexArticleList: state => state.article.list,
    indexArticleParams: state => state.article.params,
    // event
    indexEventOptions: state => state.event.options,
    indexEventResults: state => state.event.results,
    indexEventList: state => state.event.list,
    indexEventParams: state => state.event.params,
    // initiative
    indexInitiativeResults: state => state.initiative.results,
    indexInitiativeList: state => state.initiative.list,
    indexInitiativeToggle: state => state.initiative.toggle,

}

const actions = {
    searchAll({commit, state}) {
        // actions.searchInitiatives({commit, state})
        actions.searchArticles({commit, state})
        actions.searchEvents({commit, state})
    },
    searchInitiatives({commit, state}) {
        let request = {
            search: state.query,
            ...state.initiative.filters,
            sort: state.initiative.sort,
            order: state.initiative.order
        }
        Api.initiative.get(request).then(response => {
            const initiatives = response.data.initiatives
            commit('UPDATE_INITIATIVE_LIST', initiatives)
        })
    },
    searchArticles({commit, state}) {
        let request = {
            search: state.query,
            ...state.article.params.filters,
            ...state.article.params.sort_order,
        }
        Api.article.search(request).then(response => {
            const articles = response.data.articles
            commit('UPDATE_ARTICLE_LIST', articles)
        })
    },
    searchEvents({commit, state}) {
        let request = {
            search: state.query,
            ...state.event.params.filters,
            ...state.event.params.sort_order,
        }
        Api.event.search(request).then(response => {
            const events = response.data.events
            commit('UPDATE_EVENT_LIST', events)
        })
    },
    // initiative
    updateInitiativeFilter(context, {field, value}) {
        context.commit('UPDATE_INITIATIVE_FILTER', {field, value})
        actions.searchInitiatives(context)
    },
    updateInitiativeSort(context, value) {
        context.commit('UPDATE_INITIATIVE_SORT', value)
        actions.searchInitiatives(context)
    },
    updateInitiativeOrder(context, value) {
        context.commit('UPDATE_INITIATIVE_ORDER', value)
        actions.searchInitiatives(context)
    },
    // article
    updateArticleFilter(context, {field, value}) {
        context.commit('UPDATE_ARTICLE_FILTER', {field, value})
        actions.searchArticles(context)
    },
    updateArticleSortOrder(context, value){
        context.commit('UPDATE_ARTICLE_SORT_ORDER', value)
        actions.searchArticles(context)
    },
    // event
    updateEventFilter(context, {field, value}) {
        context.commit('UPDATE_EVENT_FILTER', {field, value})
        actions.searchEvents(context)
    },
    updateEventSortOrder(context, value){
        context.commit('UPDATE_EVENT_SORT_ORDER', value)
        actions.searchEvents(context)
    },
}

const mutations = {
    UPDATE_QUERY(state, query) {
        state.query = query
    },

    // article
    UPDATE_ARTICLE_OPTIONS(state, options) {
        options.categories = _.map(options.categories, function (category) {
            return category.name
        });
        options.tags = _.map(options.tags, function (t) {
            return t.name
        });
        state.article.options = options
    },
    UPDATE_ARTICLE_LIST(state, articles) {
        state.article.list = articles
    },
    UPDATE_ARTICLE_RESULTS(state, results) {
        state.article.results = results
    },
    UPDATE_ARTICLE_FILTER(state, {field, value}) {
        state.article.params.filters[field] = value
    },
    UPDATE_ARTICLE_SORT_ORDER(state, value) {
        state.article.params.sort_order = value
    },
    UPDATE_ARTICLE_PARAMS(state, value) {
        state.article.params = value
    },
    // event
    UPDATE_EVENT_OPTIONS(state, options) {
        options.categories = _.map(options.categories, function (category) {
            return category.name
        });
        options.tags = _.map(options.tags, function (t) {
            return t.name
        });
        state.event.options = options
    },
    UPDATE_EVENT_LIST(state, events) {
        state.event.list = events
    },
    UPDATE_EVENT_RESULTS(state, results) {
        state.event.results = results
    },
    UPDATE_EVENT_FILTER(state, {field, value}) {
        state.event.params.filters[field] = value
    },
    UPDATE_EVENT_SORT_ORDER(state, value) {
        state.event.params.sort_order = value
    },
    UPDATE_EVENT_PARAMS(state, value) {
        state.event.params = value
    },
    // initiative
    UPDATE_INITIATIVE_TOGGLE(state) {
        state.initiative.toggle = !state.initiative.toggle
    },
    UPDATE_INITIATIVE_LIST(state, initiatives) {
        state.initiative.list = initiatives
    },
    UPDATE_INITIATIVE_RESULTS(state, results) {
        state.initiative.results = results
    },
    UPDATE_INITIATIVE_FILTER(state, {field, value}) {
        state.initiative.filters[field] = value
    },
    UPDATE_INITIATIVE_SORT(state, value) {
        state.initiative.sort = value
    },
    UPDATE_INITIATIVE_ORDER(state, value) {
        state.initiative.order = value
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}