/**
 * This is a wrapper for "route modules"; they contain only actions, needed for route change fetching.
 * This is done for the sake of semantic folder structure.
 */
import home from './routed/home';
import article from  './routed/article'
import event from  './routed/event'
import search from './routed/search';

export default {
    namespaced: true,
    modules: {
        home,
        article,
        event,
        search
    }
}