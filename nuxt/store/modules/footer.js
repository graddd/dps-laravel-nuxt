// initial state
const state = {
    information: []
};

// getters
const getters = {
    information: state => state.information
};

// actions
const actions = {
};

// mutations
const mutations = {
    update_information (state, information) {
        state.information = information
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}