import Api from '../../api'
import _ from 'lodash'
// import form from './initiative/form'
import micelij from './initiative/micelij'

// initial state
const state = {
    initiatives: [],
    initiative: {},
    filters: {
        toggle: false,
        selected: {
            categories: [],
            audience: []
        }
    },
    search: '',
    timeout: null,
    options: {},
}

// getters
const getters = {
    indexInitiatives: state => state.initiatives,
    show: state => state.initiative,
    indexFilters: state => state.filters,
    indexSearch: state => state.search,
    indexSelectedFilters: state => state.filters.selected,
    indexOptions: state => state.options
}

// actions
const actions = {
    refresh({commit}) {
        let audience = localStorage.getItem('audience')
        let categories = localStorage.getItem('categories')
        let search = localStorage.getItem('search')

        if (audience) commit('updateSelectedAudience', JSON.parse(audience))
        if (categories) commit('updateSelectedCategories', JSON.parse(categories))
        if (search) commit('updateSearch', search)
        actions.getInitiatives({commit, state})
    },
    clearCriteria({commit, state}) {
        commit('updateSelectedAudience', [])
        commit('updateSelectedCategories', [])
        commit('updateSearch', '')
        actions.getInitiatives({commit, state})
    },
    getInitiatives ({commit, state}) {
        let categories = []
        _.each(state.filters.selected.categories, function (category) {
            categories.push(category.name)
        })

        let audience = []
        _.each(state.filters.selected.audience, function (a) {
            audience.push(a.name)
        })


        let request = {
            search: state.search,
            categories,
            audience
        };

        Api.initiative.get(request).then(response => {
            let initiatives = response.data.initiatives,
                markers = response.data.markers;
            commit('updateInitiatives', {initiatives})
            commit('initiative/map/updateMarkers', markers, {root: true})
        })
    },
    find({commit, rootState}) {
        const id = rootState.route.params.id
        Api.initiative.find({id}).then(response => {
            let initiative = response.data.initiative
            commit('updateInitiative', {initiative})
        });
    },
    clickSidebarSearch({commit, state}, query) {
        actions.getInitiatives({commit, state});
    },
    // getFilters({commit, state}) {
    //     Api.initiative.getCategories().then(response => {
    //         let categories = response.data.categories
    //         commit('updateCategories', {categories})
    //     })
    //
    //     Api.initiative.getAudience().then(response => {
    //         let audience = response.data.audience
    //         commit('updateAudience', {audience})
    //     })
    // },
    getOptions({commit, state}) {
        Api.initiative.getOptions().then(response => {
            let options = response.data
            commit('updateOptions', {options})
        })
    }

}

// mutations
const mutations = {
    updateInitiatives (state, {initiatives}) {
        state.initiatives = initiatives
    },
    updateInitiative (state, {initiative}) {
        state.initiative = initiative
    },
    updateSearch (state, query) {
        state.search = query
        localStorage.setItem('search', query)
    },
    toggleFilters (state) {
        state.filters.toggle = !state.filters.toggle
    },
    // ['UPDATE_MARKERS'] (state, {initiatives}) {
    //     let markers = []
    //     _.each(initiatives, function (initiative) {
    //         _.each(initiative.locations, function (location) {
    //             markers.push({position: {lat: location.lat, lng: location.lng}, initiatives: location.initiatives})
    //         })
    //     })
    //     state.markers = markers;
    // },
    updateSelectedCategories (state, selected) {
        state.filters.selected.categories = selected
        localStorage.setItem('categories', JSON.stringify(selected))
    },
    updateSelectedAudience (state, selected) {
        state.filters.selected.audience = selected
        localStorage.setItem('audience', JSON.stringify(selected))
    },
    // updateCategories(state, {categories}) {
    //     _.each(categories, function (category) {
    //         category.label = category.name
    //         category.value = category.id
    //     })
    //
    //     state.filters.categories = categories
    // },
    // updateAudience(state, {audience}) {
    //     _.each(audience, function (a) {
    //         a.label = a.name
    //         a.value = a.id
    //     })
    //
    //     state.filters.audience = audience
    // },
    updateOptions(state, {options}) {
        _.each(options.categories, function (category) {
            category.label = category.name
            category.value = category.id
        })

        _.each(options.audience, function (a) {
            a.label = a.name
            a.value = a.id
        })

        _.each(options.tags, function (t) {
            t.label = t.name
            t.value = t.id
        })

        state.options = options
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    modules: {
        // form,
        micelij
    }
}