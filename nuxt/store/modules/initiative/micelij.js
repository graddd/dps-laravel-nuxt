import InitiativeMapService from "../../../services/initiative-map-service";

const state = {
    center: {
        lat: 46.198654,
        lng: 14.806461
    },
    zoom: 8,
    mapTypeId: 'terrain',
    options: {
        map: {
            scrollwheel: true,
            minZoom: 7,
            disableDefaultUi: true,
            mapTypeControl: false,
            zoomControl: false
        },
        marker: {
            icon: {
                path: 'M27.648-41.399q0-3.816-2.7-6.516t-6.516-2.7-6.516 2.7-2.7 6.516 2.7 6.516 6.516 2.7 6.516-2.7 2.7-6.516zm9.216 0q0 3.924-1.188 6.444l-13.104 27.864q-.576 1.188-1.71 1.872t-2.43.684-2.43-.684-1.674-1.872l-13.14-27.864q-1.188-2.52-1.188-6.444 0-7.632 5.4-13.032t13.032-5.4 13.032 5.4 5.4 13.032z',
                scale: 0.7,
                strokeWeight: 1,
                strokeColor: '#fff',
                strokeOpacity: 0.7,
                fillColor: '#444',
                fillOpacity: 1
            }
        },
        infoWindow: {
            disableAutoPan: true
        }
    },
    bounds: {},
    markers: [],
    focusedMarkers: [],
    focusedInitiatives: [],
    // topInitiatives: [],
}

// getters
const getters = {
    index: state => state,
    indexMarkers: state => state.markers,
    focusedMarkers: state => state.focusedMarkers,
    focusedInitiatives: state => state.focusedInitiatives
}

// actions
const actions = {
    updateBounds({commit}, bounds){
        commit('updateBounds', bounds)
    },
    updateCenter({commit}, center){
        commit('updateCenter', center)
    },
    adjustCenter({commit, state}, position) {
        let sidebarLngWidth = InitiativeMapService.sidebarWidthDeg(state.bounds),
            apparentCenter = {
                lat: position.lat,
                lng: position.lng - sidebarLngWidth / 2
            };

        commit('updateCenter', apparentCenter)
    },
    focusInitiative({commit, state}, initiative) {
        let correspondingMarkers = InitiativeMapService.findCorrespondingEntities(
                state.markers,
                initiative.locations
            );

        if(_.size(correspondingMarkers) > 1){
            let bounds = InitiativeMapService.multipleMarkerBounds(correspondingMarkers)
                // mapObject = window.mapObject;

            // mapObject.fitBounds(bounds);
        }else{
            let marker = correspondingMarkers[Object.keys(correspondingMarkers)[0]];
            actions.adjustCenter({commit, state}, marker.position);
        }

        commit('updateFocusedMarkers', correspondingMarkers);
        commit('updateFocusedInitiatives', {
            [initiative.id]: initiative
        });
        // commit('updateTopInitiatives', {})
    },
    focusMarker({commit, rootGetters}, marker) {
        let correspondingInitiatives = InitiativeMapService.findCorrespondingEntities(
            rootGetters['initiative/indexInitiatives'],
            marker.initiatives
        );

        commit('updateFocusedInitiatives', correspondingInitiatives);
        commit('updateFocusedMarkers', {
            [marker.id]: marker
        });
    },
    clearFocused({commit}) {
        commit('updateFocusedInitiatives', {});
        commit('updateFocusedMarkers', {});
    },
    inspectInitiative({commit, store}, initiative) {
        // to do: scroll down and inspect initiative
    }
}

// mutations
const mutations = {
    updateBounds (state, bounds){
        state.bounds = bounds
    },
    updateCenter (state, center){
        state.center= center
    },
    updateFocusedInitiatives (state, initiatives){
        state.focusedInitiatives = initiatives
    },
    updateTopInitiatives (state, initiatives){
        state.topInitiatives = initiatives
    },
    updateTopAndFocusedInitiatives (state, initiatives){
        state.topInitiatives = initiatives;
        state.focusedInitiatives = initiatives
    },
    updateMarkers (state, markers){ 
        state.markers = markers
    },
    updateFocusedMarkers (state, markers){
        state.focusedMarkers = markers
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}