import Api from '../../../../api'
import _ from 'lodash'

// initial state
const state = {
    name: '',
    start_at: new Date(),
    categories: [],
    status: null,
    group_size: null,
    audience_size: null,
    audience: [],
    location: {
        name: null,
        position: {
            lat: null,
            lng: null
        },

    },
    location_type: null,
    area_size: null,
    logo: null,
    cover: null,
    keywords: '',
    short_description: '',
    description: '',
    contacts: [],
    errors: {
        validator: true,
        categories: false,
        contacts: false,
        audience: false,
    }
}

// getters
const getters = {
    indexForm: state => state,
    indexErrors: state => state.errors
}

// actions
const actions = {
    refresh({commit}) {
        const stored = localStorage.getItem('form')

        if(stored) {
            commit('refreshForm', {stored})
        }
    },
    edit({commit, rootState}) {
        let request = {
            id: rootState.route.params.id
        }

        Api.user.initiative.find(request).then(response => {
            let initiative = response.data.initiative
            commit('editForm', {initiative})
        })
    },
    create({commit, state}) {
        let request = {
            ...state
        }

        actions.prepareRequest(request)

        Api.user.initiative.create(request).then(response => {
            console.log(response)
        })
    },
    update({commit, state, rootState}) {
        let request = {
            id: rootState.route.params.id,
            ...state
        }

        actions.prepareRequest(request)

        Api.user.initiative.update(request).then(response => {
            console.log(response)
        })
    },
    validate({commit, state}) {
        let errors = {
            categories: !state.categories.length,
            audience: !state.audience.length,
            // audience_size: !state.audience_size,
            contacts: !state.contacts.length,
            location: !state.location.position.lat || !state.location.position.lng,
            validator: true
        }

        _.each(errors, function (error, key) {
            if (error && key !== 'validator') errors.validator = false
        })

        commit('updateErrors', {errors})
    },
    prepareRequest(request) {
        delete request.errors
        request.location = {
            name: request.location.name,
            ...request.location.position
        }
        request.audience_size = request.audience_size.value
    }
}

// mutations
const mutations = {
    updateForm(state, {field, value}) {
        Object.assign(state, {
            [field]: value
        });
        localStorage.setItem('form', JSON.stringify(state))
    },
    refreshForm(state, {stored}) {
        _.each(stored, function(value, field) {
            Object.assign(state, {
                [field]: value
            });
        })
    },
    editForm(state, {initiative}) {
        _.each(initiative.categories, function (category) {
            category.label = category.name
            category.value = category.id
        })

        _.each(initiative.audience, function (a) {
            a.label = a.name
            a.value = a.id
        })

        let location = initiative.locations[0]

        initiative.location = {
            name: location.name,
            position: {
                lat: location.lat,
                lng: location.lng
            }
        }

        _.each(initiative, function(value, field) {
            Object.assign(state, {
                [field]: value
            });
        })
    },
    addContact(state, {contact}) {
        state.contacts.push(contact)
    },
    removeContact(state, index) {
        state.contacts.splice(index, 1)
    },
    updateLocation(state, {location}) {
        state.location = location
    },
    updateErrors(state, {errors}) {
        state.errors = errors
    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}