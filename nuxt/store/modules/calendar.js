import fn from '../../services/calendar'
import _ from 'lodash'

// initial state
const state = {
    events: [],
    upcoming: [],
    upcomingPaginated: [],
    selectedDate: {},
    selectedMonth: {},
    selectedDateEvents: [],
    selectedMonthEvents: [],
    table: [],
    days: [],
    months: [],
    activePill: 'upcoming'
}

// getters
const getters = {
    indexEvents: state => state.events,
    indexTable: state => state.table,
    indexSelectedDate: state => state.selectedDate,
    indexSelectedMonth: state => state.selectedMonth,
    indexSelectedDateEvents: state => state.selectedDateEvents,
    indexSelectedMonthEvents: state => state.selectedMonthEvents,
    indexUpcoming: state => state.upcoming,
    indexUpcomingPaginated: state => state.upcomingPaginated,
    indexActivePill: state => state.activePill
}

// actions
const actions = {
    generateTable ({commit, state}) {
        const today = new Date()
        if (_.isEmpty(state.selectedDate)) {
            state.selectedDate = {
                year: today.getFullYear(),
                month: today.getMonth(),
                day: today.getDate()
            }
        }
        if (_.isEmpty(state.selectedMonth)) {
            state.selectedMonth = {
                year: today.getFullYear(),
                month: today.getMonth()
            }
        }
        const year = state.selectedMonth.year
        const month = state.selectedMonth.month
        const firstDay = new Date(year, month, 1)
        const selected = new Date(state.selectedDate.year, state.selectedDate.month, state.selectedDate.day)

        let dayCounter = 0,
            offset = 0,
            table = [];

        /**
         * offset dayCounter backward to achieve correct aligning in calendar table column
         */
        let firstDayInMonthWeekday = firstDay.getDay() || 7

        if (firstDayInMonthWeekday !== 0) {
            offset = firstDayInMonthWeekday - 2
            dayCounter -= offset
        }

        /**
         * Next line does this: we go one month forward with day equal to 0 (days start with 1 when using Date), thus
         * we got last day of current month. getDate() gives us length of selected month.
         */
        const monthLength = new Date(year, month + 1, 0).getDate()

        for (let row = 0; row < 42; row++, dayCounter++) {
            const fullDateOfDay = new Date(year, month, dayCounter)
            const isToday = fn.isEqualDates(fullDateOfDay, today)
            const isThisMonth = fn.isThisMonth(fullDateOfDay, firstDay)
            const formattedDate = fn.dateToString(fullDateOfDay.getFullYear(), fullDateOfDay.getMonth(), fullDateOfDay.getDate())
            let eventClass = []
            if (isThisMonth && state.days[formattedDate]) {
                _.each(state.days[formattedDate], function (event) {
                    eventClass.push(event.category.color)
                })
            }
            const picked = fn.isEqualDates(fullDateOfDay, selected) && isThisMonth
            const dayNumber = isThisMonth ? fullDateOfDay.getDate() : null
            const style = actions.addStyleToDay(dayNumber, isToday, picked)

            table.push({
                date: fullDateOfDay,
                dayNumber,
                today: isToday,
                eventClass,
                picked,
                style
            })

            // break so that it doesn't push in an empty row
            if (isThisMonth && fullDateOfDay.getDate() === monthLength) break
        }

        commit('UPDATE_CALENDAR_TABLE', {table})
    },
    nextMonth ({commit, state}) {
        let month = state.selectedMonth.month + 1
        let year = state.selectedMonth.year

        if (month > 11) {
            month = 0
            year++
        }

        commit('UPDATE_CALENDAR_SELECTED_MONTH', {year, month})
        actions.generateTable({commit, state})
    },
    prevMonth ({commit, state}) {
        let month = state.selectedMonth.month - 1
        let year = state.selectedMonth.year

        if (month < 0) {
            month = 11
            year--
        }

        commit('UPDATE_CALENDAR_SELECTED_MONTH', {year, month})
        actions.generateTable({commit, state})
    },
    setCurrentDay ({commit, state}, date) {
        const day = date.getDate();
        const month = date.getMonth();
        const year = date.getFullYear();

        commit('UPDATE_CALENDAR_SELECTED_DATE', {year, month, day})
        commit('UPDATE_ACTIVE_PILL', 'selected-date')

        actions.generateTable({commit, state})
        console.log(state)
    },
    addStyleToDay (dayNumber, today, picked) {
        let string = ''
        string += dayNumber ? 'has-day ' : ''
        string += today ? 'today ' : ''
        string += picked ? 'picked ' : ''

        return string
    }
}

// mutations
const mutations = {
    RECEIVE_EVENTS (state, {events}) {
        state.events = events
    },
    update_event_list (state, events) {
        state.events = events

        const days = {};
        const months = {};
        _.each(events, function (event) {
            let start = new Date(event.start_at),
                end = new Date(event.end_at);

            _.each(fn.getDates(start, end), function (date) {
                const dateString = fn.dateToString(date.getFullYear(), date.getMonth(), date.getDate());
                if (days[dateString] === undefined) days[dateString] = [];
                days[dateString].push(event);
            });

            _.each(fn.getMonths(start, end), function (month) {
                let monthString = fn.dateToString(month.getFullYear(), month.getMonth());
                if (months[monthString] === undefined) months[monthString] = [];
                months[monthString].push(event);
            });
        })
        state.days = days;
        state.months = months;
        mutations.UPDATE_CALENDAR_SELECTED_MONTH_EVENTS(state);
        mutations.UPDATE_CALENDAR_SELECTED_DATE_EVENTS(state);

        const upcoming = [];
        _.each(events, function (event) {
            let startAt = new Date(event.start_at),
                endAt = new Date(event.end_at),
                now = new Date();
            if (startAt >= now || fn.isBetweenDates(now, startAt, endAt)) {
                upcoming.push(event)
            }
        })
        state.upcoming = upcoming
    },
    UPDATE_CALENDAR_TABLE (state, {table}) {
        state.table = table
    },
    UPDATE_CALENDAR_UPCOMING (state, {events}) {
        state.upcoming = []
        _.each(events, function (event) {
            const startAt = new Date(event.start_at)
            if (startAt >= new Date()) {
                state.upcoming.push(event)
            }
        })
    },
    UPDATE_CALENDAR_SELECTED_DATE (state, {year, month, day}) {
        state.selectedDate = {year, month, day}
        mutations.UPDATE_CALENDAR_SELECTED_DATE_EVENTS(state)
    },
    UPDATE_CALENDAR_SELECTED_DATE_EVENTS (state) {
        let selectedDateString = fn.dateToString(state.selectedDate.year, state.selectedDate.month, state.selectedDate.day)
        state.selectedDateEvents = _.toArray(state.days[selectedDateString])
    },
    UPDATE_CALENDAR_SELECTED_MONTH (state, {year, month}) {
        state.selectedMonth = {year, month}
        mutations.UPDATE_CALENDAR_SELECTED_MONTH_EVENTS(state)
    },
    UPDATE_CALENDAR_SELECTED_MONTH_EVENTS (state) {
        let selectedMonthString = fn.dateToString(state.selectedMonth.year, state.selectedMonth.month)
        state.selectedMonthEvents = _.toArray(state.months[selectedMonthString])
    },
    UPDATE_CALENDAR_UPCOMING_PAGINATED (state, upcomingPaginated) {
        state.upcomingPaginated = upcomingPaginated
    },
    UPDATE_ACTIVE_PILL (state, activePill) {
        state.activePill = activePill
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
