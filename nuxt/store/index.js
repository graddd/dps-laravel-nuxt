import Vuex from 'vuex'

import routed from './modules/routed.js'
import initiative from './modules/initiative'
import header from './modules/header'
import footer from './modules/footer'
import calendar from './modules/calendar'
import auth from './modules/auth'
import user from './modules/user'
import search from './modules/search'
import map from './modules/map'

const store = () => new Vuex.Store({
    state: {
        authToken: false,
        user: false,
        metadata: {},
        breadcrumb: {
            name: '',
            to: {
                name: 'index'
            }
        }
    },
    getters: {
        breadcrumb: state => state.metadata.breadcrumb,
        metadata: state => state.metadata,
        route: state => state.route
    },
    mutations: {
        update_metadata (state, metadata) {
            state.metadata = metadata
        },
        update_breadcrumb (state, name, to) {
            state.breadcrumb.name = name
            state.breadcrumb.to.name = to
        }
    },
    modules: {
        routed,
        auth,
        initiative,
        header,
        footer,
        calendar,
        // user,
        search,
        map
    }
    // actions: {
    //   getCurrentUser ({ commit }) {
    //     return Api.currentUser()
    //       .then((response) => {
    //         commit('setUser', response.data.data)
    //       })
    //   },
    //
    //   authRedirect ({ commit }) {
    //     Api.authRedirect()
    //   },
    //
    //   login ({ commit }, { code }) {
    //     Api.login(code)
    //       .then((response) => {
    //         commit('setUser', response.data.data)
    //         commit('setAuthToken', response.headers.authorization)
    //         $nuxt.$router.push('/dashboard')
    //       })
    //   },
    //
    //   logout ({ commit }) {
    //     Api.logout()
    //       .then(() => {
    //         commit('setUser', null)
    //         $nuxt.$router.push('/')
    //       })
    //   }
    // },
    //
    // mutations: {
    //   setAuthToken (state, token) {
    //     localStorage.authorization = token
    //     state.authToken = token
    //   },
    //   setUser (state, user) {
    //     state.user = user
    //   }
    // },
    //
    // getters: {
    // }
})

export default store
