import Api from './index'

/**
 * This is a plugin that helps wrap axios promise resolvement into a developer-friendly form that helps evade
 * repetitive code. Place in fetch.
 *
 * Use example: (in component)
 * fetch({app, params}){
 *     return app.$api.fetch('articles.get', params)
 * }
 *
 * or for multiple concurrent requests
 *
 * fetch({app, params}){
 *     return app.$api.fetch({
 *         article: ['articles.get', params.article],
 *         user: ['user.get', 'params.user']
 *     }, ({ article, user }){
 *         // here you can use article.data and user.data
 *     }, false) // optionally, set autoCommit to false
 * }
 *
 });
 */
export default (context) => {
    context.app.$api = {
        resolveArgs(args) {
            if(!args[0]){
                throw new Error('No argument passed to $api.fetch.')
            }
            if(args[0].constructor === String){
                return {
                    call: args[0],
                    params: args[1],
                    callback: args[2],
                    autoCommit: args[3]
                }
            } else {
                return {
                    call: args[0],
                    callback: args[1],
                    autoCommit: args[2]
                }
            }
        },

        /**
         * Main function that you should call for making api calls inside fetch.
         *
         * @param args
         * @return Promise
         */
        fetch(...args) {
            let {call, params = null, callback = null, autoCommit = true} = this.resolveArgs(args);

            let callerPromise = this.resolveCall(call, params);
            return callerPromise.then(response => {
                let resolvedResponse = this.resolveResponse({call, response});
                if (callback && callback.constructor === Function) {
                    callback(resolvedResponse);
                }
                if (autoCommit) {
                    this.autoCommit({context, response});
                }
            }).catch(error => {
                if (error.response.status === 404) {
                    context.error({
                        statusCode: 404
                    });
                }
            });
        },

        /**
         * Inputs a call argument and spits out correspondent promise.
         *
         * @param call
         * @param params
         * @return Promise
         */
        resolveCall(call, params) {
            let callerPromise;

            Object.propertyFromString = function (o, s) {
                s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
                s = s.replace(/^\./, '');           // strip a leading dot
                const a = s.split('.');
                for (let i = 0, n = a.length; i < n; ++i) {
                    let k = a[i];
                    if (k in o) {
                        o = o[k];
                    } else {
                        return;
                    }
                }
                return o;
            };

            function callerFromString(s){
                const caller = Object.propertyFromString(Api, s)
                if(!caller || caller.constructor !== Function){
                    throw new Error(`Property ${s} not defined as a function in api.`)
                }
                return caller
            }

            function callerFromArray(array) {
                // helper function
                let fn = callerFromString(array[0]),
                    params = array[1] || null;

                return fn(params);
            }

            switch (call.constructor) {
                case String:
                    callerPromise = callerFromString(call)(params);
                    break;
                case Object:
                    callerPromise = Promise.all(Object.keys(call).map((key) => {
                        return call[key].constructor === Array ? callerFromArray(call[key]) : callerFromString(call[key])();
                    }));
                    break;
                default:
                    throw new Error('First argument of $api.fetch must be either a string or an object.');
                    break;
            }

            return callerPromise;
        },

        /**
         * Makes an object with call names as keys when executing multiple ajax calls per one Promise.
         *
         * @param call
         * @param response
         */
        resolveResponse({call, response}) {
            if (response.constructor === Array) {
                let responseKeyed = {};
                Object.keys(response).forEach((key) => {
                    let namedKey = Object.keys(call)[key];
                    responseKeyed[namedKey] = response[key];
                });
                response = responseKeyed
            }

            return response;
        },

        /**
         * Automatically commits response data. Response data must be an object of form
         * {first_commit_name: first_commit_data, ...} for single call or [{first_commit_name: first_commit_data, ...}, ...]
         * for multuple ajax calls. Meta data also gets committed.
         *
         * @param context
         * @param response
         */
        autoCommit({context, response}) {
            function commitSingleReponse(singleResponse) {
                let singleResponseData = singleResponse.data;
                Object.keys(singleResponseData).forEach((commitName) => {
                    context.store.commit(commitName, singleResponseData[commitName], {root: true});
                    console.log('api-service autocommit: Mutation', commitName, 'committed.') //, singleResponseData[commitName]
                });
            }

            if (response.constructor === Array) {
                for (let singleResponse of response) {
                    commitSingleReponse(singleResponse);
                }
            } else {
                commitSingleReponse(response);
            }
        }
    }
}