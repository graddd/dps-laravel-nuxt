import axios from 'axios'

export default {
    routed: {
        home: {
            get () {
                return axios.get('spa/')
            }
        },
        article: {
            get (params) {
                return axios.get('spa/article', {params})
            },
            find (params) {
                return axios.get('spa/article/' + params.slug)
            }
        },
        search: {
            get (params) {
                return axios.get('spa/search', {params})
            }
        },
        event: {
            get (params) {
                return axios.get('spa/event', {params})
            },
            find (params) {
                return axios.get('spa/event/' + params.slug)
            },
            calendar: {
                get() {
                    return axios.get('spa/calendar')
                }
            }
        }
    },
    user: {
        initiative: {
            get () {
                return axios.get('user/initiative')
            },
            find (params) {
                return axios.get('user/initiative/' + params.id)
            },
            create (params) {
                return axios.post('user/initiative', params)
            },
            update (params) {
                return axios.put('user/initiative/' + params.id, {params})
            }
        }

    },
    event: {
        search (params) {
            return axios.get('event/search', {params})
        }
    },
    article: {
        search (params) {
            return axios.get('article/search', {params})
        },
        getOptions () {
            return axios.get('article/options')
        }
    },
    email: {
        subscribe(params) {
            return axios.post('email/subscribe', params)
        }
    },
    initiative: {
        get (params) {
            return axios.get('initiative', {params})
        },
        find (params) {
            return axios.get('initiative/' + params.id)
        },
        getOptions () {
            return axios.get('initiative/options')
        }
    },
    auth: {
        login (params) {
            return axios.post('auth/password', params)
        },
        setAxiosDefaults (params) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + params.access_token
        },
        getUser () {
            return axios.get('user')
        },
        facebook () {
            return axios.get('auth/facebook')
        }
    }
}
