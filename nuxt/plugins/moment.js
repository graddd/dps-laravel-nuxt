import Vue from 'vue'
import moment from 'moment'

export default ({ app }) => {
    // Set i18n instance on app
    // This way we can use it in middleware and pages asyncData & fetch
    app.$moment = moment
}
