window.$ = window.jQuery = require('jquery')
window._ = require('lodash')
require('sticky-kit/dist/sticky-kit')
require('bootstrap-sass')
require('featherlight')

window.fbAsyncInit = function () {
  FB.init({
    appId: '1209031475844354',
    cookie: true,
    xfbml: true,
    version: 'v2.9'
  })
  FB.AppEvents.logPageView()

  return 'loaded'
};

(function (d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0]
  if (d.getElementById(id)) { return }
  js = d.createElement(s); js.id = id
  js.src = '//connect.facebook.net/en_US/sdk.js'
  fjs.parentNode.insertBefore(js, fjs)
}(document, 'script', 'facebook-jssdk'))
