import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'
import VueLazyload from 'vue-lazyload'
import Multiselect from 'vue-multiselect'
import Lightbox from 'vue-pure-lightbox'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps'

// vue google maps
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAWbJYfCbPNlyIcmkQ7TO7MfzhtO8yS6aA',
        // libraries: 'places', // This is required if you use the Autocomplete plugin
    }
})
// vue-moment
const moment = require('moment')
require('moment/locale/sl')
Vue.use(require('vue-moment'), {
    moment
})
// vue-scrollto
Vue.use(VueScrollTo, {
    container: 'body',
    duration: 1000,
    easing: 'ease',
    offset: 0,
    cancelable: true,
    onDone: false,
    onCancel: false
})
// truncating
Vue.use(require('vue-truncate-filter'))
// image lazy load
Vue.use(VueLazyload)
// Vue Multiselect
Multiselect.props.deselectLabel.default = 'Odstrani izbiro'
Multiselect.props.selectLabel.default = ''
Multiselect.props.selectedLabel.default = 'Izbrano'
Vue.component('multiselect', Multiselect)
// vue lightbox, currently not in use
Vue.use(Lightbox)
