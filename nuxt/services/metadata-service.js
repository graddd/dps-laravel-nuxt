
export const metadata = (metadata, route) => {
    return {
        title: metadata.document_title,
        meta: [
            {hid: 'description', name: 'description', content: metadata.description},
            {hid: 'keywords', name: 'keywords', content: metadata.keywords},
            {hid: 'og_title', property: 'og:title', content: metadata.title},
            {hid: 'og_description', property: 'og:description', content: metadata.description},
            {hid: 'og_image', property: 'og:image', content: metadata.image},
            {hid: 'og_url', property: 'og:url', content: route.fullPath},
            {hid: 'og_type', property: 'og:type', content: metadata.type},
        ]
    }
};