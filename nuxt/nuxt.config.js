const {resolve} = require('path')

module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'Don\'t forget to change page title in page!',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui'},
            {hid: 'description', content: 'hid defined in nuxt.config.js'}
        ],
        link: [
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css?family=Inconsolata:400,700|Martel:400,700&subset=latin-ext'
            },
            {rel: 'icon', type: '/images/icons/x-icon', href: '/images/icons/favicon.ico'}
        ]
    },
    modules: [
        ['@nuxtjs/google-analytics', { ua: 'UA-107129045-1' }],
    ],
    /*
     * Globally available plugins such as bootstrap, jquery, axios, facebook sdk and
     * Vue plugins that utilize Vue.use().
     */
    plugins: [
        '~plugins/axios.js',
        '~api/plugin.js',
        '~plugins/vuex-router-sync.js',
        '~plugins/vue-usable.js',
        '~plugins/i18n.js',
        '~plugins/moment.js',
        {src: '~plugins/web.js', ssr: false}
    ],
    build: {
        extend(config) {
            config.resolve.alias['vue'] = 'vue/dist/vue.common'
        },
        vendor: [
            'axios',
            'lodash',
            'jquery',
            'vue-i18n',
            'vue-scrollto',
            'vue-lazyload',
            'vue-multiselect',
            'vue-truncate-filter',
            'js-cookie',
            'vue2-google-maps'
        ]
    },
    /*
     ** Global CSS
     */
    css: [
        resolve(__dirname, 'node_modules/vue-multiselect/dist/vue-multiselect.min.css'),
        resolve(__dirname, 'assets/sass/app.scss')
    ],

    /*
     * Customize the progress-bar color
     */
    loading: {color: '#6aab88', height: '5px'},
    /*
     * is production?
     */
    dev: (process.env.NODE_ENV !== 'production')
}
